# Activbet Web Application

## Cloning

Be sure to clone the project with the ``--recursive`` flag so the core submodule is checked out as well,
and use SSH to connect to gitlab. Follow [this guide](http://gitlab.office.betserv.systems/help/ssh/README) if you do not have an
SSH key associated with your machine yet.

```
git clone --recursive git@gitlab.office.betserv.systems:betserv/betago.git
```

If you already have the project cloned, you can init and update the submodule with:

```
git submodule init
git submodule update
```

## Install Java

In Ubuntu, install Java 8 with the following commands:

```
sudo add-apt-repository ppa:no1wantdthisname/openjdk-fontfix
sudo apt-get update
sudo apt-get install openjdk-8-jdk
```

Ensure that the default java is the newly installed Java 8 JDK with:

```
sudo update-alternatives --config java
```

## Setup IntelliJ Ultimate

Open the ``pom.xml`` file in the project root with IntelliJ Idea, setup your JDK (In Unix systems under ``/usr/lib/jvm/{JAVA 8 FOLDER}``)
and install the Lombok Plugin (File -> Settings -> Plugins -> Browse Repositories -> Search For Lombok -> Install -> Restart).

Use your terminal (or press ``Alt+F12`` in IntelliJ) and go to ``betago/src/main/resources/static``. Then execute the following commands to install the
Javascript development environment:

```
yarn
```

Then select the necessary maven profiles within IntelliJ. Double tap the ``Shift`` key, search for "Maven Projects" and open the Tool Window.
Expand profiles and check the appropriate profile for your operating system (osx, win or linux). Re-Import the changes
if necessary. If an alert is displayed, "Enable auto-import" if possible.

## Import the code style settings

Go to the IntelliJ settings and navigate to ``Editor -> Code Style``. Click on ``Manage`` and import the ``codeStyleSettings.xml`` from the root
of the project folder. Then select the just imported ``Betago Web`` style.

## Start the Application

Start the application in IntelliJ by using the Bug-Icon in the top right (This starts in Debug-Mode so the Hot-Swapping of
classes and resources work). If any errors occur, try Re-Importing the Project by right clicking the spring folder in the
navigation panel on the left and then ``Maven -> Reimport``. If there are still any compile errors try ``Build -> Rebuild Project``
from the menu bar.

If the application and the javascript development server (``yarn start``) is started, the page can be reached under ``http://localhost:3000`` in the browser.

### Development

When developing, it is necessary to start the webpack dev server to transpile and serve the javascript files. Therefor navigate to the
``betago/src/main/resources/static`` folder and run ``yarn start``. The server also hot-swaps the javascript files into the
running application.

## Packaging

Maven is configured in a way that should care about everything when packaging the application. It will install node and yarn and
execute the necessary commands to produce the Javascript bundles. The only thing that is necessary is to set the correct
profile for the target operating system. This is due to the V8 Javascript Engine that is included for the server side
pre-rendering of the React templates. Choose the correct line from below. Without profile builds for linux.

```
// Linux (default, the same as without profile)
mvn clean package -P linux
// OS X
mvn clean package -P osx
// Windows
mvn clean package -P win
```

## Running Java from the Commandline

The spring application can be run from the commandline (maven must be installed ``sudo apt-get install maven``) with the following command:

```
mvn -P run clean spring-boot:run
```


## Help
### Windows node-sass throws an Error because of VCBuild.exe

In Admin Powershell

```
yarn global add windows-build-tools
```
or install the latest .NET Framework SDK (Included in Windows 10 SDK).

Then reboot.