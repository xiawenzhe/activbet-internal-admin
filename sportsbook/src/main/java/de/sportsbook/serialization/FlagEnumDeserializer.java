package de.sportsbook.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.stream.Stream;

@Slf4j
@NoArgsConstructor
public class FlagEnumDeserializer<T extends Enum<T> & FlagEnum> extends JsonDeserializer<T> implements ContextualDeserializer {

    public Class<T> clazz;

    public FlagEnumDeserializer(Class<T> clazz) {
        this.clazz = clazz;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        int value = p.getIntValue();

        try {
            return Stream.of((T[]) clazz.getMethod("values").invoke(null))
                    .filter(val -> val.getFlag() == value)
                    .findFirst()
                    .orElse(null);
        } catch (Exception e) {
            log.error("Error deserializing FlagEnum", e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public JsonDeserializer<T> createContextual(DeserializationContext ctxt, BeanProperty property) throws
                                                                                                    JsonMappingException {
        return (JsonDeserializer<T>) new FlagEnumDeserializer(property.getType().getRawClass());
    }
}
