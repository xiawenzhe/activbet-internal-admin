package de.sportsbook.serialization;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/*
  There is no other way to reliably deserialize polymorphic other than an annotation, and this
  is used for all objectmappers. Should be ok though as we only deserialize FlagEnums coming from
  the backend. If the value of a FlagEnum needs to be deserialized coming from the Frontend with,
  this needs a Workaround.

  The serialization is handled via registration in the objectmapper as we need it differently
  in the frontend and backend.
 */
@JsonDeserialize(using = FlagEnumDeserializer.class)
public interface FlagEnum {
    int getFlag();
}