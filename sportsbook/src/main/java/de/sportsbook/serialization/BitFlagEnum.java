package de.sportsbook.serialization;

import java.util.EnumSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface BitFlagEnum {
    int getFlag();

    static long toFlag(EnumSet<? extends BitFlagEnum> set) {
        if (set == null) {
            return 0;
        }

        long bitFlag = 0;

        for (Object t : set) {
            bitFlag |= ((BitFlagEnum) t).getFlag();
        }

        return bitFlag;
    }

    @SuppressWarnings("unchecked")
    static <T extends Enum<T> & BitFlagEnum> EnumSet<T> fromFlag(Class<T> clazz, long bitFlag) throws Exception {
        return Stream.of((T[]) clazz.getMethod("values").invoke(null))
                // If the Enum has a distinct zero-value member, only include it if the bitFlag is exactly zero.
                .filter(v -> (bitFlag == 0 || v.getFlag() != 0) && (v.getFlag() & bitFlag) == v.getFlag())
                .collect(Collectors.toCollection(() -> EnumSet.noneOf(clazz)));
    }

}
