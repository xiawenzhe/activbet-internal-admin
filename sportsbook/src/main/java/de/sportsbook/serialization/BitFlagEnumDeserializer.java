package de.sportsbook.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.EnumSet;

@Slf4j
@NoArgsConstructor
public class BitFlagEnumDeserializer<T extends Enum<T> & BitFlagEnum> extends JsonDeserializer<EnumSet<T>> implements ContextualDeserializer {

    public Class<T> clazz;

    public BitFlagEnumDeserializer(Class<T> clazz) {
        this.clazz = clazz;
    }

    @SuppressWarnings("unchecked")
    @Override
    public EnumSet<T> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        try {
            return BitFlagEnum.fromFlag(clazz, p.getLongValue());
        } catch (Exception e) {
            log.error("Error deserializing BitFlagEnum", e);
            return EnumSet.noneOf(clazz);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public JsonDeserializer<T> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        return (JsonDeserializer<T>) new BitFlagEnumDeserializer(property.getType().getContentType().getRawClass());
    }
}
