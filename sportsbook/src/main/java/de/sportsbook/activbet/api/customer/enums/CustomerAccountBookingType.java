package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerAccountBookingType implements FlagEnum {
    DIRECT_PAYIN(1),
    DIRECT_PAYOUT(2),
    PAYOUT_REQUEST(3),
    PRE_PAYMENT(10),
    EXTERNAL_PAY_IN(15),
    EXTERNAL_PAY_OUT(16),
    STAKE(21),
    WIN(22),
    CASHOUT(23);

    private final int flag;
}
