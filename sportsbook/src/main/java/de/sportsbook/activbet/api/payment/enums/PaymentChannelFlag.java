package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PaymentChannelFlag implements BitFlagEnum {
    // Enum Set
    NONE(0),
    PAY_IN_ALLOWED(1 << 0),
    PAY_OUT_ALLOWED(1 << 1),
    PAY_OUT_WITH_APPROVE(1 << 3),
    PAY_OUT_ONLY_THROUGH_PAY_IN_CHANNEL(1 << 4);

    private final int flag;
}
