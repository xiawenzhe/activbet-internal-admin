package de.sportsbook.activbet.api.oauth.entities;

import lombok.Data;
import lombok.Getter;

@Data
public class OauthAuthentication {

    private Long expires;
    @Getter
    private String accessToken;
    @Getter
    private String refreshToken;

    public boolean isExpired() {
        // Add 30 Buffer Seconds
        return expires - (System.currentTimeMillis() / 1000) < 30;
    }

}
