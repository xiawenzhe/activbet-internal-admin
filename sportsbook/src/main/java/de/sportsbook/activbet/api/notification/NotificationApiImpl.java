package de.sportsbook.activbet.api.notification;

import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.notification.entities.CashFlowNotification;
import de.sportsbook.activbet.api.notification.entities.MessageNotification;
import de.sportsbook.activbet.api.notification.entities.PayOutRequestNotification;
import de.sportsbook.activbet.api.notification.entities.PaymentTransactionNotification;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service
public class NotificationApiImpl extends BearerApi implements NotificationApi {

    /** contact */
    @Override
    public MessageNotification getMessageNotification() throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("widgets", "contacts");

        try {
            return get(uri.build(), MessageNotification.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /widgets/contacts", e);
            throw new ApiErrorException("get /widgets/contacts", e, uri.toUriString(), "");
        }
    }

    /** payout request */
    @Override
    public PayOutRequestNotification getPayOutRequestNotification() throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("widgets", "payout_requests");
        try {
            return get(uri.build(), PayOutRequestNotification.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /widgets/payout_requests", e);
            throw new ApiErrorException("get /widgets/payout_requests", e, uri.toUriString(), "");
        }
    }

    /** payment transaction */
    @Override
    public PaymentTransactionNotification getPaymentTransactionNotification() throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("widgets", "payment_transactions");
        try {
            return get(uri.build(), PaymentTransactionNotification.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /widgets/payment_transactions", e);
            throw new ApiErrorException("get /widgets/payment_transactions", e, uri.toUriString(), "");
        }
    }

    /** cash flow */
    @Override
    public CashFlowNotification getCashFlowNotification(String from,  Integer currencyId) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("widgets", "cash_flow");
        uri.queryParam("from", from);
        uri.queryParam("currencyId", currencyId);

        try {
            return get(uri.build(), CashFlowNotification.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /widgets/cash_flow", e);
            throw new ApiErrorException("get /widgets/cash_flow", e, uri.toUriString(), "");
        }
    }

}
