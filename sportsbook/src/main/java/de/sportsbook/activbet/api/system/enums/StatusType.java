package de.sportsbook.activbet.api.system.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum StatusType implements FlagEnum {
    INACTIVE(0),
    PENDING(1),
    ACTIVE(2);

    private final int flag;
}
