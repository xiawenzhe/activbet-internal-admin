package de.sportsbook.activbet.api.notification.entities;

import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class PayOutRequestNotification {
    private Integer needReleaseCount;
    private BigDecimal needReleaseVolume;
    private ZonedDateTime lastRequestTime;
    private ZonedDateTime oldestRequestTime;
}
