package de.sportsbook.activbet.api.ticket.entities;

import de.sportsbook.activbet.api.PageWrapper;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Ticket {
    private Integer id;
    private String number;
    private Boolean isStandard;
    private Boolean isLive;
    private ZonedDateTime insertTime;
    private ZonedDateTime cancelTime;
    private ZonedDateTime payOutTime;
    private Integer customerUserId;
    private String customerUser;
    private BigDecimal stake;
    private BigDecimal stakeFee;
    private BigDecimal payIn;
    private Integer betCount;
    private BigDecimal maxOdd;
    private BigDecimal maxWin;
    private BigDecimal maxWinFee;
    private BigDecimal maxPayOut;
    private Integer possibleBetCount;
    private BigDecimal possibleOdd;
    private BigDecimal possibleWin;
    private BigDecimal possibleWinFee;
    private BigDecimal possiblePayOut;
    private Integer winBetCount;
    private BigDecimal winOdd;
    private BigDecimal win;
    private BigDecimal winFee;
    private BigDecimal payOut;
    private Boolean isOpen;
    private Boolean isRefund;
    private Boolean isLost;
    private Boolean isWon;
    private Boolean isCancelled;
    private Boolean isEvaluated;
    private Boolean isPayoutReady;
    private Boolean isPayedOut;
    private Boolean isRequest;
    private Boolean isPaymentError;
    private Boolean isCashout;
    private Boolean isNotStatistic;
    private String currencyIso;
    private Integer currencyDecimalDigits;

    private List<TicketSystem> systems;
    private List<TicketBet> bets;

    public static class TicketPage extends PageWrapper<Ticket> {

    }

}
