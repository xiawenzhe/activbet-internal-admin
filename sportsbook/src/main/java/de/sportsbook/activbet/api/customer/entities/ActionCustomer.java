package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class ActionCustomer {
    private Integer id;
    private String username;
    private String firstName;
    private String middleName;
    private String lastName;
}