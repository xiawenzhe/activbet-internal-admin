package de.sportsbook.activbet.api.company;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.company.entities.CashFlow;
import de.sportsbook.activbet.api.company.entities.CashierReport;
import de.sportsbook.activbet.api.company.entities.CashierReportsRequest;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.api.company.entities.Company.CompanyList;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class CompanyApiImpl extends BearerApi implements CompanyApi {

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;

    /**
     * get company list
     * */
    @Override
    public List<Company> getCompanies(Integer parentId) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("companies", "list");
        if (Objects.nonNull(parentId)) {
            uri.queryParam("companyId", parentId);
        }

        try {
            return get(uri.build(), CompanyList.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /companies/list", e);
            throw new ApiErrorException("get /companies/list", e, uri.toUriString(), "");
        }
    }

    /**
     * get cashier report
     * */
    @Override
    public CashierReport postForCashierReports(Integer companyId, String from, String to) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("CashierReports");

        CashierReportsRequest request = new CashierReportsRequest();
        request.setCompanyId(companyId);
        request.setFrom(from);
        request.setUntil(to);

        String body = "";
        try {
            body = objectMapper.writeValueAsString(request);
        } catch (Exception e) {
        }

        try {
            return post(uri.build(), request, CashierReport.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("post /CashierReports", e);
            throw new ApiErrorException("post /CashierReports", e, uri.toUriString(), body);
        }
    }

    /**
       get cash flow
    @Override
    public CashFlow getCashFlow(String from, Integer currencyId) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("widgets", "cash_flow");
        uri.queryParam("from", from);
        uri.queryParam("currencyId", currencyId);

        try {
            return get(uri.build(), CashFlow.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /widgets/cash_flow", e);
            throw new ApiErrorException("get /widgets/cash_flow", e, uri.toUriString(), null);
        }
    }
     */

}
