package de.sportsbook.activbet.api.system.entities;

import de.sportsbook.activbet.api.system.enums.FilterType;
import lombok.Data;

@Data
public class FromTo {
    private String from;
    private String to;
}
