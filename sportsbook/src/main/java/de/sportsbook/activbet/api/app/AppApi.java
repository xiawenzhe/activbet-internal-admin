package de.sportsbook.activbet.api.app;

import de.sportsbook.activbet.api.app.entities.Language;
import de.sportsbook.activbet.api.customer.entities.Currency;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;

import java.util.List;

public interface AppApi {
    List<Language> getLanguages() throws UnauthorizedUserException, ApiErrorException;
    List<Currency> getCurrencies() throws UnauthorizedUserException, ApiErrorException;
}


