package de.sportsbook.activbet.api;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sportsbook.activbet.exceptions.AlreadyLoggedException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.utils.ContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import java.io.IOException;
import java.util.Arrays;

@Slf4j
public abstract class Api {

    private static RestTemplate restTemplate;

    protected RestTemplate getRestTemplate() {
        if (restTemplate == null) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setConnectTimeout(10_000);
            requestFactory.setReadTimeout(60_000);
            restTemplate = new RestTemplate(requestFactory);
            restTemplate.setMessageConverters(Arrays.asList(new FormHttpMessageConverter(),
                                                            new MappingJackson2HttpMessageConverter(objectMapper),
                                                            new ByteArrayHttpMessageConverter()));
        }
        return restTemplate;
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Environment env;

    public <T> T get(UriComponents uri, Class<T> entityClass) {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.GET, null);
    }

    public <T, E extends Exception> T get(UriComponents uri, Class<T> entityClass, Class<E> exceptionClass) throws E {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.GET, exceptionClass);
    }

    public <T> T post(UriComponents uri, Class<T> entityClass) {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.POST, null);
    }

    public <T, E extends Exception> T post(UriComponents uri, Class<T> entityClass, Class<E> exceptionClass) throws E {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.POST, exceptionClass);
    }

    public <T> T post(UriComponents uri, Object requestEntity, Class<T> entityClass) {
        return exchangeForEntity(uri, requestEntity, entityClass, HttpMethod.POST, null);
    }

    public <T, E extends Exception> T post(UriComponents uri, Object requestEntity, Class<T> entityClass, Class<E> exceptionClass) throws E {
        return exchangeForEntity(uri, requestEntity, entityClass, HttpMethod.POST, exceptionClass);
    }

    public <T> T put(UriComponents uri, Object requestEntity, Class<T> entityClass) {
        return exchangeForEntity(uri, requestEntity, entityClass, HttpMethod.PUT, null);
    }

    public <T, E extends Exception> T put(UriComponents uri, Object requestEntity, Class<T> entityClass, Class<E> exceptionClass) throws E {
        return exchangeForEntity(uri, requestEntity, entityClass, HttpMethod.PUT, exceptionClass);
    }

    public <T> T delete(UriComponents uri, Class<T> entityClass) {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.DELETE, null);
    }

    public <T, E extends Exception> T delete(UriComponents uri, Class<T> entityClass, Class<E> exceptionClass) throws E {
        return exchangeForEntity(uri, null, entityClass, HttpMethod.DELETE, exceptionClass);
    }

    public <T> T delete(UriComponents uri, Object requestEntity, Class<T> entityClass) {
        return exchangeForEntity(uri, requestEntity, entityClass, HttpMethod.DELETE, null);
    }

    private <T, E extends Exception> T exchangeForEntity(UriComponents uri, Object requestEntity, Class<T> entityClass, HttpMethod method,
                                                         Class<E> exceptionClass) throws E {

        HttpHeaders httpHeaders = getHttpHeaders();

        System.out.println(httpHeaders.get("Authorization"));

        try {
            ResponseEntity<T> template = getRestTemplate().exchange(uri.toUri(), method, new HttpEntity<>(requestEntity, httpHeaders), entityClass);
            return template.getBody();
        } catch (RestClientException e) {
            if (e instanceof HttpStatusCodeException) {
                HttpStatusCodeException codeException = (HttpStatusCodeException) e;

                // unauthorized user
                if (codeException.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    throw new UnauthorizedUserException(uri.toUriString() + " returned HTTP 401 Unauthorized!");

                    /*
                    String authorization = httpHeaders.getFirst("Authorization");
                    if (authorization != null && authorization.startsWith("Bearer")) {
                        // Only throw the UnauthorizedUserException if a bearer api returned unauthorized
                        // (which in turn is e.g. handled by a redirect to the login page).
                        throw new UnauthorizedUserException(uri.toUriString() + " returned HTTP 401 Unauthorized!");
                    } else {
                        // If a normal basic authentication returns unauthorized, it's a more serious problem.
                        throw new RuntimeException(
                                "The non-bearer API " + uri.toUriString() + " returned HTTP 401 Unauthorized. Headers sent: " + httpHeaders.toString());
                    }
                     */
                } else if (codeException.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE) {
                    throw new RuntimeException("API " + uri.toUriString() + " returned 503 Service Unavailable");
                } else if (codeException.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
                    throw new RuntimeException("API " + uri.toUriString() + " returned 500 Service Internal Error");
                }

                String responseBody = codeException.getResponseBodyAsString();
                if (codeException.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR || env.acceptsProfiles("local", "dev")) {
                    log.error("Error " + method + " " + uri + " (" + codeException.getMessage() + "). Response:\n" + responseBody);
                }

                ObjectMapper om = ContextUtils.getBean(ObjectMapper.class);
                try {
                    if (exceptionClass != null) {
                        throw om.readValue(responseBody, exceptionClass);
                    }
                } catch (IOException e1) {
                    if (e1 instanceof JsonParseException) {
                        log.error("Invalid json returned from {} {} (Status {}):\n[Response Body Start]\n{}\n[Response Body End]",
                                  method,
                                  uri,
                                  codeException.getStatusCode(),
                                  responseBody,
                                  e1);
                        throw new AlreadyLoggedException(e1);
                    }
                    // application error
                    throw new RuntimeException(e1);
                }
            } else {
                log.error("", e);
            }
            throw new AlreadyLoggedException(e);
        }
    }

    protected abstract HttpHeaders getHttpHeaders();

}
