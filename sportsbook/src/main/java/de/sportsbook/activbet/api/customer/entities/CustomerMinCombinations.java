package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.customer.enums.MinCombinationInheritance;
import lombok.Data;

@Data
public class CustomerMinCombinations {
    private Integer applicationValue;
    private Integer applicationValueInherited;
    private MinCombinationInheritance applicationValueInheritance;
    private Integer cashValue;
    private Integer cashValueInherited;
    private MinCombinationInheritance cashValueInheritance;
    private Integer terminalValue;
    private Integer terminalValueInherited;
    private MinCombinationInheritance terminalValueInheritance;
}
