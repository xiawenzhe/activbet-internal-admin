package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class CustomerTitle {
    private Integer id;
    private String name;
}
