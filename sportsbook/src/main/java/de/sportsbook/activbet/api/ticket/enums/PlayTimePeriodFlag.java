package de.sportsbook.activbet.api.ticket.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PlayTimePeriodFlag implements BitFlagEnum {
    NONE(0),
    PERIOD_1(1 << 0),
    PERIOD_2(1 << 1),
    PERIOD_3(1 << 2),
    PERIOD_4(1 << 3),
    PERIOD_5(1 << 4),
    PERIOD_6(1 << 5),
    PERIOD_7(1 << 6),
    PERIOD_8(1 << 7),
    PERIOD_9(1 << 8),
    PERIOD_10(1 << 9),
    PERIOD_11(1 << 10),
    PERIOD_12(1 << 11),
    PERIOD_13(1 << 12),
    PERIOD_14(1 << 13),
    PERIOD_15(1 << 14),
    PERIOD_16(1 << 15),
    PERIOD_17(1 << 16),
    PERIOD_18(1 << 17),
    PERIOD_19(1 << 18),
    Period20(1 << 19),
    Period21(1 << 20),
    Period22(1 << 21),
    Period23(1 << 22),
    Period24(1 << 23),
    Period25(1 << 24),

    FULL_TIME((1 <<25) -1),

    OVERTIME_1(1 << 25),
    OVERTIME_2(1 << 26),
    OVERTIME_3(1 << 27),
    OVERTIME_4(1 << 28),

    OVERTIME_FULL_TIME((1 << 29) - 1),

    PENALTY(1 << 29),

    PENALTY_FULL_TIME((1 << 30) -1);

    private final int flag;
}
