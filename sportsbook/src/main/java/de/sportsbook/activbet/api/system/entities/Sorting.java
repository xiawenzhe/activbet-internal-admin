package de.sportsbook.activbet.api.system.entities;

import lombok.Data;

@Data
public class Sorting {
    private String property;
    private String direction;
}
