package de.sportsbook.activbet.api.ticket.entities;

import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Data
public class TicketBet {
    private Integer id;
    private ZonedDateTime gameStartTime;
    private String sportName;
    private String sportIconName;
    private String categoryName;
    private String categoryIconName;
    private String tournamentName;
    private String gameName;
    private List<String> rivalNames;
    private String tipName;
    private String oddName;
    private BigDecimal oddValue;
    private BigDecimal adjustedOddValue;
    private BigDecimal stake;
    private String gameStatus;
    private Boolean isBank;
    private Boolean isRefund;
    private Boolean isHalfLost;
    private Boolean isLost;
    private Boolean isHalfWon;
    private Boolean isWon;
    private Boolean isLive;
    private BigDecimal scoreHome;
    private BigDecimal scoreAway;
    private List<TicketBetGameResult> gameResults;
}
