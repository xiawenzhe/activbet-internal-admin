package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PaymentDirection implements FlagEnum {
    DEPOSIT(1),
    WITHDRAW(2);

    private final int flag;
}
