package de.sportsbook.activbet.api.company.entities;

import de.sportsbook.activbet.api.customer.entities.Currency;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;

@Data
public class CashierReportSub {
    /* sports */
    private CashierReportCountValue inserted;
    private CashierReportCountValue cancelled;
    private CashierReportCountValue cashedOut;
    private CashierReportCountValue payedOut;
    private CashierReportCountValue unpaid;
    private CashierReportCountValue balance;

    /* external */
    private String name;
    // inserted, cancelled, payedOut, balance

    /* prePayments */
    private CashierReportCountValue generated;
    private CashierReportCountValue booked;
    private CashierReportCountValue open;
    private CashierReportCountValue expired;
    // balance

    /* payOutRequests */
    // generated, booked, open, expired, balance

    /* customerBookings */
    private CashierReportCountValue payIn;
    private CashierReportCountValue payOut;
    // balance

    /* cashierBookings */
    private CashierReportCountValue patch;
    // payIn, payOut, balance

}
