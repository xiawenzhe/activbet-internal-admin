package de.sportsbook.activbet.api.customer;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.entities.CustomerAccountBooking;
import de.sportsbook.activbet.api.customer.entities.CustomerMessage;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.api.customer.enums.*;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;

import java.math.BigDecimal;

public interface CustomerApi {
    Customer getCustomerByUsername(String username, CustomerType customerType) throws ApiErrorException, UnauthorizedUserException;
    PageWrapper<Customer> getCustomers(Integer pageIndex, Integer pageSize, Integer companyId, String search, CustomerType customerType) throws ApiErrorException, UnauthorizedUserException;
    PageWrapper<CustomerLog> getCustomerLogs(CustomerLogType logType, Integer pageIndex, Integer pageSize) throws ApiErrorException, UnauthorizedUserException;

    PageWrapper<CustomerAccountBooking> getCustomerAccountBookings(
        Integer pageIndex,
        Integer pageSize,
        Integer customerId,
        String searchKeyWord,
        String from,
        String to,
        CustomerAccountBookingType type,
        CustomerAccountBookingReason reason,
        Boolean onlyTickets,
        Boolean onlyPayments,
        Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException;

    CustomerAccountBooking postForPatchCustomerBooking(Integer customerId, BigDecimal value, String text)
        throws ApiErrorException, UnauthorizedUserException;

    Customer getCustomerById(Integer customerId) throws ApiErrorException, UnauthorizedUserException;

    PageWrapper<CustomerMessage> getCustomerContact(
            Integer pageIndex, Integer pageSize,
            String from, String to, Boolean isUnread, Boolean isClosed
    ) throws ApiErrorException, UnauthorizedUserException;

    CustomerMessage putCustomerContact(@NonNull Integer id) throws ApiErrorException, UnauthorizedUserException;
    CustomerMessage deleteCustomerContact(@NonNull Integer id) throws ApiErrorException, UnauthorizedUserException;
}
