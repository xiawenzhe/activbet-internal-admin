package de.sportsbook.activbet.api.payment.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.ActionCustomer;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.system.entities.SystemClient;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionType;
import lombok.Data;


import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class PaymentTransaction {
    private String uniqueCode;
    private BigDecimal value;
    private BigDecimal fee;
    private BigDecimal bookingValue;
    private PaymentDirection direction;
    private PaymentTransactionStatus status;
    private ZonedDateTime statusChangeTime;
    private String statusReason;
    private ActionCustomer customer;
    private PaymentChannel paymentChannel;
    private SystemClient client;
    private String remoteAddress;
    private TechnicalData technicalData;
    private String externalReferenceId;

    public static class PaymentTransactionPage extends PageWrapper<PaymentTransaction> {
    }

}
