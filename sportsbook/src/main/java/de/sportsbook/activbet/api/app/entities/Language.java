package de.sportsbook.activbet.api.app.entities;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Language {
    private Integer id;
    private String name;
    private String code;

    public static class LanguageList extends ArrayList<Language> {

    }

}
