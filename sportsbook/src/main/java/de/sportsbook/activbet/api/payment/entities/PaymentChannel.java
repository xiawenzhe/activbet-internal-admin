package de.sportsbook.activbet.api.payment.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.CustomerStatusInfo;
import de.sportsbook.activbet.api.payment.enums.PaymentChannelFlag;
import de.sportsbook.activbet.api.payment.enums.PaymentChannelPayMethod;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionType;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import lombok.Data;

@Data
public class PaymentChannel {

    private PaymentProvider paymentProvider;
    private TechnicalData technicalData;

    private String iconName;
    private String name;
    private String labelName;
    private String ledgerNumber;
    private String providerTransactionCode;
    private PaymentDirection paymentDirection;
    private PaymentChannelFlag paymentChannelFlag;
    private PaymentTransactionType paymentChannelType;
    private PaymentChannelPayMethod paymentChannelPayMethod;
    private Integer paymentProviderId;
    private Boolean payOutWithApprove;
    private Boolean payOutOnlyThroughPayInChannel;
    private CustomerStatusInfo statusInfo;

    public static class PaymentChannelPage extends PageWrapper<PaymentChannel> {

    }
}
