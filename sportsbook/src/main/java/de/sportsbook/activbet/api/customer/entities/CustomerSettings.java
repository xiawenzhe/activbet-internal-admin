package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class CustomerSettings {
    private CustomerSetting autoPrintTicket;
    private CustomerSetting isPreTicket;
    private CustomerSetting maxTipCustomerRiskFactor;
    private CustomerSetting minCombination;
    private CustomerSetting maxMoreWays;
    private CustomerSetting maxOddsInBetslip;
    private CustomerSetting maxCredit;
    private CustomerSetting customerSettingsWorkflow;
    private CustomerSetting cancelTime;
    private CustomerSetting liveCancelTime;
    private CustomerSetting liveDelay;
    private CustomerSetting prePlayTime;
    private CustomerSetting isMorewayForbidden;
    private CustomerSetting isSystemForbidden;
    private CustomerSetting isBankForbidden;
    private CustomerSetting combinationPrintMatrixOrderByOdd;
    private CustomerSetting combinationPrintMatrixMaxLength;
    private CustomerSetting combinationPrintMatrixMaxWidth;
    private CustomerSetting acceptOddChanges;
    private CustomerSetting includeFee;
    private CustomerSetting showScannerNumber;
    private CustomerSetting sendPlacedBetEmail;
    private CustomerSetting sendWinTicketEmail;
    private CustomerSetting sendPaymentEmail;
    private CustomerSetting verifyPaymentByEmail;
}
