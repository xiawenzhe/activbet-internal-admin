package de.sportsbook.activbet.api.system.entities;

import lombok.Data;

import java.util.List;

@Data
public class Relation {
    private List<RelationProperty> properties;
}
