package de.sportsbook.activbet.api.oauth;

import de.sportsbook.activbet.api.oauth.enums.LogoutReason;

public interface OauthApi {
    void postLogout(String userId, String remoteAddress, String userAgent, LogoutReason logoutReason);
}
