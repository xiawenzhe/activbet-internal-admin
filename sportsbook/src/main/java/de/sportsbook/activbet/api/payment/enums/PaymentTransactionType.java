package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PaymentTransactionType implements FlagEnum {
    PAY_IN(0),
    PAY_OUT(1);
    private final int flag;
}
