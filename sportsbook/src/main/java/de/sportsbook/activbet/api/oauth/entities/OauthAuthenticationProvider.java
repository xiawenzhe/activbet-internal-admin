package de.sportsbook.activbet.api.oauth.entities;

import java.util.Optional;

public interface OauthAuthenticationProvider {
    Optional<OauthAuthentication> getAuthentication();
}
