package de.sportsbook.activbet.api.system.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FilterType implements FlagEnum{

    STRING(0),
    NUMBER_RANGE(1),
    ENUM(2),
    BIT_FLAG_ENUM(3),
    DATE_TIME_RANGE(4),
    COMPANY(5),
    SPORT_COUNTRY_LEAGUE(6),
    TIP_MODEL(7);

    private final int flag;


    /*
    String,
    NumberRange,
    Enum,
    BitFlagEnum,
    DateTimeRange,
    Company,
    SportCountryLeague,
    TipModel;

     */

}
