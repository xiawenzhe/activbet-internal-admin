package de.sportsbook.activbet.api.company.entities;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CashierReportCountValue {
    private Integer count;
    private BigDecimal value;
}
