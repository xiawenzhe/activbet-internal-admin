package de.sportsbook.activbet.api;

import lombok.Data;

import java.util.List;

@Data
public class PageWrapper<T> {
    private Integer indexFrom;
    private Integer pageIndex;
    private Integer pageSize;
    private Integer totalCount;
    private Integer totalPages;
    private Boolean hasPreviousPage;
    private Boolean hasNextPage;
    private List<T> items;

    // used by account balance


}
