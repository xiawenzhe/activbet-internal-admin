package de.sportsbook.activbet.api.customer.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerAccountBookingReason implements FlagEnum {
    INSERT(0),
    DELETE(1),
    CANCEL(2),
    PATCH(3),
    PAY_IN(4),
    PAY_OUT(5);

    private final int flag;
}
