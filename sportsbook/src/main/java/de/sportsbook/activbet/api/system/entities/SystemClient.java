package de.sportsbook.activbet.api.system.entities;

import de.sportsbook.activbet.api.system.enums.ClientType;
import lombok.Data;

import java.util.EnumSet;

@Data
public class SystemClient {
    private Integer id;
    private String name;
    private EnumSet<ClientType> clientType;
}
