package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.system.entities.SystemClient;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class CustomerMessage {
    private Integer id;
    private ZonedDateTime insertTime;
    private ZonedDateTime updateTime;
    private String firstName;
    private String lastName;
    private String email;
    private String subject;
    private String content;
    private Boolean isAnonymous;
    private SystemClient client;
    private ActionCustomer requestUser;
    private ActionCustomer progressUser;
    private ActionCustomer closeUser;

    public static class CustomerContactPage extends PageWrapper<CustomerMessage> {
    }
}
