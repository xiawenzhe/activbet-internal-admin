package de.sportsbook.activbet.api.payment.enums;

import de.sportsbook.serialization.FlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PayOutRequestStatus implements FlagEnum {
    OPEN(0),
    PAID_OUT(1),
    CANCELLED_BY_USER(91),
    CANCELLED_BY_TIMEOUT(92);

    private final int flag;
}
