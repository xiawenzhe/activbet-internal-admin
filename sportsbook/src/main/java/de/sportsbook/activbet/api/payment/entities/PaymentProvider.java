package de.sportsbook.activbet.api.payment.entities;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.system.entities.TechnicalData;
import lombok.Data;

import java.util.ArrayList;

@Data
public class PaymentProvider {
    private Integer paymentChannelCount;
    private TechnicalData technicalData;
    private String name;

    public static class PaymentProviderPage extends PageWrapper<PaymentProvider> {
    }
}
