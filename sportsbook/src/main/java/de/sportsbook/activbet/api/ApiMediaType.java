package de.sportsbook.activbet.api;

import org.springframework.http.MediaType;

public class ApiMediaType extends MediaType {

    public ApiMediaType(String apiType, String version) {
        super("application", "vnd.betago-api-" + apiType + "." + version + "+json");
    }

}
