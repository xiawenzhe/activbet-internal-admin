package de.sportsbook.activbet.api.ticket.entities;

import de.sportsbook.activbet.api.ticket.enums.PlayTimePeriodFlag;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.List;

@Data
public class TicketBetGameResult {
    private BigDecimal home;
    private BigDecimal away;
    private EnumSet<PlayTimePeriodFlag> playTimePeriodFlag;
}
