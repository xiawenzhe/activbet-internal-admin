package de.sportsbook.activbet.api.oauth.enums;

public enum LogoutReason {
    MANUAL_LOGOUT,
    AUTOMATIC_LOGOUT,
    FORCE_LOGOUT,
    LOGIN_ABORTED,
    SESSION_EXPIRED
}