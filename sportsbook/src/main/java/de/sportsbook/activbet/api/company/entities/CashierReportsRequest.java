package de.sportsbook.activbet.api.company.entities;

import de.sportsbook.activbet.api.customer.entities.Currency;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class CashierReportsRequest {
    private Integer companyId;
    private String from;
    private String until;
}
