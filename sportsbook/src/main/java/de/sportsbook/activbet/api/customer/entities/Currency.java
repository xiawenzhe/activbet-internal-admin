package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Currency {
    private Integer id;
    private String iso;
    private String sign;
    private String name;
    private Integer decimalDigits;

    public static class CurrencyList extends ArrayList<Currency> {
    }

}
