package de.sportsbook.activbet.api.company.entities;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Company {
    private Integer id;
    private String shortcut;
    private String name1;
    private String name2;
    private Integer parentId;

    public static class CompanyList extends ArrayList<Company> {
    }
}
