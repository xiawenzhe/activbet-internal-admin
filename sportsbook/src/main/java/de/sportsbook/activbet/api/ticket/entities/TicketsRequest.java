package de.sportsbook.activbet.api.ticket.entities;

import de.sportsbook.activbet.api.system.entities.Filter;
import de.sportsbook.activbet.api.system.entities.Sorting;
import lombok.Data;

import java.util.List;

@Data
public class TicketsRequest {
    private Integer currencyId;
    private Sorting sorting;
    private List<Filter> filters;
}
