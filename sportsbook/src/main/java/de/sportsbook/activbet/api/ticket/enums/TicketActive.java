package de.sportsbook.activbet.api.ticket.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TicketActive implements BitFlagEnum {
    NONE(0),
    OPEN(1 << 0),
    REFUND(1 << 1),
    LOST(1 << 2),
    WON(1 << 3),
    CANCELLED(1 << 4),
    EVALUATED(1 << 5),
    PAYOUT_READY(1 << 6),
    PAID_OUT(1 << 7),
    CASHOUT(1 << 8),
    REQUEST(1 << 9),
    PAYMENT_ERROR(1 << 10),
    NOT_VISIBLE_TO_CUSTOMER(1 << 11);

    private final int flag;
}
