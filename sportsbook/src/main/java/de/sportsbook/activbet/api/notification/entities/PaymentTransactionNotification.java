package de.sportsbook.activbet.api.notification.entities;

import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class PaymentTransactionNotification {
    private Integer openPayOutCount;
    private BigDecimal openPayOutVolume;
    private ZonedDateTime lastOpenPayOutTime;
    private ZonedDateTime oldestOpenPayOutTime;
    private Integer openPayInCount;
    private BigDecimal openPayInVolume;
    private ZonedDateTime lastOpenPayInTime;
    private ZonedDateTime oldestOpenPayInTime;
}
