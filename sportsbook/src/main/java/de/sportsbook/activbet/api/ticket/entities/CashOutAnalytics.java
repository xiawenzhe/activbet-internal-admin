package de.sportsbook.activbet.api.ticket.entities;

import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Data
public class CashOutAnalytics {
    private Integer openCount;
    private BigDecimal openStake;
    private BigDecimal openCashOut;
    private BigDecimal openCashFlow;
    private Integer lostCount;
    private BigDecimal lostStake;
    private BigDecimal lostCashOut;
    private BigDecimal lostCashFlow;
    private Integer wonCount;
    private Integer partialWonCount;
    private BigDecimal wonStake;
    private BigDecimal wonCashOut;
    private BigDecimal wonMaxPayOut;
    private BigDecimal wonCashFlow;
    private Integer count;
    private BigDecimal stake;
    private BigDecimal cashOut;
    private BigDecimal cashFlow;
    private BigDecimal cashOutAdvantage;

}
