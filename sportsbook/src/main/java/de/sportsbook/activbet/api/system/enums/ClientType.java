package de.sportsbook.activbet.api.system.enums;

import de.sportsbook.serialization.BitFlagEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ClientType implements BitFlagEnum {
    // Enum Set
    NONE(0),
    APPLICATION(1 << 0),
    TERMINAL(1 << 1),
    CASH(1 << 2),
    AGENT(1 << 10),
    ADMINISTRATION(1 << 11);

    private final int flag;
}
