package de.sportsbook.activbet.api;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "api")
public class ApiProperties {
    private String baseUrl;
    //private String oddsExportUrl;
    private String user;
    private String password;
}
