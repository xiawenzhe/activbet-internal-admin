package de.sportsbook.activbet.api.system.entities;

import de.sportsbook.activbet.api.system.enums.FilterType;
import lombok.Data;

@Data
public class Browser {
    private String remoteAddress;
    private String userAgent;
}
