package de.sportsbook.activbet.api.company.entities;

import de.sportsbook.activbet.api.customer.entities.Currency;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerAccount {
    private Company company;
    private BigDecimal cashierBalance;
    private BigDecimal customerBalance;
    private Currency currency;
    private BigDecimal cashierBalanceSystem;
    private BigDecimal customerBalanceSystem;
}
