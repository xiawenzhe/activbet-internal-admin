package de.sportsbook.activbet.api.company.entities;

import de.sportsbook.activbet.api.customer.entities.Currency;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class CashierReport {
    private String name;
    private ZonedDateTime from;
    private ZonedDateTime until;
    private Boolean daily;
    private Currency currency;
    private BigDecimal balanceStart;
    private BigDecimal balanceEnd;
    private CashierReportCountValue totalUnpaid;

    private CashierReportSub sports;
    private List<CashierReportSub> externals;
    private CashierReportSub prePayments;
    private CashierReportSub payOutRequests;
    private CashierReportSub customerBookings;
    private CashierReportSub cashierBookings;

    private List<CashierReport> cashiers;
}
