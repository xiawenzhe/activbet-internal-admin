package de.sportsbook.activbet.api;

import de.sportsbook.activbet.api.oauth.entities.OauthAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

public class BearerApi extends Api {

    @Autowired
    private OauthAuthenticationProvider authenticationProvider;

    @Autowired
    protected ApiProperties configuration;

    @Override
    protected HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        authenticationProvider.getAuthentication()
                .ifPresent(auth -> headers.add("Authorization", "Bearer " + auth.getAccessToken()));
        headers.setAccept(Collections.singletonList(getAccept()));
        return headers;
    }

    protected ApiMediaType getAccept() {
        return new ApiMediaType("customer", "v10");
    }

    protected UriComponentsBuilder bearApiUriBuilder() {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(configuration.getBaseUrl());
        uriBuilder.queryParam("api-version", "1.0");
        return uriBuilder;
    }

}
