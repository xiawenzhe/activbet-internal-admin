package de.sportsbook.activbet.api.customer.entities;

import lombok.Data;

@Data
public class UserSettings {
    private CustomerSetting failedPasswordWaitDuration;
    private CustomerSetting lockedOutWaitDuration;
    private CustomerSetting maxInvalidPasswordAttempts;
    private CustomerSetting securityTokenAvailabilityDuration;
    private CustomerSetting updateSecurityTokenAvailabilityTimeByRequest;
}
