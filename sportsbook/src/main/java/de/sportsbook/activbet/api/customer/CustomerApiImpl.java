package de.sportsbook.activbet.api.customer;

import de.sportsbook.activbet.api.BearerApi;
import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.entities.CustomerAccountBooking;
import de.sportsbook.activbet.api.customer.entities.CustomerMessage;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.api.customer.entities.CustomerLog.CustomerLogPage;
import de.sportsbook.activbet.api.customer.enums.*;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Objects;

@Slf4j
@Service
public class CustomerApiImpl extends BearerApi implements CustomerApi {

    @Override
    public PageWrapper<Customer> getCustomers(Integer pageIndex, Integer pageSize, Integer companyId, String search, CustomerType customerType)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("Customers");
        if (Objects.nonNull(pageIndex)) {
            uri.queryParam("pageIndex", pageIndex);
        }

        if (Objects.nonNull(pageSize)) {
            uri.queryParam("pageSize", pageSize);
        }

        if (Objects.nonNull(companyId)) {
            uri.queryParam("companyId", companyId);
        }

        if (!StringUtils.isEmpty(search)) {
            uri.queryParam("search", search);
        }

        if (Objects.nonNull(customerType)) {
            uri.queryParam("userType", customerType.getFlag());
        }

        try {
            return get(uri.build(), Customer.CustomersPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /Customers", e);
            throw new ApiErrorException("get /Customers", e, uri.toString(), "");
        }
    }

    /**
    * get customer forgot password logs
    * */
    @Override
    public PageWrapper<CustomerLog> getCustomerLogs(CustomerLogType logType, Integer pageIndex, Integer pageSize)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("userlogs");
        if (Objects.nonNull(logType)) {
            uri.queryParam("type", logType.getFlag());
        }
        if (Objects.nonNull(pageIndex)) {
            uri.queryParam("pageIndex", pageIndex);
        }
        if (Objects.nonNull(pageSize)) {
            uri.queryParam("pageSize", pageSize);
        }

        String url = uri.toUriString();
        String body = "";

        try {
            return get(uri.build(), CustomerLogPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /userlogs", e);
            throw new ApiErrorException("get /userlogs", e, url, body);
        }
    }

    /**
        get customer account bookings
     */
    @Override
    public PageWrapper<CustomerAccountBooking> getCustomerAccountBookings(
        Integer pageIndex,
        Integer pageSize,
        Integer customerId,
        String searchKeyWord,
        String from,
        String to,
        CustomerAccountBookingType type,
        CustomerAccountBookingReason reason,
        Boolean onlyTickets,
        Boolean onlyPayments,
        Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("CustomerBookings", "page");

        if (Objects.nonNull(pageIndex)) {
            uri.queryParam("page", pageIndex);
        }

        if (Objects.nonNull(pageSize)) {
            uri.queryParam("size", pageSize);
        }

        if (Objects.nonNull(customerId)) {
            uri.queryParam("customerId", customerId);
        }

        if (!StringUtils.isEmpty(searchKeyWord)) {
            uri.queryParam("search", searchKeyWord);
        }

        if (Objects.nonNull(from)) {
            uri.queryParam("from", from);
        }

        if (Objects.nonNull(to)) {
            uri.queryParam("until", to);
        }

        if (Objects.nonNull(type)) {
            uri.queryParam("type", type.getFlag());
        }

        if (Objects.nonNull(reason)) {
            uri.queryParam("reason", reason.getFlag());
        }

        if (Objects.nonNull(onlyTickets)) {
            uri.queryParam("onlyTickets", onlyTickets);
        }

        if (Objects.nonNull(onlyPayments)) {
            uri.queryParam("onlyPayments", onlyPayments);
        }

        if (Objects.nonNull(orderDescend)) {
            uri.queryParam("sortAscending", !orderDescend);
        }

        try {
            return get(uri.build(), CustomerAccountBooking.CustomerAccountBookingPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /CustomerBookings/page", e);
            throw new ApiErrorException("get /CustomerBookings/page", e, uri.toUriString(), "");
        }
    }

    @Override
    public Customer getCustomerByUsername(String username, CustomerType customerType)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("Customers", "username", username);
        if (Objects.nonNull(customerType)) {
            uri.queryParam("userType", customerType.getFlag());
        }

        try {
            return get(uri.build(), Customer.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /Customers/username", e);
            throw new ApiErrorException("get /Customers/username", e, uri.toUriString(), "");
        }
    }

    /**
        write booking to customer account
     */
    @Override
    public CustomerAccountBooking postForPatchCustomerBooking(Integer customerId, BigDecimal value, String text)
        throws ApiErrorException, UnauthorizedUserException
    {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("CustomerBookings", "patch");
        uri.queryParam("customerId", customerId);
        uri.queryParam("value", value);
        uri.queryParam("text", text);

        try {
            return post(uri.build(), CustomerAccountBooking.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("post /CustomerBookings/patch", e);
            throw new ApiErrorException("post /CustomerBookings/patch", e, uri.toUriString(), "");
        }
    }

    /**
        get customer by id
     */
    @Override
    public Customer getCustomerById(Integer customerId) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("Customers", String.valueOf(customerId));
        try {
            return get(uri.build(), Customer.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /Customers/" + customerId, e);
            throw new ApiErrorException("get /Customers/" + customerId, e, uri.toUriString(), "");
        }
    }

    /**
     * get contacts
     * */
    @Override
    public PageWrapper<CustomerMessage> getCustomerContact(
        Integer pageIndex, Integer pageSize,
        String from, String to, Boolean isUnread, Boolean isClosed
    ) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("contacts");
        uri.queryParam("from", from);
        uri.queryParam("until", to);
        uri.queryParam("pageIndex", pageIndex);
        uri.queryParam("pageSize", pageSize);
        if (Objects.nonNull(isUnread)) {
            uri.queryParam("unread", isUnread);
        }
        if (Objects.nonNull(isClosed)) {
            uri.queryParam("closed", isClosed);
        }

        try {
            return get(uri.build(), CustomerMessage.CustomerContactPage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("get /contacts", e);
            throw new ApiErrorException("get /contacts", e, uri.toUriString(), "");
        }
    }

    /**
        start working on messages
     */
    @Override
    public CustomerMessage putCustomerContact(@NonNull Integer id) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("contacts", String.valueOf(id));

        try {
            return put(uri.build(), null, CustomerMessage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("put /contacts/" + id, e);
            throw new ApiErrorException("put /contacts/" + id, e, uri.toUriString(), "");
        }
    }

    /**
        close message
     */
    @Override
    public CustomerMessage deleteCustomerContact(@NonNull Integer id) throws ApiErrorException, UnauthorizedUserException {
        UriComponentsBuilder uri = this.bearApiUriBuilder().pathSegment("contacts", String.valueOf(id));

        try {
            return delete(uri.build(), CustomerMessage.class);
        } catch (UnauthorizedUserException e) {
            throw e;
        } catch (Exception e) {
            log.error("delete /contacts/" + id, e);
            throw new ApiErrorException("delete /contacts/" + id, e, uri.toUriString(), "");
        }
    }

}
