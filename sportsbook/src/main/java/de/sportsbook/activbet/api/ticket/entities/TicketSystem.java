package de.sportsbook.activbet.api.ticket.entities;

import de.sportsbook.activbet.api.PageWrapper;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class TicketSystem {
    private Boolean isWin;
    private Integer minSystem;
    private Integer maxSystem;
    private Integer betCount;
    private Integer bankCount;
    private BigDecimal stake;
    private BigDecimal stakeFee;
    private BigDecimal payIn;
    private BigDecimal maxOdd;
    private BigDecimal maxWin;
    private BigDecimal maxWinFee;
    private BigDecimal maxPayOut;
    private Integer possibleBetCount;
    private BigDecimal possibleOdd;
    private BigDecimal possibleWin;
    private BigDecimal possibleWinFee;
    private BigDecimal possiblePayOut;
    private Integer winBetCount;
    private BigDecimal winOdd;
    private BigDecimal win;
    private BigDecimal winFee;
    private BigDecimal payOut;
}
