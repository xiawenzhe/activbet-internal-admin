package de.sportsbook.activbet.api.system.entities;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class RelationProperty {
    private String name;
    private String value;
}
