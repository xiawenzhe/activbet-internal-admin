package de.sportsbook.activbet.api.customer.entities;

import de.sportsbook.activbet.api.system.enums.StatusType;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class CustomerStatusInfo {
    private StatusType status;
    private ZonedDateTime activeFrom;
    private ZonedDateTime activeTo;
    private Boolean activeNow;
}
