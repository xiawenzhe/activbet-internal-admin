package de.sportsbook.activbet.controllers;

import de.sportsbook.activbet.configuration.BetagoWebProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
@Slf4j
@Controller
public class DownloadController {

    private BetagoWebProperties properties;

    public DownloadController(Environment env, BetagoWebProperties properties) {
        this.properties = properties;

        if (env.acceptsProfiles("dev", "qa", "production")) {
            // Set font cache folder to /tmp, else PdfBox would try
            // to use the /var/www directory, which throws an
            // NoPermissionException.
            System.setProperty("pdfbox.fontcache", "/tmp");
        }
    }

}
