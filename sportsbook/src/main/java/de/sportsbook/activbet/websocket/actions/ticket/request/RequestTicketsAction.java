package de.sportsbook.activbet.websocket.actions.ticket.request;

import de.sportsbook.activbet.api.ticket.enums.TicketActive;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.EnumSet;

@Getter
@Setter
@ToString
public class RequestTicketsAction extends Action {
    private String customerUsername;
    private EnumSet<TicketActive> ticketActive;
    //private Integer ticketActive;
    private String ticketNumber;
    private String insertFrom;
    private String insertTo;
    private BigDecimal stakeFrom;
    private BigDecimal stakeTo;
    private Integer currencyId;
    private Integer pageIndex;
    private Integer pageSize;
}
