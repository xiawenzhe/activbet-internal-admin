package de.sportsbook.activbet.websocket.actions.company.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCashierReportAction extends Action {
    private Integer companyId;
    private String from;
    private String to;
    private Integer currencyId;
}
