package de.sportsbook.activbet.websocket.actions.notification.response;

import de.sportsbook.activbet.services.notifications.dtos.CashFlowNotificationDto;
import de.sportsbook.activbet.services.notifications.dtos.MessageNotificationDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceiveCashFlowNotificationAction extends Action {
    private CashFlowNotificationDto notification;
}
