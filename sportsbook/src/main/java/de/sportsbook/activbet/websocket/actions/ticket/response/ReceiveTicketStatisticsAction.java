package de.sportsbook.activbet.websocket.actions.ticket.response;

import de.sportsbook.activbet.services.tickets.dtos.TicketStatisticDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveTicketStatisticsAction extends Action {
    List<TicketStatisticDto> ticketStatistics;
    private String from;
    private String to;
}
