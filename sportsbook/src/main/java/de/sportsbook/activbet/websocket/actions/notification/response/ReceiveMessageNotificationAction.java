package de.sportsbook.activbet.websocket.actions.notification.response;

import de.sportsbook.activbet.services.notifications.dtos.MessageNotificationDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentChannelDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveMessageNotificationAction extends Action {
    private MessageNotificationDto notification;
}
