package de.sportsbook.activbet.websocket.actions.customer.request;

import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingReason;
import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingType;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCustomerAccountBookingAction extends Action {
    private Integer customerId;
    private String customerUsername;
    private String keyWord;
    private String from;
    private String to;
    private CustomerAccountBookingType bookingType;
    private CustomerAccountBookingReason bookingReason;
    private Boolean onlyTickets;
    private Boolean onlyPayments;
    private Boolean orderDescend;

    private Integer pageIndex;
    private Integer pageSize;
}
