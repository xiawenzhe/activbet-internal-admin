package de.sportsbook.activbet.websocket.actions.notification.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCashFlowNotificationAction extends Action {
    private String from;
    private Integer currencyId;
}
