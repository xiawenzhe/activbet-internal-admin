package de.sportsbook.activbet.websocket.actions.customer.response;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.company.dtos.CompanyDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerLogDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveResetPasswordLogsAction extends Action {
    private PaginationFilterDto pagination;
    private List<CustomerLogDto> customerLogs;
}
