package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.ticket.entities.CashOutAnalytics;
import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.exceptions.UpdateTicketStatisticFlagException;
import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.tickets.TicketService;
import de.sportsbook.activbet.services.tickets.dtos.TicketDto;
import de.sportsbook.activbet.services.tickets.dtos.TicketStatisticDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.utils.ApplicationUtils;
import de.sportsbook.activbet.websocket.actions.ticket.request.*;
import de.sportsbook.activbet.websocket.actions.ticket.response.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class TicketRestController extends BaseRestfulController {

    @Autowired
    private TicketService ticketService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_ADD_STATISTIC_FLAG")
    public ReceiveUpdateTicketStatisticFlagAction requestAddStatisticFlag(@RequestBody RequestUpdateTicketStatisticFlagAction action)
        throws UnauthorizedUserException, ApiErrorException, UpdateTicketStatisticFlagException
    {
        Session session = SessionHolder.getSafeSession();
        Boolean isAdmin = session.getAttribute(SessionAttributes.IS_ADMIN_USER);

        if (isAdmin == true) {
            ticketService.setInternRemoveFlag(action.getTicketNumber());
            ReceiveUpdateTicketStatisticFlagAction answer = new ReceiveUpdateTicketStatisticFlagAction();
            answer.setActionId(action.getActionId());
            return answer;
        } else {
            UpdateTicketStatisticFlagException ex = new UpdateTicketStatisticFlagException();
            ex.addPayload("description", "not admin user, not allowed to update ticket");
            throw ex;
        }
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_REMOVE_STATISTIC_FLAG")
    public ReceiveUpdateTicketStatisticFlagAction requestRemoveStatisticFlag(@RequestBody RequestUpdateTicketStatisticFlagAction action)
        throws UpdateTicketStatisticFlagException, ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        Boolean isAdmin = session.getAttribute(SessionAttributes.IS_ADMIN_USER);

        if (isAdmin == true) {
            ticketService.removeInterRemoveFlag(action.getTicketNumber());
            ReceiveUpdateTicketStatisticFlagAction answer = new ReceiveUpdateTicketStatisticFlagAction();
            answer.setActionId(action.getActionId());
            return answer;
        } else {
            UpdateTicketStatisticFlagException ex = new UpdateTicketStatisticFlagException();
            ex.addPayload("description", "not admin user, not allowed to update ticket");
            throw ex;
        }
    }

    /**
        get ticket statistics
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_TICKET_STATISTICS")
    public ReceiveTicketStatisticsAction requestTicketStatistics(@RequestBody RequestTicketStatisticsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();

        // get statistics
        List<TicketStatistics> statistics = ticketService.requestAllTicketStatistics(
            session,
            action.getCustomerUsername(),
            action.getTicketActive(),
            action.getTicketNumber(),
            action.getInsertFrom(),
            action.getInsertTo(),
            action.getStakeFrom(),
            action.getStakeTo(),
            action.getCurrencyId()
        );

        // get shop statistics
        List<TicketStatistics> shopBaseStatistics = ticketService.requestShopBasedTicketStatistics(
            session,
            action.getCustomerUsername(),
            action.getTicketActive(),
            action.getTicketNumber(),
            action.getInsertFrom(),
            action.getInsertTo(),
            action.getStakeFrom(),
            action.getStakeTo(),
            action.getCurrencyId()
        );

        // get shop online statistics
        List<TicketStatistics> shopRegisteredStatistics = ticketService.requestShopRegisteredTicketStatistics(
            session,
            action.getCustomerUsername(),
            action.getTicketActive(),
            action.getTicketNumber(),
            action.getInsertFrom(),
            action.getInsertTo(),
            action.getStakeFrom(),
            action.getStakeTo(),
            action.getCurrencyId()
        );

        // nothing found, direct return null
        if (CollectionUtils.isEmpty(statistics) && CollectionUtils.isEmpty(shopBaseStatistics) && CollectionUtils.isEmpty(shopRegisteredStatistics))  {
            ReceiveTicketStatisticsAction answer = new ReceiveTicketStatisticsAction();
            answer.setActionId(action.getActionId());
            answer.setTicketStatistics(null);
            return answer;
        }

        List<TicketStatisticDto> ticketStatistics = new ArrayList<>();

        statistics.forEach(statistic -> {
            TicketStatisticDto dto = new TicketStatisticDto();
            dto.setAllStatistic(statistic);
            dto.setShopBasedStatistic(
                shopBaseStatistics.stream().filter(based -> statistic.getCompanyId().intValue() == based.getCompanyId()).findAny().orElse(null)
            );
            dto.setShopRegisteredStatistic(
                shopRegisteredStatistics.stream().filter(registered -> statistic.getCompanyId().intValue() == registered.getCompanyId()).findAny().orElse(null)
            );
            ticketStatistics.add(dto);
        });

        /*
        // check if total is calculated
        Boolean hasAll = statistics.stream().filter(s -> 0 == s.getCompanyId()).findAny().isPresent();
        if (!hasAll) {
            TicketStatistics all = new TicketStatistics();
            all.setCompanyId(0);
            all.setCompanyShortcut("Total");
            all.setCompanyName1("Total");
            all.setCurrencyIso("ETB");
            all.setCurrencyDecimalDigits(2);

            TicketStatistics shop = new TicketStatistics();
            shop.setCompanyId(0);
            shop.setCompanyShortcut("Total");
            shop.setCompanyName1("Total");
            shop.setCurrencyIso("ETB");
            shop.setCurrencyDecimalDigits(2);

            TicketStatistics online = new TicketStatistics();
            online.setCompanyId(0);
            online.setCompanyShortcut("Total");
            online.setCompanyName1("Total");
            online.setCurrencyIso("ETB");
            online.setCurrencyDecimalDigits(2);

            ticketStatistics.forEach(s -> {
                all.addNext(s.getAllStatistic());
                shop.addNext(s.getShopBasedStatistic());
                online.addNext(s.getShopRegisteredStatistic());
            });

            all.setRevenuePercent(
                ApplicationUtils.isNotNullAndZero(all.getEvaluatedStake()) ?
                all.getRevenue().multiply(new BigDecimal(100)).divide(all.getEvaluatedStake(), 5, RoundingMode.HALF_UP) : null
            );

            shop.setRevenuePercent(
                ApplicationUtils.isNotNullAndZero(shop.getEvaluatedStake()) ?
                shop.getRevenue().multiply(new BigDecimal(100)).divide(shop.getEvaluatedStake(), 5, RoundingMode.HALF_UP) : null
            );

            online.setRevenuePercent(
                ApplicationUtils.isNotNullAndZero(online.getEvaluatedStake()) ?
                online.getRevenue().multiply(new BigDecimal(100)).divide(online.getEvaluatedStake(), 5, RoundingMode.HALF_UP) : null
            );

            all.setCashFlowPercent(
                ApplicationUtils.isNotNullAndZero(all.getEvaluatedPayIn()) ?
                all.getCashFlow().multiply(new BigDecimal(100)).divide(all.getPayIn(), 5, RoundingMode.HALF_UP) : null
            );

            shop.setCashFlowPercent(
                ApplicationUtils.isNotNullAndZero(shop.getEvaluatedPayIn()) ?
                shop.getCashFlow().multiply(new BigDecimal(100)).divide(shop.getPayIn(), 5, RoundingMode.HALF_UP) : null
            );

            online.setCashFlowPercent(
                ApplicationUtils.isNotNullAndZero(online.getEvaluatedPayIn()) ?
                online.getCashFlow().multiply(new BigDecimal(100)).divide(online.getPayIn(), 5, RoundingMode.HALF_UP) : null
            );

            TicketStatisticDto first = new TicketStatisticDto();
            first.setAllStatistic(all);
            first.setShopBasedStatistic(shop);
            first.setShopRegisteredStatistic(online);

            ticketStatistics.add(0, first);
        }
         */

        ReceiveTicketStatisticsAction answer = new ReceiveTicketStatisticsAction();
        answer.setActionId(action.getActionId());
        answer.setTicketStatistics(ticketStatistics);
        return answer;
    }

    /**
        get tickets
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_TICKETS")
    public ReceiveTicketsAction requestTickets(@RequestBody RequestTicketsAction action)
        throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        PageWrapper<Ticket> page = ticketService.requestTickets(
            session,
            action.getCustomerUsername(),
            action.getTicketActive(),
            action.getTicketNumber(),
            action.getInsertFrom(),
            action.getInsertTo(),
            action.getStakeFrom(),
            action.getStakeTo(),
            action.getCurrencyId(),
            action.getPageIndex(),
            action.getPageSize()
        );

        ReceiveTicketsAction answer = new ReceiveTicketsAction();
        PaginationFilterDto pagination = new PaginationFilterDto(page);
        answer.setPagination(pagination);
        if (!CollectionUtils.isEmpty(page.getItems())) {
            List<TicketDto> tickets = page.getItems().stream().map(TicketDto::new).collect(Collectors.toList());
            answer.setTickets(tickets);

            // request ticket statistics
            List<TicketStatistics> statistics = ticketService.requestTicketsStatistics(
                session,
                action.getCustomerUsername(),
                action.getTicketActive(),
                action.getTicketNumber(),
                action.getInsertFrom(),
                action.getInsertTo(),
                action.getStakeFrom(),
                action.getStakeTo(),
                action.getCurrencyId()
            );

            Optional<TicketStatistics> opt = statistics.stream().filter(s -> 0 == s.getCompanyId()).findAny();
            if (opt.isPresent()) {
                answer.setTicketStatistic(opt.get());
            } else {
                // calculate by myself
                TicketStatistics total = new TicketStatistics();
                total.setCompanyId(0);
                total.setCompanyShortcut("Total");
                total.setCompanyName1("Total");

                statistics.forEach(s -> total.addNext(s));

                total.setRevenuePercent(
                    ApplicationUtils.isNotNullAndZero(total.getEvaluatedStake()) ?
                    total.getRevenue().multiply(new BigDecimal(100)).divide(total.getEvaluatedStake(), 5, RoundingMode.HALF_UP) : null
                );

                total.setCashFlowPercent(
                    ApplicationUtils.isNotNullAndZero(total.getEvaluatedPayIn()) ?
                    total.getCashFlow().multiply(new BigDecimal(100)).divide(total.getPayIn(), 5, RoundingMode.HALF_UP) : null
                );

                answer.setTicketStatistic(total);
            }
        } else {
            answer.setTickets(null);
            answer.setTicketStatistic(null);
        }
        return answer;
    }

    /**
        cancel ticket
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_DELETE_TICKET")
    public ReceiveEvaluateTicketAction requestTickets(@RequestBody RequestEvaluateTicketAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        TicketDto ticket = ticketService.cancelTicket(action.getId());
        ReceiveEvaluateTicketAction answer = new ReceiveEvaluateTicketAction();
        answer.setActionId(action.getActionId());
        answer.setTicket(ticket);
        return answer;
    }

    /**
     * cash out analytics
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/ticket/REQUEST_CASH_OUT_ANALYTICS")
    public ReceiveCashOutAnalyticsAction requestTickets(@RequestBody RequestCashOutAnalyticsAction action)
            throws ApiErrorException, UnauthorizedUserException
    {
        Session session = SessionHolder.getSafeSession();
        CashOutAnalytics cashOut = ticketService.requestTicketCashOutAnalytics(session, action.getFrom(), action.getTo());

        ReceiveCashOutAnalyticsAction answer = new ReceiveCashOutAnalyticsAction();
        answer.setActionId(action.getActionId());
        answer.setCashOutAnalytics(cashOut);
        return answer;
    }

}
