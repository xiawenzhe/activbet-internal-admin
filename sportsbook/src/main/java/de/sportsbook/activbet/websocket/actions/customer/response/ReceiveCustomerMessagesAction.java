package de.sportsbook.activbet.websocket.actions.customer.response;

import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerMessageDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveCustomerMessagesAction extends Action {
    private List<CustomerMessageDto> messages;
    private PaginationFilterDto pagination;
}
