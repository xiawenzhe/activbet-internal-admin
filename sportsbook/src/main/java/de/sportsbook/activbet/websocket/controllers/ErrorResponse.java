package de.sportsbook.activbet.websocket.controllers;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class ErrorResponse {
    // UNAUTHORIZED_USER_EXCEPTION, API_ERROR_EXCEPTION, INTERNAL_ERROR
    private String type;
    private String error;
    private String trace;
    private String apiUrl;
    private String apiBody;
}
