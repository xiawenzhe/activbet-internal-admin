package de.sportsbook.activbet.websocket.actions.payment.response;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReceiveUpdatePayoutRequestAction extends Action {
}
