package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.app.AppService;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.websocket.actions.AckAction;
import de.sportsbook.activbet.websocket.actions.Action;
import de.sportsbook.activbet.websocket.actions.RequestAction;
import de.sportsbook.activbet.websocket.actions.app.response.ReceiveCurrenciesAction;
import de.sportsbook.activbet.websocket.actions.app.response.ReceiveLanguagesAction;
import de.sportsbook.activbet.websocket.actions.app.response.ReceiveTimezonesAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class AppRestController extends BaseRestfulController {

    @Autowired
    private AppService appService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/app/REQUEST_LANGUAGES")
    public ReceiveLanguagesAction requestLanguages(@RequestBody RequestAction action) throws ApiErrorException, UnauthorizedUserException {
        Session session = SessionHolder.getSafeSession();
        ReceiveLanguagesAction answer = new ReceiveLanguagesAction();
        answer.setActionId(action.getActionId());
        answer.setLanguages(appService.getLanguages());
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/app/REQUEST_TIMEZONES")
    public ReceiveTimezonesAction requestTimezones(@RequestBody RequestAction action) {
        ReceiveTimezonesAction answer = new ReceiveTimezonesAction();
        answer.setActionId(action.getActionId());
        answer.setTimezones(appService.getTimezones());
        return answer;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/rest/app/REQUEST_CURRENCIES")
    public ReceiveCurrenciesAction requestCurrencies(@RequestBody RequestAction action) throws ApiErrorException, UnauthorizedUserException {
        ReceiveCurrenciesAction answer = new ReceiveCurrenciesAction();
        answer.setActionId(action.getActionId());
        answer.setCurrencies(appService.getCurrencies());
        return answer;
    }
}
