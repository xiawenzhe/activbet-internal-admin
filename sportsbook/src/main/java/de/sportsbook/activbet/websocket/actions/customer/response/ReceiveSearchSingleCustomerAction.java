package de.sportsbook.activbet.websocket.actions.customer.response;

import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveSearchSingleCustomerAction extends Action {
    private CustomerDto customer;
    private String requestTimeStamp;
}
