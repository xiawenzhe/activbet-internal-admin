package de.sportsbook.activbet.websocket.actions.customer.request;

import de.sportsbook.activbet.api.customer.enums.CustomerMessageStatus;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestCustomerMessagesAction extends Action {
    private Integer pageIndex;
    private Integer pageSize;
    private String from;
    private String to;
    private CustomerMessageStatus messageStatus;
}
