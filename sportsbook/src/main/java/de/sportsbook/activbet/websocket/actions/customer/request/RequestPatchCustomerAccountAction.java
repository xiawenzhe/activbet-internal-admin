package de.sportsbook.activbet.websocket.actions.customer.request;

import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingReason;
import de.sportsbook.activbet.api.customer.enums.CustomerAccountBookingType;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class RequestPatchCustomerAccountAction extends Action {
    private Integer customerId;
    private Integer direction;
    private BigDecimal value;
    private String text;
}
