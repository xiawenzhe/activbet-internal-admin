package de.sportsbook.activbet.websocket.actions.payment.response;

import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentTransactionDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceivePaymentTransactionAction extends Action {
    private PaymentTransactionDto paymentTransaction;

}
