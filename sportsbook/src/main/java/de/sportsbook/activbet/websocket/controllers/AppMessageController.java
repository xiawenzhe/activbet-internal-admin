package de.sportsbook.activbet.websocket.controllers;

import de.sportsbook.activbet.state.SessionHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class AppMessageController {

    @MessageMapping("app/CHECK_SESSION")
    @SendToUser("/queue/session")
    public String checkSession() {
        // Happens before any subscriptions are started. Independent from the normal
        // actions (actionId etc.)
        Session session = SessionHolder.getSession(false);

        if (session == null) {
            return "NACK";
        }
        return "ACK";
    }
}
