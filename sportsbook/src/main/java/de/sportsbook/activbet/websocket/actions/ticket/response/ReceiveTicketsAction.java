package de.sportsbook.activbet.websocket.actions.ticket.response;

import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.services.PaginationFilterDto;
import de.sportsbook.activbet.services.tickets.dtos.TicketDto;
import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ReceiveTicketsAction extends Action {
    private PaginationFilterDto pagination;
    //private List<TicketStatistics> ticketStatistics;
    private TicketStatistics ticketStatistic;
    private List<TicketDto> tickets;
}
