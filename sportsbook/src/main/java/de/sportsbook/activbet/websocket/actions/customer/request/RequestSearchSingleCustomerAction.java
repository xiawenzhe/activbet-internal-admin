package de.sportsbook.activbet.websocket.actions.customer.request;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RequestSearchSingleCustomerAction extends Action {
    private Integer customerId;
}
