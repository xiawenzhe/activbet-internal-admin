package de.sportsbook.activbet.services.tickets.dtos;

import de.sportsbook.activbet.api.ticket.entities.Ticket;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import de.sportsbook.activbet.api.ticket.entities.TicketSystem;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;
import org.apache.tomcat.jni.Local;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class TicketDto {
    private Integer id;
    private String number;
    private Boolean isStandard;
    private Boolean isLive;
    private String insertTime;
    private String cancelTime;
    private String payOutTime;
    private Integer customerUserId;
    private String customerUser;
    private BigDecimal stake;
    private BigDecimal stakeFee;
    private BigDecimal payIn;
    private Integer betCount;
    private BigDecimal maxOdd;
    private BigDecimal maxWin;
    private BigDecimal maxWinFee;
    private BigDecimal maxPayOut;
    private Integer possibleBetCount;
    private BigDecimal possibleOdd;
    private BigDecimal possibleWin;
    private BigDecimal possibleWinFee;
    private BigDecimal possiblePayOut;
    private Integer winBetCount;
    private BigDecimal winOdd;
    private BigDecimal win;
    private BigDecimal winFee;
    private BigDecimal payOut;
    private Boolean isOpen;
    private Boolean isRefund;
    private Boolean isLost;
    private Boolean isWon;
    private Boolean isCancelled;
    private Boolean isEvaluated;
    private Boolean isPayoutReady;
    private Boolean isPayedOut;
    private Boolean isRequest;
    private Boolean isPaymentError;
    private Boolean isCashOut;
    private Boolean isNotStatistic;
    private String currencyIso;
    private Integer currencyDecimalDigits;

    private List<TicketSystem> systems;
    private List<TicketBetDto> bets;

    public TicketDto(Ticket ticket) {
        this.id = ticket.getId();
        this.number = ticket.getNumber();
        this.isStandard = ticket.getIsStandard();
        this.isLive = ticket.getIsLive();
        this.insertTime = LocaleUtils.getDateTimeForUser(ticket.getInsertTime());
        this.cancelTime = LocaleUtils.getDateTimeForUser(ticket.getCancelTime());
        this.payOutTime = LocaleUtils.getDateTimeForUser(ticket.getPayOutTime());
        this.customerUserId = ticket.getCustomerUserId();
        this.customerUser = ticket.getCustomerUser();
        this.stake = ticket.getStake();
        this.stakeFee = ticket.getStakeFee();
        this.payIn = ticket.getPayIn();
        this.betCount = ticket.getBetCount();
        this.maxOdd = ticket.getMaxOdd();
        this.maxWin = ticket.getMaxWin();
        this.maxWinFee = ticket.getMaxWinFee();
        this.maxPayOut = ticket.getMaxPayOut();
        this.possibleBetCount = ticket.getPossibleBetCount();
        this.possibleOdd = ticket.getPossibleOdd();
        this.possibleWin = ticket.getPossibleWin();
        this.possibleWinFee = ticket.getPossibleWinFee();
        this.possiblePayOut = ticket.getPossiblePayOut();
        this.winBetCount = ticket.getWinBetCount();
        this.winOdd = ticket.getWinOdd();
        this.win = ticket.getWin();
        this.winFee = ticket.getWinFee();
        this.payOut = ticket.getPayOut();
        this.isOpen = ticket.getIsOpen();
        this.isRefund = ticket.getIsRefund();
        this.isLost = ticket.getIsLost();
        this.isWon = ticket.getIsWon();
        this.isCancelled = ticket.getIsCancelled();
        this.isEvaluated = ticket.getIsEvaluated();
        this.isPayoutReady = ticket.getIsPayoutReady();
        this.isPayedOut = ticket.getIsPayedOut();
        this.isRequest = ticket.getIsRequest();
        this.isPaymentError = ticket.getIsPaymentError();
        this.isCashOut = ticket.getIsCashout();
        this.isNotStatistic = ticket.getIsNotStatistic();
        this.currencyIso = ticket.getCurrencyIso();
        this.currencyDecimalDigits = ticket.getCurrencyDecimalDigits();

        this.systems = ticket.getSystems();

        if (!CollectionUtils.isEmpty(ticket.getBets())) {
            this.bets = ticket.getBets().stream().map(TicketBetDto::new).collect(Collectors.toList());
        }
    }

}
