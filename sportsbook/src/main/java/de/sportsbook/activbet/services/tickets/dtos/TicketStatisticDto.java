package de.sportsbook.activbet.services.tickets.dtos;

import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.ticket.entities.TicketStatistics;
import lombok.Data;

import java.util.Objects;

@Data
public class TicketStatisticDto {
    private TicketStatistics allStatistic;
    private TicketStatistics shopBasedStatistic;
    private TicketStatistics shopRegisteredStatistic;
}
