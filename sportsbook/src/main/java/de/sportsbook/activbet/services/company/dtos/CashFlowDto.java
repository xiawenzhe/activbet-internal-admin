package de.sportsbook.activbet.services.company.dtos;

import de.sportsbook.activbet.api.company.entities.CashFlow;
import de.sportsbook.activbet.api.company.entities.Company;
import lombok.Data;

import java.util.List;

@Data
public class CashFlowDto {

    // last ticket buy time
    private String lastTicketTime;
    // last deposit time
    private String lastDepositTime;

    // gain shop this week
    // gain shop this month

    // gain online this week
    // gain online this month

    // gain total this week
    // gain totoal this month

    /*
    private CashFlowLastAction lastOnlinePayIn;
    private CashFlowLastAction lastOnlinePayOut;
    private CashFlowLastAction lastShopPayIn;
    private CashFlowLastAction lastShopPayOut;
    private CashFlowLastAction lastAnonymousTicketPayIn;
    private CashFlowLastAction lastAnonymousTicketPayOut;

    private BigDecimal onlinePayInByOnlineCustomer;
    private BigDecimal onlinePayInByShopCustomer;
    private BigDecimal onlinePayIn;
    private BigDecimal shopPayInByOnlineCustomer;
    private BigDecimal shopPayInByShopCustomer;
    private BigDecimal shopPayIn;
    private BigDecimal anonymousTicketPayIn;
    private BigDecimal totalShopPayIn;
    private BigDecimal totalPayIn;
    private BigDecimal onlinePayOutByOnlineCustomer;
    private BigDecimal onlinePayOutByShopCustomer;
    private BigDecimal onlinePayOut;
    private BigDecimal shopPayOutByOnlineCustomer;
    private BigDecimal shopPayOutByShopCustomer;
    private BigDecimal shopPayOut;
    private BigDecimal anonymousTicketPayOut;
    private BigDecimal totalShopPayOut;
    private BigDecimal totalPayOut;
    private BigDecimal onlineBalanceByOnlineCustomer;
    private BigDecimal onlineBalanceByShopCustomer;
    private BigDecimal onlineBalance;
    private BigDecimal shopBalanceByOnlineCustomer;
    private BigDecimal shopBalanceByShopCustomer;
    private BigDecimal shopBalance;
    private BigDecimal anonymousTicketBalance;
    private BigDecimal totalShopBalance;
    private BigDecimal totalBalance;
     */

}
