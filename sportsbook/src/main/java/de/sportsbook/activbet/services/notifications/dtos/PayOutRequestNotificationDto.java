package de.sportsbook.activbet.services.notifications.dtos;

import de.sportsbook.activbet.api.notification.entities.PayOutRequestNotification;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class PayOutRequestNotificationDto {
    private Integer needReleaseCount;
    private BigDecimal needReleaseVolume;
    private String lastRequestTime;
    private String oldestRequestTime;

    public PayOutRequestNotificationDto(PayOutRequestNotification notification) {
        this.needReleaseCount = notification.getNeedReleaseCount();
        this.needReleaseVolume = notification.getNeedReleaseVolume();
        this.lastRequestTime = LocaleUtils.getDateTimeForUser(notification.getLastRequestTime());
        this.oldestRequestTime = LocaleUtils.getDateTimeForUser(notification.getLastRequestTime());
    }
}
