package de.sportsbook.activbet.services.company;

import de.sportsbook.activbet.api.company.CompanyApi;
import de.sportsbook.activbet.api.company.entities.CashFlow;
import de.sportsbook.activbet.api.company.entities.CashierReport;
import de.sportsbook.activbet.api.company.entities.Company;
import de.sportsbook.activbet.api.customer.CustomerApi;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.company.dtos.CashFlowDto;
import de.sportsbook.activbet.services.company.dtos.CompanyDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.session.Session;

import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Slf4j
@Service
public class CompanyService {

    @Autowired
    private CompanyApi companyApi;

    @Autowired
    private CustomerApi customerApi;

    /**
        request company list
     */
    public List<CompanyDto> requestAllCompanies() throws ApiErrorException, UnauthorizedUserException {
        List<Company> companies = companyApi.getCompanies(null);
        List<CompanyDto> list = new ArrayList<>();

        if (Objects.nonNull(companies)) {
            // remove test company
            //companies.removeIf(c -> 5 == c.getId() || 6 == c.getId() );

            // only show last level
            companies.forEach(c -> {
                if ( !companies.stream().filter(c2 -> Objects.nonNull(c2.getParentId()) && c.getId().intValue() == c2.getParentId()).findAny().isPresent() ) {
                    list.add(new CompanyDto(c));
                }
            });
        }

        return list;
    }

    /**
        request all companies
     */
    public List<CompanyDto> requestCompanyTree() throws ApiErrorException, UnauthorizedUserException {
        List<Company> companies = companyApi.getCompanies(null);

        // remove test company
        /*
        if (Objects.nonNull(companies)) {
            companies.removeIf(c -> 5 == c.getId() || 6 == c.getId() );
        }
         */

        List<CompanyDto> companyDtos = new ArrayList<>();

        if (!CollectionUtils.isEmpty(companies)) {
            companyDtos = companies.stream().filter(
                company -> {
                    Integer parentId = company.getParentId();
                    if (Objects.isNull(parentId)) {
                        return true;
                    }
                    return !companies.stream().filter(c -> parentId.intValue() == c.getId()).findAny().isPresent();
                }
            ).map(CompanyDto::new).collect(Collectors.toList());
            companyDtos.forEach(companyDto -> companyDto.setLevel(0));
            this.buildCompanies(companyDtos, companies);
        }
        return companyDtos;
    }

    public Company requestCompanyByCompanyId(Integer companyId) throws ApiErrorException, UnauthorizedUserException {
        List<Company> companies = companyApi.getCompanies(null);
        return companies.stream().filter(c -> companyId.intValue() == c.getId()).findAny().orElse(null);
    }

    private void buildCompanies(List<CompanyDto> dtos, List<Company> companies) {
        dtos.forEach(dto -> {
            List<CompanyDto> children = companies.stream().filter(company -> Objects.equals(dto.getId(), company.getParentId())).map(CompanyDto::new).collect(Collectors.toList());
            children.forEach(child -> child.setLevel(dto.getLevel() + 1));
            dto.setCompanies(children);
            this.buildCompanies(children, companies);
        });
    }

    /**
    * get cashier reports
    * */
    public CashierReport requestCashierReport(Session session, Integer companyId, String from, String to)
        throws ApiErrorException, UnauthorizedUserException
    {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String fromStr = LocaleUtils.getDateTimeForLocal(timezone, from);
        String toStr = LocaleUtils.getDateTimeForLocal(timezone, to);

        return companyApi.postForCashierReports(companyId, fromStr, toStr);
    }

    /**
    * get cash flow report of current week
    *
    public CashFlowDto requestCashFlow(Session session, Integer currencyId) throws ApiErrorException, UnauthorizedUserException {
        // current week
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        LocalDate monday = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        ZonedDateTime day = monday.atStartOfDay(ZoneId.of(timezone));
        String from = day.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
        CashFlow weekCashFlow = companyApi.getCashFlow(from, currencyId);

        // month
        ZonedDateTime day2 = ZonedDateTime.now(ZoneId.of(timezone)).withDayOfMonth(1);
        String from2 = day2.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
        CashFlow monthCashFlow = companyApi.getCashFlow(from, currencyId);

        CashFlowDto cashFlow = new CashFlowDto();
        return cashFlow;
    }
     */
}
