package de.sportsbook.activbet.services.oauth;

import de.sportsbook.activbet.api.oauth.entities.CustomPasswordAccessTokenProvider;
import de.sportsbook.activbet.api.oauth.enums.LogoutReason;
import de.sportsbook.activbet.api.oauth.OauthApi;
import de.sportsbook.activbet.api.oauth.entities.OauthAuthentication;
import de.sportsbook.activbet.configuration.OauthProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service
public class OauthService {

    @Autowired
    protected OauthProperties oauthProperties;

    @Autowired
    private OauthApi oauthApi;

    private ResourceOwnerPasswordResourceDetails initDetails() {
        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();
        resource.setAccessTokenUri(getAccessTokenUri());
        resource.setClientAuthenticationScheme(AuthenticationScheme.header);
        return resource;
    }

    private String getAccessTokenUri() {
        return UriComponentsBuilder.fromHttpUrl(oauthProperties.getBaseUrl()).pathSegment("oauth", "token").toUriString();
    }

    public void logout(String username, String remoteAddress, String userAgent, LogoutReason logoutReason) {
        oauthApi.postLogout(username, remoteAddress, userAgent, logoutReason);
    }

    // ResourceOwnerPasswordResourceDetails details,
    public OAuth2AccessToken authenticate(String username, String password, String ipAddress, String userAgent) throws OAuth2AccessDeniedException {
        ResourceOwnerPasswordResourceDetails details = initDetails();
        String clientId = oauthProperties.getClientId();
        String clientSecret = oauthProperties.getClientSecret();
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        details.setUsername(username);
        details.setPassword(password);

        ResourceOwnerPasswordAccessTokenProvider provider = new CustomPasswordAccessTokenProvider(ipAddress, userAgent);
        DefaultAccessTokenRequest request = new DefaultAccessTokenRequest();
        return provider.obtainAccessToken(details, request);
    }

    public OAuth2AccessToken refreshAuthentication(OauthAuthentication authentication, String ipAddress, String userAgent) {
        ResourceOwnerPasswordResourceDetails details = initDetails();
        details.setClientId(oauthProperties.getClientId());
        details.setClientSecret(oauthProperties.getClientSecret());
        ResourceOwnerPasswordAccessTokenProvider provider = new CustomPasswordAccessTokenProvider(ipAddress, userAgent);
        DefaultAccessTokenRequest request = new DefaultAccessTokenRequest();
        return provider.refreshAccessToken(details, new DefaultOAuth2RefreshToken(authentication.getRefreshToken()), request);
    }
}
