package de.sportsbook.activbet.services.notifications.dtos;

import de.sportsbook.activbet.api.notification.entities.PaymentTransactionNotification;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentTransactionNotificationDto {

    private Integer openPayOutCount;
    private BigDecimal openPayOutVolume;
    private String lastOpenPayOutTime;
    private String oldestOpenPayOutTime;
    private Integer openPayInCount;
    private BigDecimal openPayInVolume;
    private String lastOpenPayInTime;
    private String oldestOpenPayInTime;

    public PaymentTransactionNotificationDto(PaymentTransactionNotification notification) {
        this.openPayOutCount = notification.getOpenPayOutCount();
        this.openPayOutVolume = notification.getOpenPayOutVolume();
        this.lastOpenPayOutTime = LocaleUtils.getDateTimeForUser(notification.getLastOpenPayOutTime());
        this.oldestOpenPayOutTime = LocaleUtils.getDateTimeForUser(notification.getOldestOpenPayOutTime());

        this.openPayInCount = notification.getOpenPayInCount();
        this.openPayInVolume = notification.getOpenPayInVolume();
        this.lastOpenPayInTime = LocaleUtils.getDateTimeForUser(notification.getLastOpenPayInTime());
        this.oldestOpenPayInTime = LocaleUtils.getDateTimeForUser(notification.getOldestOpenPayInTime());
    }
}