package de.sportsbook.activbet.services.customer.dtos;

import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.api.customer.enums.CustomerLogType;
import de.sportsbook.activbet.api.system.enums.ClientType;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class CustomerLogDto {

    private Integer id;
    private String insertTime;
    private String userLogType;
    private List<String> clientType;
    private String remoteAddress;
    private String hostAddress;
    private String userAgent;
    private Boolean successful;
    private String message;
    private String errorMessage;
    private Boolean isDeprecated;

    public CustomerLogDto(CustomerLog log) {
        this.id = log.getId();
        this.insertTime = LocaleUtils.getDateTimeForUser(log.getInsertTime());

        if (Objects.nonNull(log.getUserLogType())) {
            this.userLogType = log.getUserLogType().name();
        }

        if (Objects.nonNull(log.getClientType())) {
            this.clientType = new ArrayList<>();
            log.getClientType().forEach(ct -> clientType.add(ct.name()));
        }

        this.remoteAddress = log.getRemoteAddress();
        this.hostAddress = log.getHostAddress();
        this.userAgent = log.getUserAgent();
        this.successful = log.getSuccessful();
        this.message = log.getMessage();
        this.errorMessage = log.getErrorMessage();

        this.isDeprecated = log.getIsDeprecated();
    }
}
