package de.sportsbook.activbet.services.customer.dtos;

import de.sportsbook.activbet.api.customer.entities.Customer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Data
public class CustomerDto {
    private Integer userId;
    private Integer companyId;
    private String companyShortcut;
    private String username;
    private String email;
    private String mobile;
    private String firstName;
    private String lastName;
    private String customerType;
    private BigDecimal balance;
    private String currencyIso;
    private Boolean activeNow;

    public CustomerDto(Customer customer) {
        if (Objects.nonNull(customer.getTechnicalData())) {
            this.userId = customer.getTechnicalData().getId();
        } else {
            this.userId = customer.getId();
        }
        this.username = customer.getUsername();
        this.email = customer.getEmail();
        this.firstName = customer.getFirstName();
        this.lastName = customer.getLastName();
        if (Objects.nonNull(customer.getUserType())) {
            this.customerType = customer.getUserType().name();
        }

        this.balance = customer.getBalance();

        if (Objects.nonNull(customer.getCurrency())) {
            this.currencyIso = customer.getCurrency().getIso();
        }

        if (Objects.nonNull(customer.getCompany())) {
            this.companyId = customer.getCompany().getId();
            this.companyShortcut = customer.getCompany().getShortcut();
        }

        if (Objects.nonNull(customer.getStatusInfo())) {
            this.activeNow = customer.getStatusInfo().getActiveNow();
        }

        this.mobile = customer.getMobile();
    }
}
