package de.sportsbook.activbet.services.customer;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.CustomerApi;
import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.entities.CustomerAccountBooking;
import de.sportsbook.activbet.api.customer.entities.CustomerMessage;
import de.sportsbook.activbet.api.customer.entities.CustomerLog;
import de.sportsbook.activbet.api.customer.enums.*;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.customer.dtos.CustomerAccountBookingDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerMessageDto;
import de.sportsbook.activbet.services.customer.dtos.CustomerDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class CustomerService {

    @Autowired
    private CustomerApi customerApi;

    /**
        get Customers
     */
    public PageWrapper<Customer> requestCustomers(Integer pageIndex, Integer pageSize, Integer companyId, String search, CustomerType customerType)
        throws ApiErrorException, UnauthorizedUserException
    {
        PageWrapper<Customer> customers = customerApi.getCustomers(pageIndex, pageSize, companyId, search, customerType);
        return customers;
    }

    /**
        get reset password logs
     */
    public PageWrapper<CustomerLog> requestCustomerResetPasswordLogs(Integer pageIndex, Integer pageSize)
        throws ApiErrorException, UnauthorizedUserException
    {
        // get logs
        PageWrapper<CustomerLog> result = customerApi.getCustomerLogs(CustomerLogType.RESET_PASSWORD, pageIndex, pageSize);
        List<CustomerLog> resultLogs = result.getItems();
        resultLogs.forEach(resultLog -> resultLog.setKeyWord(this.getLogKeyWord(resultLog.getMessage())) );

        // get 1000 logs = 40 pages;
        PageWrapper<CustomerLog> all = customerApi.getCustomerLogs(CustomerLogType.RESET_PASSWORD, 0, 1000);
        List<CustomerLog> allLogs = all.getItems();
        allLogs.forEach(allLog -> allLog.setKeyWord(this.getLogKeyWord(allLog.getMessage())));

        // set deprecated;
        resultLogs.forEach(resultLog -> {
            for (Integer i = 0; i < allLogs.size(); i++) {
                if (resultLog.getId() < allLogs.get(i).getId()) {
                    if (Objects.nonNull(resultLog.getKeyWord()) && Objects.equals(resultLog.getKeyWord(), allLogs.get(i).getKeyWord()) ) {
                        resultLog.setIsDeprecated(true);
                        break;
                    }
                }
            }
        });

        result.setItems(resultLogs);
        return result;
    }

    private String getLogKeyWord(String message) {
        if (Objects.nonNull(message)) {
            String startStr = "from user";
            String endStr = "to temporary";
            Integer startIndex = message.indexOf(startStr);
            Integer endIndex = message.indexOf(endStr);
            if ( (startIndex > 0) && (endIndex > 0) ) {
                return message.substring(startIndex + startStr.length(), endIndex).trim();
            }
        }
        return null;
    }

    /*
        get customer payment transactions
    public PageWrapper<PaymentTransaction> requestCustomerPaymentTransactions(
        Session session,
        Integer pageIndex,
        Integer pageSize,
        Integer paymentProviderId,
        Integer paymentChannelId,
        Integer customerId,
        PaymentDirection direction,
        CustomerPaymentTransactionStatus status,
        Boolean isNotStatus,
        String from,
        String to,
        Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);

        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        return customerApi.getCustomerPaymentTransactions(
            pageIndex,
            pageSize,
            paymentProviderId,
            paymentChannelId,
            customerId,
            direction,
            status,
            isNotStatus,
            localFrom,
            localTo,
            orderDescend
        );
    }
     */

    /**
    * get account bookings
    * */
    public PageWrapper<CustomerAccountBooking> requestCustomerAccountBookings(
        Session session,
        Integer pageIndex,
        Integer pageSize,
        Integer customerId,
        String customerUsername,
        String keyWord,
        String from,
        String to,
        CustomerAccountBookingType type,
        CustomerAccountBookingReason reason,
        Boolean onlyTickets,
        Boolean onlyPayments,
        Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        if (Objects.isNull(customerId)) {
            if (!StringUtils.isEmpty(customerUsername)) {
                Customer customer = customerApi.getCustomerByUsername(customerUsername, CustomerType.CUSTOMER);
                if (Objects.nonNull(customer)) {
                    customerId = customer.getId();
                    if (Objects.isNull(customerId)) {
                        customerId = customer.getTechnicalData().getId();
                    }
                }
            }
        }

        return customerApi.getCustomerAccountBookings(
            pageIndex,
            pageSize,
            customerId,
            keyWord,
            localFrom,
            localTo,
            type,
            reason,
            onlyTickets,
            onlyPayments,
            orderDescend
        );
    }

    public CustomerAccountBookingDto patchCustomerBooking(Integer customerId, Integer direction, BigDecimal value, String text)
        throws ApiErrorException, UnauthorizedUserException
    {
        // tyoe = 0: payin, 1: payout
        if (direction.intValue() == 0) {
            // payin
            if (value.compareTo(BigDecimal.ZERO) < 0) {
                value = BigDecimal.ZERO.subtract(value);
            }
        } else {
            // payout
            if (value.compareTo(BigDecimal.ZERO) > 0) {
                value = BigDecimal.ZERO.subtract(value);
            }
        }

        CustomerAccountBooking booking = customerApi.postForPatchCustomerBooking(customerId, value, text);
        return new CustomerAccountBookingDto(booking);
    }

    public CustomerDto requestCustomerbyId(Integer customerId) throws ApiErrorException, UnauthorizedUserException {
        Customer customer = customerApi.getCustomerById(customerId);
        return new CustomerDto(customer);
    }

    public PageWrapper<CustomerMessage> requestCustomerMessages(
        Session session, Integer pageIndex, Integer pageSize,
        String from, String to, CustomerMessageStatus messageStatus
    ) throws ApiErrorException, UnauthorizedUserException {
        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        Boolean isUnread = null;
        Boolean isClosed = null;

        if (Objects.nonNull(messageStatus)) {
            if (Objects.equals(CustomerMessageStatus.UN_READ, messageStatus)) {
                isUnread = true;
            }

            if (Objects.equals(CustomerMessageStatus.CLOSED, messageStatus)) {
                isClosed = true;
            }
        }
        return customerApi.getCustomerContact(pageIndex, pageSize, localFrom, localTo, isUnread, isClosed);
    }

    public CustomerMessageDto requestUpdateCustomerMessage(Integer id) throws ApiErrorException, UnauthorizedUserException {
        CustomerMessage message = customerApi.putCustomerContact(id);
        if (Objects.nonNull(message)) {
            return new CustomerMessageDto(message);
        }
        return null;
    }

    public CustomerMessageDto requestCloseCustomerMessage(Integer id) throws ApiErrorException, UnauthorizedUserException {
        CustomerMessage message = customerApi.deleteCustomerContact(id);
        if (Objects.nonNull(message)) {
            return new CustomerMessageDto(message);
        }
        return null;
    }

}
