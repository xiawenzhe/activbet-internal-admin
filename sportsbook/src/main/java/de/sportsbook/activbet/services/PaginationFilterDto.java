package de.sportsbook.activbet.services;

import de.sportsbook.activbet.api.PageWrapper;
import lombok.Data;

@Data
public class PaginationFilterDto {
    private Integer pageIndex;
    private Integer pageSize;
    private Integer totalCount;
    private Integer totalPages;
    //private Boolean hasPreviousPage;
    //private Boolean hasNextPage;

    private Boolean showPreviousButton;
    private Boolean showNextButton;

    public PaginationFilterDto(PageWrapper wrapper) {
        this.pageIndex = wrapper.getPageIndex();
        this.pageSize = wrapper.getPageSize();
        this.totalCount = wrapper.getTotalCount();
        this.totalPages = wrapper.getTotalPages();
        //this.hasPreviousPage = wrapper.getHasPreviousPage();
        //this.hasNextPage = wrapper.getHasNextPage();
        this.showPreviousButton = wrapper.getHasPreviousPage();
        this.showNextButton = wrapper.getHasNextPage();
    }

}
