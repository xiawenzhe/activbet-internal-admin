package de.sportsbook.activbet.services.payment;

import de.sportsbook.activbet.api.PageWrapper;
import de.sportsbook.activbet.api.customer.CustomerApi;
import de.sportsbook.activbet.api.customer.entities.Customer;
import de.sportsbook.activbet.api.customer.enums.CustomerType;
import de.sportsbook.activbet.api.payment.PaymentApi;
import de.sportsbook.activbet.api.payment.entities.PaymentChannel;
import de.sportsbook.activbet.api.payment.entities.PaymentProvider;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction;
import de.sportsbook.activbet.api.payment.entities.PaymentTransaction.PaymentTransactionPage;
import de.sportsbook.activbet.api.payment.entities.PayOutRequest.PayOutRequestPage;
import de.sportsbook.activbet.api.payment.enums.PayOutRequestFlag;
import de.sportsbook.activbet.api.payment.enums.PaymentDirection;
import de.sportsbook.activbet.api.payment.enums.PaymentTransactionStatus;
import de.sportsbook.activbet.api.payment.enums.PayOutRequestStatus;
import de.sportsbook.activbet.exceptions.ApiErrorException;
import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.services.payment.dtos.PaymentChannelDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentProviderDto;
import de.sportsbook.activbet.services.payment.dtos.PaymentTransactionDto;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PaymentService {

    @Autowired
    private PaymentApi paymentApi;

    @Autowired
    private CustomerApi customerApi;

    /**
     *  request payment providers
     *  */
    public List<PaymentProviderDto> requestPaymentProviderList() throws ApiErrorException, UnauthorizedUserException {
        PageWrapper<PaymentProvider> page = paymentApi.getPaymentProviders(0, 1000, null);
        return page.getItems().stream().map(PaymentProviderDto::new).collect(Collectors.toList());
    }

    /**
     *  request payment channels
     *  */
    public List<PaymentChannelDto> requestPaymentChannels() throws ApiErrorException, UnauthorizedUserException {
        PageWrapper<PaymentChannel> page = paymentApi.getPaymentChannels(0, 1000, null, null);
        List<PaymentChannel> channels = page.getItems();
        if (Objects.isNull(channels)) {
            return new ArrayList<>();
        }
        return channels.stream().map(PaymentChannelDto::new).collect(Collectors.toList());
    }

    /**
     * request payment transactions
     * */
    public PaymentTransactionPage requestPaymentTransactions(
        Session session,
        Integer paymentProviderId,
        Integer paymentChannelId,
        Integer customerId,
        String customerUsername,
        PaymentDirection direction,
        PaymentTransactionStatus status,
        String from, String to,
        Integer pageIndex, Integer pageSize, Boolean orderDescend
    ) throws ApiErrorException, UnauthorizedUserException {
        if (Objects.isNull(customerId)) {
            if (!StringUtils.isEmpty(customerUsername)) {
                Customer customer = customerApi.getCustomerByUsername(customerUsername, CustomerType.CUSTOMER);
                // customer not found
                if (Objects.isNull(customer)) {
                    customerId = -1;
                } else {
                    customerId = customer.getTechnicalData().getId();
                }
            }
        }

        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        return paymentApi.getPaymentTransactions(
            pageIndex, pageSize,
            paymentProviderId, paymentChannelId, customerId,
            direction, status, false,
            localFrom, localTo, orderDescend
        );
    }

    /**
     * request pay out requests
     * */
    public PayOutRequestPage requestPayOutRequests(
        Session session,
        Integer companyId,
        Integer customerId,
        String customerUsername,
        Integer payOutUserId,
        PayOutRequestStatus status,
        PayOutRequestFlag flag,
        String keyWord,
        Boolean orderDescending,
        String from, String to,
        Integer pageIndex, Integer pageSize
    ) throws ApiErrorException, UnauthorizedUserException {
        if (Objects.isNull(customerId)) {
            if (!StringUtils.isEmpty(customerUsername)) {
                Customer customer = customerApi.getCustomerByUsername(customerUsername, CustomerType.CUSTOMER);
                // customer not found
                if (Objects.isNull(customer)) {
                    customerId = -2;
                } else {
                    customerId = customer.getTechnicalData().getId();
                }
            }
        }

        String timezone = session.getAttribute(SessionAttributes.TIMEZONE);
        String localFrom = LocaleUtils.getDateTimeForLocal(timezone, from);
        String localTo = LocaleUtils.getDateTimeForLocal(timezone, to);

        return paymentApi.getPayOutRequests(
            pageIndex, pageSize,
            companyId, customerId, payOutUserId, status, flag,
            keyWord, localFrom, localTo, orderDescending
        );
    }

    /**
     * handle payment transaction
     * */
    public PaymentTransactionDto requestHandlePaymentTransaction(
          Session session,
          Integer id,
          Boolean allow
    ) throws ApiErrorException, UnauthorizedUserException {
        String userAgent = session.getAttribute(SessionAttributes.USER_AGENT);
        String remoteAddress = session.getAttribute(SessionAttributes.REMOTE_ADDRESS);

        if (allow) {
            PaymentTransaction transaction = paymentApi.putAcceptPaymentTransaction(id, remoteAddress, userAgent);
            return new PaymentTransactionDto(transaction);
        } else {
            PaymentTransaction transaction = paymentApi.putDeclinePaymentTransaction(id, remoteAddress, userAgent);
            return new PaymentTransactionDto(transaction);
        }
    }

    /**
     * sync payment transaction
     * */
    public PaymentTransactionDto requestSyncPaymentTransaction(Integer id) throws ApiErrorException, UnauthorizedUserException {
        PaymentTransaction transaction = paymentApi.putSyncPaymentTransaction(id);
        return new PaymentTransactionDto(transaction);
    }

    /**
     * handle payout requests
     * */
    public void requestHandlePayOutRequest(
            Integer id,
            Boolean allow
    ) throws ApiErrorException, UnauthorizedUserException {
        if (allow) {
            paymentApi.putAcceptPayOutRequest(id);
        } else {
            paymentApi.deleteForDenyPayOutRequest(id);
        }
    }
}
