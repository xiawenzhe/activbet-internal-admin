package de.sportsbook.activbet.services.payment.dtos;

import de.sportsbook.activbet.api.payment.entities.PaymentChannel;
import de.sportsbook.activbet.api.payment.entities.PaymentProvider;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.util.Objects;

@Data
public class PaymentChannelDto {

    private Integer id;
    private String iconName;
    private String name;
    private String labelName;
    private String paymentDirection; // DEPOSIT, WITHDRAW
    private String paymentChannelFlag;  // NONE, PAY_IN_ALLOWED, PAY_OUT_ALLOWED, PAY_OUT_WITH_APPROVE, PAY_OUT_ONLY_THROUGH_PAY_IN_CHANNEL
    private String paymentChannelType; // PAY_IN, PAY_OUT
    private String paymentChannelPayMethod; // USSD ...
    private Boolean payOutWithApprove;
    private Boolean payOutOnlyThroughPayInChannel;
    private Integer paymentProviderId;
    private String paymentProviderName;
    private String insertTime;
    private Boolean activeNow;

    public PaymentChannelDto(PaymentChannel channel) {
        if (Objects.nonNull(channel.getTechnicalData())) {
            this.id = channel.getTechnicalData().getId();
            this.insertTime = LocaleUtils.getDateTimeForUser(channel.getTechnicalData().getInsertTime());
        }

        this.iconName = channel.getIconName();
        this.name = channel.getName();
        this.labelName = channel.getLabelName();

        if (Objects.nonNull(channel.getPaymentDirection())) {
            this.paymentDirection = channel.getPaymentDirection().name();
        }

        if (Objects.nonNull(channel.getPaymentChannelFlag())) {
            this.paymentChannelFlag = channel.getPaymentChannelFlag().name();
        }

        if (Objects.nonNull(channel.getPaymentChannelType())) {
            this.paymentChannelType = channel.getPaymentChannelType().name();
        }

        this.paymentProviderId = channel.getPaymentProviderId();

        PaymentProvider provider = channel.getPaymentProvider();
        if (Objects.nonNull(provider)) {
            if (Objects.isNull(paymentProviderId) && Objects.nonNull(provider.getTechnicalData()) ) {
                this.paymentProviderId = provider.getTechnicalData().getId();
            }
            this.paymentProviderName = provider.getName();
        }

        if (Objects.nonNull(channel.getStatusInfo())) {
            this.activeNow = channel.getStatusInfo().getActiveNow();
        }

        this.payOutWithApprove = channel.getPayOutWithApprove();
        this.payOutOnlyThroughPayInChannel = channel.getPayOutOnlyThroughPayInChannel();

        if (Objects.nonNull(channel.getPaymentChannelPayMethod())) {
            this.paymentChannelPayMethod = channel.getPaymentChannelPayMethod().name();
        }
    }
}
