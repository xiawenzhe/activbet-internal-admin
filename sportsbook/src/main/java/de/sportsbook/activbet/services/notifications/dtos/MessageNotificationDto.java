package de.sportsbook.activbet.services.notifications.dtos;

import de.sportsbook.activbet.api.notification.entities.MessageNotification;
import de.sportsbook.activbet.utils.LocaleUtils;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class MessageNotificationDto {
    private Integer unreadAnonymous;
    private Integer unreadCustomer;
    private Integer unread;
    private Integer progressedAnonymous;
    private Integer progressedCustomer;
    private Integer progressed;
    private Integer contactsActualMonth;
    private Integer contactsActualWeek;
    private Integer contactsYesterday;
    private Integer contactsToday;
    private String lastContactInsertTime;

    public MessageNotificationDto(MessageNotification notification) {

        this.unreadAnonymous = notification.getUnreadAnonymous();
        this.unreadCustomer = notification.getUnreadCustomer();
        this.unread = notification.getUnread();
        this.progressedAnonymous = notification.getProgressedAnonymous();
        this.progressedCustomer = notification.getProgressedCustomer();
        this.progressed = notification.getProgressed();
        this.contactsActualMonth = notification.getContactsActualMonth();
        this.contactsActualWeek = notification.getContactsActualWeek();
        this.contactsYesterday = notification.getContactsYesterday();
        this.contactsToday = notification.getContactsToday();
        this.lastContactInsertTime = LocaleUtils.getDateTimeForUser(notification.getLastContactInsertTime());


    }

}
