package de.sportsbook.activbet.exceptions;

import lombok.Getter;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class PayloadException extends Exception {

    private final Map<String, Object> payload = new LinkedHashMap<>();

    public void addPayload(String key, Object value) {
        payload.put(key, value);
    }

}
