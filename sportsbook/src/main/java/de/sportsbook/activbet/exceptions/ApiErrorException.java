package de.sportsbook.activbet.exceptions;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.web.client.HttpStatusCodeException;

@Getter
@ToString
@Slf4j
public class ApiErrorException extends Exception {
    private final String error;
    private final String trace;
    private String requestUrl;
    private String requestBody;

    public ApiErrorException(String errorId, Exception e, String url, String body) {
        this.error = errorId;
        this.trace = ExceptionUtils.getStackTrace(e);

        this.requestUrl = url;
        this.requestBody = body;
        /*
        if (e instanceof HttpStatusCodeException) {
            this.httpStatusCode = ((HttpStatusCodeException) e).getStatusCode().toString();
        } else {
            this.httpStatusCode = "";
        }
         */
    }
}