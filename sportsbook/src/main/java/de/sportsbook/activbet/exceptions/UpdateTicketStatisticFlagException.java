package de.sportsbook.activbet.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateTicketStatisticFlagException extends PayloadException {
    private final String message = "error.ticket.add_statistic_flag";
}
