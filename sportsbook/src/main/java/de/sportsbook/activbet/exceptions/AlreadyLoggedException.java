package de.sportsbook.activbet.exceptions;

/**
 * Wrap exceptions that have already been logged with this to prevent multiple logs for the same exception.
 */
public class AlreadyLoggedException extends RuntimeException {

    public AlreadyLoggedException(Throwable cause) {
        super(cause);
    }

}
