package de.sportsbook.activbet.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ServiceErrorException extends PayloadException {
    private final String message;
}
