package de.sportsbook.activbet;

public class Routes {
    public static final String ROOT = "/";
    public static final String LOGIN = "/login";
    public static final String TICKET = "/ticket/**";

    public static final String CUSTOMER = "/customer/**";
}
