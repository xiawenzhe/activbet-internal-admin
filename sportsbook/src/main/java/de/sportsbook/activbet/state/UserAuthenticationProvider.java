package de.sportsbook.activbet.state;

import de.sportsbook.activbet.api.oauth.entities.OauthAuthentication;
import de.sportsbook.activbet.api.oauth.entities.OauthAuthenticationProvider;
import de.sportsbook.activbet.services.oauth.OauthService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class UserAuthenticationProvider implements OauthAuthenticationProvider {

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private OauthService oAuthService;

    @Override
    public Optional<OauthAuthentication> getAuthentication() {
        Session session = SessionHolder.getSession(false);
        if (session == null) {
            return Optional.empty();
        }

        if (SessionAttributes.isAuthenticated(session)) {
            RLock lock = redisson.getLock(session.getAttribute(SessionAttributes.USERNAME) + ".authentication");
            try {
                lock.lock();
                // Lock getting the authentication to always get the newest token (another thread could refresh the token currently).
                OauthAuthentication authentication = getAuthentication(session);
                if (authentication.isExpired()) {
                    String remoteAddress = session.getAttribute(SessionAttributes.REMOTE_ADDRESS);
                    String userAgent = session.getAttribute(SessionAttributes.USER_AGENT);
                    OAuth2AccessToken token = oAuthService.refreshAuthentication(authentication, remoteAddress, userAgent);
                    SessionAttributes.setAuthentication(session, token);
                    authentication = getAuthentication(session);
                }

                return Optional.of(authentication);
            } catch (Exception e) {
                log.debug("Error refreshing access token", e);
                return Optional.empty();
            } finally {
                lock.unlock();
            }
        }

        return Optional.empty();
    }

    private OauthAuthentication getAuthentication(Session session) {
        OauthAuthentication authentication = new OauthAuthentication();
        authentication.setExpires(session.getAttribute(SessionAttributes.OAUTH_EXPIRES));
        authentication.setAccessToken(session.getAttribute(SessionAttributes.OAUTH_ACCESS_TOKEN));
        authentication.setRefreshToken(session.getAttribute(SessionAttributes.OAUTH_REFRESH_TOKEN));
        return authentication;
    }

}
