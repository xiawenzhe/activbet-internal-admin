package de.sportsbook.activbet.state.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@JsonInclude(Include.NON_DEFAULT)
@EqualsAndHashCode(of = {"sportId", "categoryIds", "tournamentIds"})
@NoArgsConstructor
public class LiveSelection implements Serializable {

    // live can only select one sport
    private Integer sportId;
    private Set<Integer> categoryIds = new HashSet<>();
    private Set<Integer> tournamentIds = new HashSet<>();

    public LiveSelection(
            List<Integer> selectedSportIds,
            List<List<Integer>> selectedCategoryIds,
            List<List<Integer>> selectedTournamentIds) {

        // [sport_id]
        if (!CollectionUtils.isEmpty(selectedSportIds)) {
            this.sportId = selectedSportIds.get(0);
        }

        // [sport_id, category_id]
        if (!CollectionUtils.isEmpty(selectedCategoryIds)) {
            selectedCategoryIds.forEach( ids -> {
                this.sportId = ids.get(0);
                this.categoryIds.add(ids.get(1));
            });
        }

        // [sport_id, category_id, tournament_id]
        if (!CollectionUtils.isEmpty(selectedTournamentIds)) {
            selectedTournamentIds.forEach(ids -> {
                this.sportId = ids.get(0);
                this.categoryIds.add(ids.get(1));
                this.tournamentIds.add(ids.get(2));
            });
        }
    }

}
