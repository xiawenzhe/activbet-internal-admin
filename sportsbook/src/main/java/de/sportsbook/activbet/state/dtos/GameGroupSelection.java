package de.sportsbook.activbet.state.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_DEFAULT)
public class GameGroupSelection implements Serializable {
    private Integer countryId;
    private Integer leagueId;
    //private Boolean isFavorite;
    //private List<Integer> games;
}
