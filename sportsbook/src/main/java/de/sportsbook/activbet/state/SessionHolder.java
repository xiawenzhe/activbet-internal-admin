package de.sportsbook.activbet.state;

import de.sportsbook.activbet.exceptions.UnauthorizedUserException;
import de.sportsbook.activbet.utils.ContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpAttributesContextHolder;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Slf4j
public class SessionHolder {

    private static SessionRepository sessionRepository;

    private static String getCurrentUserSessionId(boolean create) {
        try {
            // Websocket Connection
            return (String) SimpAttributesContextHolder.currentAttributes().getAttribute("SPRING.SESSION.ID");
        } catch (IllegalStateException e) {
            // HTTP Connection
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

            try {
                // Use the internals for more control
                Method method = ServletRequestAttributes.class.getDeclaredMethod("getSession", boolean.class);
                method.setAccessible(true);
                // Creates the session if create is true
                HttpSession session = (HttpSession) method.invoke(attributes, create);

                if (session != null) {
                    return session.getId();
                }
                return null;
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e1) {
                log.error("Error getting the session id", e);
                return null;
            }
        }
    }

    /**
     * Get the session null-safe.
     *
     * @return
     */
    public static Session getSafeSession() {
        Session session = getSession(false);
        if (session == null) {
            throw new UnauthorizedUserException("Session is null");
        }
        return session;
    }

    public static Session getSession(boolean create) {
        return getSession(getCurrentUserSessionId(create));
    }

    public static Session getSession(String sessionId) {
        return getSessionRepository().getSession(sessionId);
    }

    private static SessionRepository getSessionRepository() {
        if (sessionRepository == null) {
            sessionRepository = ContextUtils.getBean(SessionRepository.class);
        }
        return sessionRepository;
    }

}
