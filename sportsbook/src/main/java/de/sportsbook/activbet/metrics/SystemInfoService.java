package de.sportsbook.activbet.metrics;

import com.sun.jna.Platform;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;
import oshi.software.os.OSFileStore;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class SystemInfoService {

    private final HardwareAbstractionLayer hw;
    private final OperatingSystem os;

    public SystemInfoService() {
        SystemInfo systemInfo = new SystemInfo();
        hw = systemInfo.getHardware();
        os = systemInfo.getOperatingSystem();
    }

    public Map<String, NetworkInfo> getNetwork() {
        NetworkIF[] networkIfs = hw.getNetworkIFs();
        Map<String, NetworkInfo> interfaces = new HashMap<>();
        for (NetworkIF networkIf : networkIfs) {
            interfaces.put(networkIf.getName(), new NetworkInfo(networkIf));

        }
        return interfaces;
    }

    public List<ApplicationInfo> getApplications() {
        if (!Platform.isLinux()) {
            log.warn("Application info is only available on linux systems.");
            return Collections.emptyList();
        }


        // Group by command
        Map<String, List<OSProcess>> applications = Stream.of(os.getProcesses(0, null))
            .collect(Collectors.groupingBy(p -> {
                try {
                    /* Linux Specific */
                    List<String> lines = Files.readAllLines(Paths.get("/proc/" + p.getProcessID() + "/cmdline"));
                    if (!lines.isEmpty()) {
                        // Arguments are separated with null bytes (\0)
                        return lines.get(0).replaceAll("\0", " ").trim();
                    }
                    return "";
                } catch (Exception e) {
                    log.error("", e);
                    return "";
                }
            }, Collectors.toList()));


        ArrayList<ApplicationInfo> applicationInfos = new ArrayList<>();
        for (Entry<String, List<OSProcess>> app : applications.entrySet()) {
            String cmd = app.getKey();
            if (cmd.isEmpty()) {
                continue;
            }
            List<OSProcess> processes = app.getValue();
            applicationInfos.add(new ApplicationInfo(cmd, processes));
        }

        return applicationInfos;
    }

    public Map<Integer, Double> getCpuLoad() {
        CentralProcessor cpu = hw.getProcessor();


        double[] load = cpu.getProcessorCpuLoadBetweenTicks();
        Map<Integer, Double> cpus = new HashMap<>();
        for (int i = 0; i < load.length; i++) {
            cpus.put(i, (double) Math.round(load[i] * 100000) / 100000);
        }
        return cpus;
    }

    public Map<String, VolumeInfo> getVolumes() {
        OSFileStore[] fileStores = os.getFileSystem().getFileStores();

        Map<String, VolumeInfo> volums = new HashMap<>();
        for (OSFileStore fileStore : fileStores) {
            volums.put(fileStore.getName(), new VolumeInfo(fileStore));

        }
        return volums;
    }

    public MemoryInfo getMemory() {
        return new MemoryInfo(hw.getMemory());
    }

    public Long getUptime() {
        return hw.getProcessor().getSystemUptime();
    }

    @Data
    public static class ApplicationInfo {
        private String name;
        private String cmd;
        private double cpuLoad;
        private long memory;
        private long uptime;
        private List<Integer> processIds;

        public ApplicationInfo(String cmd, List<OSProcess> processes) {
            cpuLoad = 0;
            memory = 0;
            processIds = new ArrayList<>();
            uptime = 0;

            this.cmd = cmd;
            name = processes.get(0).getName();

            for (OSProcess p : processes) {
                uptime = Math.max(p.getUpTime() / 1000, uptime);
                cpuLoad += ((double) (p.getKernelTime() + p.getUserTime()) / (double) p.getUpTime());
                memory += p.getResidentSetSize() * 4096; // Pages of memory * Linux default page size of 4kb
                processIds.add(p.getProcessID());
            }
        }
    }

    @Data
    public static class VolumeInfo {

        public VolumeInfo(OSFileStore fileStore) {
            useableSpace = fileStore.getUsableSpace();
            totalSpace = fileStore.getTotalSpace();
        }

        private final Long useableSpace;
        private final Long totalSpace;
    }

    @Data
    public static class MemoryInfo {

        public MemoryInfo(GlobalMemory memory) {
            available = memory.getAvailable();
            total = memory.getTotal();
            swapUsed = memory.getSwapUsed();
            swapTotal = memory.getSwapTotal();
        }

        private final Long available;
        private final Long total;
        private final Long swapUsed;
        private final Long swapTotal;
    }

    @Data
    public static class NetworkInfo {

        public NetworkInfo(NetworkIF nif) {
            bytesRecv = nif.getBytesRecv();
            bytesSent = nif.getBytesSent();
        }

        private final Long bytesRecv;
        private final Long bytesSent;
    }

}
