package de.sportsbook.activbet.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SystemInfoEndpoint extends AbstractEndpoint<Map<String, Object>> {

    private SystemInfoService service;

    @Autowired
    public SystemInfoEndpoint(SystemInfoService service) {
        super("system");
        this.service = service;
    }

    @Override
    public Map<String, Object> invoke() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("volumes", service.getVolumes());
        map.put("mem", service.getMemory());
        map.put("uptime", service.getUptime());
        map.put("cpu", service.getCpuLoad());
        map.put("network", service.getNetwork());
        map.put("apps", service.getApplications());

        return map;
    }

}
