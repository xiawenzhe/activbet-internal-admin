package de.sportsbook.activbet.metrics;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.stereotype.Component;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;
import oshi.software.os.OperatingSystemVersion;
import oshi.util.FormatUtil;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class HardwareInfoEndpoint extends AbstractEndpoint<Map<String, Object>> {

    private final HardwareAbstractionLayer hw;
    private final OperatingSystem os;

    public HardwareInfoEndpoint() {
        super("hardware");
        SystemInfo systemInfo = new SystemInfo();
        hw = systemInfo.getHardware();
        os = systemInfo.getOperatingSystem();
    }

    @Override
    public Map<String, Object> invoke() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("os", new OsInfo(os));
        map.put("cpu", new CpuInfo(hw));
        map.put("memory", FormatUtil.formatBytes(hw.getMemory().getTotal()));

        return map;
    }

    @Data
    public static class OsInfo {
        private final String family;
        private final String manufacturer;
        private final String version;
        private final String codeName;
        private final String buildNumber;

        public OsInfo(OperatingSystem os) {
            family = os.getFamily();
            manufacturer = os.getManufacturer();
            OperatingSystemVersion v = os.getVersion();
            version = v.getVersion();
            codeName = v.getCodeName();
            buildNumber = v.getBuildNumber();
        }

    }

    @Data
    private class CpuInfo {
        private final String name;
        private final String family;
        private final String identifier;
        private final int logicalProcessorCount;
        private final int physicalProcessorCount;
        private final String model;
        private final String vendor;
        private final String vendorFreq;
        private final String stepping;

        public CpuInfo(HardwareAbstractionLayer hw) {
            CentralProcessor processor = hw.getProcessor();
            name = processor.getName();
            family = processor.getFamily();
            identifier = processor.getIdentifier();
            logicalProcessorCount = processor.getLogicalProcessorCount();
            physicalProcessorCount = processor.getPhysicalProcessorCount();
            model = processor.getModel();
            vendor = processor.getVendor();
            vendorFreq = FormatUtil.formatHertz(processor.getVendorFreq());
            stepping = processor.getStepping();
        }

    }
}
