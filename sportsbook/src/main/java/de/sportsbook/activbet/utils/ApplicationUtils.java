package de.sportsbook.activbet.utils;

import com.moodysalem.TimezoneMapper;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Objects;

@Slf4j
public class ApplicationUtils {

    public static Boolean isNotNullAndZero(BigDecimal obj) {
        if (Objects.nonNull(obj)) {
            return (obj.compareTo(BigDecimal.ZERO) != 0);
        }
        return false;
    }
}
