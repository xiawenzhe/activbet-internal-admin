package de.sportsbook.activbet.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "version")
public class VersionProperties {

    @Autowired
    private Environment env;

    private String revision;
    private String buildTime;

    public String getRevision() {
        if (env.acceptsProfiles("local")) {
            return "local";
        }
        return revision;
    }

}
