package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import de.sportsbook.activbet.utils.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class WebSocketHandshakeIpInterceptor implements HandshakeInterceptor {

    private final WebSocketSessionUtils webSocketSessionUtils;
    private final RTopic<String> topic;
    private final RTopic<Object> successTopic;

    @Autowired
    public WebSocketHandshakeIpInterceptor(WebSocketSessionUtils webSocketSessionUtils, Redisson redisson) {
        this.webSocketSessionUtils = webSocketSessionUtils;
        topic = redisson.getTopic(WebSocketSessionRegistry.FORCE_CLOSE_WEBSOCKET_TOPIC);
        successTopic = redisson.getTopic(WebSocketSessionRegistry.FORCE_CLOSE_SUCCESSFUL_WEBSOCKET_TOPIC);
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws
                                                                                                                                                       Exception {

        Session session = SessionHolder.getSession(false);
        if (session != null) {
            // Update the users ip address
            HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();
            String remoteAddress = RequestUtils.getRemoteAddress(req);
            session.setAttribute(SessionAttributes.REMOTE_ADDRESS, remoteAddress);
            String userAgent = RequestUtils.getUserAgent(req);
            session.setAttribute(SessionAttributes.USER_AGENT, userAgent);

            constrainActiveWebSocketSessions(session, remoteAddress, userAgent);
        }

        return true;
    }

    private void constrainActiveWebSocketSessions(Session session, String remoteAddress, String userAgent) {
        Map<String, Long> webSocketSessions = session.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);
        if (webSocketSessions != null && webSocketSessions.size() >= 5) {
            // Don't allow more than 5 websocket connections per session (performance optimization)
            Entry<String, Long> oldestWebSocketSession = webSocketSessions.entrySet()
                    .stream()
                    .sorted(Comparator.comparingLong(Entry::getValue))
                    .findFirst()
                    .get();

            String closeWsSessionUuid = oldestWebSocketSession.getKey();
            log.debug("Session {} already has >=5 websocket connections ({}). Broadcasting that oldest ({}, {}) should be killed in favor of " +
                      "new connection. Remote Address: {}, User Agent: {}",
                      session.getId(),
                      webSocketSessions,
                      closeWsSessionUuid,
                      Instant.ofEpochMilli(oldestWebSocketSession.getValue()),
                      remoteAddress,
                      userAgent);

            // Do it via redis so the web application node that holds the session kills it.
            topic.publish(closeWsSessionUuid);

            AtomicBoolean success = new AtomicBoolean();
            int successListenerId = successTopic.addListener((channel, successWsSessionUuid) -> {
                if (successWsSessionUuid.equals(closeWsSessionUuid)) {
                    success.set(true);
                }
            });

            // Wait for the session to be closed.
            int waitCount = 0;
            while (!success.get()) {
                try {
                    waitCount++;
                    if (waitCount == 6) {
                        log.info("Force close websocket session {} did not respond success after 150ms. Assuming the websocket connection is not any longer" +
                                 "held by any node (dead session). Removing the id from the session and trying the next id.", closeWsSessionUuid);
                        webSocketSessionUtils.removeWebSocketSessionUuidFromSession(session, closeWsSessionUuid);
                        successTopic.removeListener(successListenerId);
                        constrainActiveWebSocketSessions(session, remoteAddress, userAgent);
                        return;
                    }

                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    log.error("Exeception waiting for websocket session to be closed", e);
                }
            }

            successTopic.removeListener(successListenerId);
        }

    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
    }

}
