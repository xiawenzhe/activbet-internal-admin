package de.sportsbook.activbet.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.*;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.ExpiringSession;
import org.springframework.session.web.socket.config.annotation.AbstractSessionWebSocketMessageBrokerConfigurer;
import org.springframework.session.web.socket.server.SessionRepositoryMessageInterceptor;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import java.util.EnumSet;
import java.util.List;

@Slf4j
@Configuration
@EnableScheduling // Necessary for heartbeats
@EnableWebSocketMessageBroker
public class WebSocketBrokerConfig extends AbstractSessionWebSocketMessageBrokerConfigurer<ExpiringSession> {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private BetagoWebProperties webProperties;

    @Autowired
    @Qualifier("frontendObjectMapper")
    private ObjectMapper objectMapper;

    @Autowired
    private Redisson redisson;

    @Autowired
    private WebSocketHandshakeIpInterceptor webSocketHandshakeIpInterceptor;

    @Autowired
    private WebSocketSessionUtils webSocketSessionUtils;

    // The client should send a heartbeat every 20 seconds,
    // so the session does not time out as long as the browser is open.
    private static final long[] HEARTBEAT = {0, 20000};

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/queue/"/*, "/topic/"*/).setHeartbeatValue(HEARTBEAT).setTaskScheduler(taskScheduler);

        registry.setApplicationDestinationPrefixes("/action");
    }

    @Override
    public boolean configureMessageConverters(List<MessageConverter> messageConverters) {
        messageConverters.add(new StringMessageConverter());
        messageConverters.add(new ByteArrayMessageConverter());
        DefaultContentTypeResolver resolver = new DefaultContentTypeResolver();
        resolver.setDefaultMimeType(MimeTypeUtils.APPLICATION_JSON);
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();

        // Frontend communication needs an own objectMapper as the backend one is not suitable
        converter.setObjectMapper(objectMapper);

        messageConverters.add(converter);
        return false;
    }

    @Override
    protected void configureStompEndpoints(StompEndpointRegistry registry) {
        StompWebSocketEndpointRegistration registration = registry.addEndpoint("/messages").addInterceptors(webSocketHandshakeIpInterceptor);

        String[] allowedOrigins = webProperties.getWebSocketAllowedOrigins();
        if (allowedOrigins != null && allowedOrigins.length > 0) {
            registration.setAllowedOrigins(allowedOrigins);
        }
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        SessionRepositoryMessageInterceptor<ExpiringSession> interceptor = sessionRepositoryInterceptor();

            // If the reallity check is enabled, allow HEARTBEATS to refresh the session.
            // This prevents sessions from timeout as long as the client is connected.
            interceptor.setMatchingMessageTypes(EnumSet.of(SimpMessageType.CONNECT,
                                                           SimpMessageType.MESSAGE,
                                                           SimpMessageType.SUBSCRIBE,
                                                           SimpMessageType.UNSUBSCRIBE,
                                                           SimpMessageType.HEARTBEAT));


        registration.interceptors(new CustomCsrfChannelInterceptor(), interceptor);
    }

    @Bean
    public RedisSessionRegistry redisSessionRegistry() {
        return new RedisSessionRegistry(redisson);
    }

    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        registration.setMessageSizeLimit(2 * 1024 * 1024 * 4);
        registration.setSendBufferSizeLimit(2 * 1024 * 1024 * 4);

        registration.addDecoratorFactory(handler -> new WebSocketSessionRegistry(handler, webSocketSessionUtils, redisson, redisSessionRegistry()));
        super.configureWebSocketTransport(registration);
    }


}
