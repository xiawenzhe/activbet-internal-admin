package de.sportsbook.activbet.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * Custom implementation of a CsrfTokenRepository, because the default HttpSessionCsrfTokenRepository uses
 * the DefaultCsrfToken class which uses only final vars which makes it impossible for Redisson to inject the
 * '@class' field and throws errors on deserialization. Also, this one saves some space by only storing the token
 * itself and not the static HEADER_NAME and PARAM_NAME as well.
 */
public class CustomRedissionSessionCsrfTokenRepository implements CsrfTokenRepository {

    public static final String HEADER_NAME = "X-CSRF-TOKEN";
    public static final String PARAM_NAME = "_csrf";
    public static final String ATTR_NAME = "csrfToken";

    @Override
    public CsrfToken generateToken(HttpServletRequest request) {
        return new CustomCsrfToken(UUID.randomUUID().toString());
    }

    @Override
    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
        if (token == null) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.removeAttribute(ATTR_NAME);
            }
        } else {
            HttpSession session = request.getSession();
            session.setAttribute(ATTR_NAME, token.getToken());
        }
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        String token = (String) session.getAttribute(ATTR_NAME);
        if (token == null) {
            return null;
        }

        return new CustomCsrfToken(token);
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CustomCsrfToken implements CsrfToken {

        private String token;

        @Override
        public String getHeaderName() {
            return HEADER_NAME;
        }

        @Override
        public String getParameterName() {
            return PARAM_NAME;
        }

    }

}
