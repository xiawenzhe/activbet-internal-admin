package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.api.app.entities.Language;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "web")
public class BetagoWebProperties {

    private String hostname;
    private Integer webSocketPort;
    private String[] webSocketAllowedOrigins;
    private Integer sessionTimeoutSeconds = 3600;

    private List<Language> languages;

    /**
     * Node version used for React server side rendering.
     */
    private String ssrNodeVersion;

    /**
     * ICU 4C Version compatible to the ssr node version (e.g. 59l for node 8.9.3)
     * Can be found out by using npm i full-icu and observing what version gets installed into
     * node_modules with the appropriate node version.
     */
    private String icu4cVersion;

    /**
     * Default Timezone (default "Europe/Berlin")
     */
    private String defaultTimezone = "Europe/Berlin";

    /**
     * Database for Redis notifications.
     */
    private Integer redisNotificationsDatabase;

    /**
     * Redis Channel for notifications (payment etc.).
     */
    private String redisNotificationsChannel = "messages.webapp";

    /**
     * Theme
     * /static/img/favicons/{theme}
     * /static/scss-{theme}
     */
    private String theme = "activbet";

    /**
     * As used in ${client}
     * //widgets.sir.sportradar.com/${client}/widgetloader
     * 30b78a05d80e16764c02366ba332bade
     */
    private String betradarSirWidgetClient = "cbd21e8d2e51d443f7667a3cf257820a";

    //@NestedConfigurationProperty
    //private GoogleRecaptchaProperties googleRecaptcha;

    private String imageResourceUrl;
    private List<String> barcodeTypes;

    /*
    @Getter
    @Setter
    public static class GoogleRecaptchaProperties {
        @NotNull
        private String siteKey;
        @NotNull
        private String secret;
    }
     */
}
