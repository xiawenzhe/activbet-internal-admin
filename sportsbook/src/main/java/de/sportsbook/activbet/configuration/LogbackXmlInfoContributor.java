package de.sportsbook.activbet.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Adds the contents of the configured logback xml configuration to the actuators /info endpoint.
 */
@Slf4j
@Component
public class LogbackXmlInfoContributor implements InfoContributor {

    @Autowired
    private Environment env;

    @Override
    public void contribute(Builder builder) {
        String loggingConfigFile = env.getProperty("logging.config");

        try {
            Resource resource;
            if (loggingConfigFile.startsWith("classpath:")) {
                resource = new ClassPathResource(loggingConfigFile.substring(10));
            } else {
                resource = new PathResource(loggingConfigFile);
            }

            String contents = StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);

            builder.withDetail("loggingConfigFile", loggingConfigFile).withDetail("loggingConfigContent", contents);

        } catch (IOException e) {
            log.error("Error getting contents of logging config file", e);
        }

    }

}
