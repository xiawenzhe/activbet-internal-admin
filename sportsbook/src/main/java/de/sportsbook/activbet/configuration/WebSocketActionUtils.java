package de.sportsbook.activbet.configuration;

import de.sportsbook.activbet.websocket.actions.Action;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Slf4j
@Service
public class WebSocketActionUtils {

    @Autowired
    private SimpMessagingTemplate template;

    public void broadcastToUser(Collection<String> webSocketSessionIds, Action action) {

        log.debug("Broadcasting message to websocketsessions {}", webSocketSessionIds);

        // convertAndSendToUser didn't work
        webSocketSessionIds.forEach(id -> template.convertAndSend("/queue/broadcasts-user" + id, action));
    }

    public void broadcast(Action action) {
        template.convertAndSend("/queue/broadcasts", action);
    }
}
