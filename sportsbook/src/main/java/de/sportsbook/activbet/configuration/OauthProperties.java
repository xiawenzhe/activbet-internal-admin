package de.sportsbook.activbet.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "oauth")
public class OauthProperties {
    private String baseUrl;
    private String managementBaseUrl;
    private String clientId;
    private String clientSecret;
}
