package de.sportsbook.activbet.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class BetagoSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/**").csrf().csrfTokenRepository(new CustomRedissionSessionCsrfTokenRepository())
                // CSRF on message connect is sent via STOMP Headers
                .ignoringAntMatchers("/messages/**", "/download/**", "/rest/**").and().logout().disable().headers()
                // We handle the X-Frame-Options header dynamically
                // in the BaseController
                .frameOptions().disable();
    }

}
