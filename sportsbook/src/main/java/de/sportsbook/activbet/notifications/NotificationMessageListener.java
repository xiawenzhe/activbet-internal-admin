package de.sportsbook.activbet.notifications;

import de.sportsbook.activbet.configuration.BetagoWebProperties;
import de.sportsbook.activbet.configuration.RedisSessionRegistry;
import de.sportsbook.activbet.configuration.WebSocketActionUtils;
import de.sportsbook.activbet.configuration.WebSocketSessionUtils;
import de.sportsbook.activbet.notifications.Notification.Message;
import de.sportsbook.activbet.notifications.Notification.WebAppMessage;
import de.sportsbook.activbet.state.SessionAttributes;
import de.sportsbook.activbet.state.SessionHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Listens to notifications sent via redis pub/sub from the middleware.
 */
@Slf4j
@Component
public class NotificationMessageListener implements MessageListener<Notification> {

    private final TaskScheduler scheduler;

    private RTopic<Notification> topic;
    private int listenerId;

    @Autowired
    private WebSocketActionUtils webSocketActionUtils;

    @Autowired
    private WebSocketSessionUtils webSocketSessionUtils;

    @Autowired
    private RedisSessionRegistry sessionRegistry;

    @Autowired
    public NotificationMessageListener(Environment env, BetagoWebProperties betagoWebProperties, TaskScheduler scheduler) {
        this.scheduler = scheduler;
        setup(env, betagoWebProperties);
    }

    private void setup(Environment env, BetagoWebProperties betagoWebProperties) {
        try {
            Config config = new Config();

            String host = env.getProperty("spring.redis.host");
            String port = env.getProperty("spring.redis.port");
            String address = "redis://" + host + ":" + port;
            String password = env.getProperty("spring.redis.password");
            Integer database = betagoWebProperties.getRedisNotificationsDatabase();

            SingleServerConfig ssc = config.useSingleServer().setAddress(address).setDatabase(database);
            if (StringUtils.hasText(password)) {
                ssc.setPassword(password);
            }

            config.setCodec(new NotificationCodec());

            RedissonClient redisson = Redisson.create(config);

            topic = redisson.getTopic(betagoWebProperties.getRedisNotificationsChannel());
            listenerId = topic.addListener(this);
            log.info("Successfully created NotificationMessageListener");
        } catch (Exception e) {
            log.error("Could not create NotificationMessageListener. Retrying...", e);
            // Try again in 10 seconds
            scheduler.schedule(() -> setup(env, betagoWebProperties), Date.from(Instant.now().plus(10, ChronoUnit.SECONDS)));
        }
    }

    @PreDestroy
    public void removeListener() {
        if (topic != null) {
            topic.removeListener(listenerId);
        }
    }

    @Override
    public void onMessage(String channel, Notification notification) {
        /*
        WebAppMessage webAppMessage = notification.getWebAppMessage();

        String username = webAppMessage.getUsername();
        Set<String> sessionIds = sessionRegistry.getAllSessionIds(username);
        if (sessionIds.isEmpty()) {
            log.debug("Received web app message, but user {} has no active sessions.", username);
            return;
        }

        for (String sessionId : sessionIds) {
            Session session = SessionHolder.getSession(sessionId);
            if (session != null) {
                Map<String, Long> wsSessionUuids = session.getAttribute(SessionAttributes.WEB_SOCKET_SESSION_UUIDS);

                if (wsSessionUuids.isEmpty()) {
                    continue;
                }

                String locale = session.getAttribute(SessionAttributes.LOCALE);

                String message = webAppMessage.getMessages()
                        .stream()
                        .filter(m -> m.getLanguage().equals(locale))
                        .map(Message::getMessage)
                        .findFirst()
                        .orElse(null);

                if (message == null) {
                    log.warn("Received web app message, but message wasn't available in the current language {} of user {}", locale, username);
                    continue;
                }

                NotificationAction action = new NotificationAction();
                action.setCategory(webAppMessage.getCategory().toUpperCase());
                action.setLevel(webAppMessage.getType().toLowerCase());
                action.setMessage(message);

                // Find the websocket connections that this node holds.
                List<String> wsSessionIds = wsSessionUuids.keySet()
                        .stream()
                        .map(webSocketSessionUtils::getWsSessionIdForWsUuid)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                // If this node holds any of the websocket connections to the user,
                // broadcast the message.
                if (!wsSessionIds.isEmpty()) {
                    webSocketActionUtils.broadcastToUser(wsSessionIds, action);
                }

            }
        }
         */
    }

}
