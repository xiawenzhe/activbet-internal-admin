package de.sportsbook.activbet.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import org.redisson.client.codec.Codec;
import org.redisson.client.protocol.Decoder;
import org.redisson.client.protocol.Encoder;

import java.io.InputStream;

public class NotificationCodec implements Codec {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Encoder encoder = obj -> Unpooled.wrappedBuffer(objectMapper.writeValueAsBytes(obj));
    private final Decoder<Object> decoder = (buf, state) -> objectMapper.readValue(
            (InputStream) new ByteBufInputStream(buf), Notification.class);

    @Override
    public Decoder<Object> getMapValueDecoder() {
        return decoder;
    }

    @Override
    public Encoder getMapValueEncoder() {
        return encoder;
    }

    @Override
    public Decoder<Object> getMapKeyDecoder() {
        return decoder;
    }

    @Override
    public Encoder getMapKeyEncoder() {
        return encoder;
    }

    @Override
    public Decoder<Object> getValueDecoder() {
        return decoder;
    }

    @Override
    public Encoder getValueEncoder() {
        return encoder;
    }
}
