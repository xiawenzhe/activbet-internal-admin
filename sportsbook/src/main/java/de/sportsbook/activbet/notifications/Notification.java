package de.sportsbook.activbet.notifications;

import lombok.Data;

import java.util.List;

@Data
public class Notification {

    private WebAppMessage webAppMessage;

    @Data
    public static class WebAppMessage {
        private String group;
        private String category;
        private String type;
        //@JsonProperty("userid")
        private String userId;
        private String username;
        private String referenceId;
        private List<Message> messages;
    }

    @Data
    public static class Message {
        private String language;
        private String message;
    }

}
