module.exports = {
    sourceMap: false,
    plugins: {
        'postcss-flexibility': {},
        'autoprefixer': {
            remove: false,
            browsers: ['last 4 versions', 'Safari 8']
        },
        "css-mqpacker": {
            sort: true
        },
        'postcss-reporter': {},
        'pixrem': {
            rootValue: 10,
            atrules: true
        }
    }
};