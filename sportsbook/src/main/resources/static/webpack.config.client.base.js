const {resolve} = require("path");
const webpack = require("webpack");
const WebpackConfig = require("webpack-config").Config;

module.exports = new WebpackConfig()
    .extend("./webpack.config.base.js")
    .merge({
        entry: {
            main: [
                "./jsx/styles.js",
                "babel-polyfill",
                "./jsx/client.js"
            ]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    screw_ie8: true,

                    unsafe: false,
                    /* Important to not screw up some IE-detections */
                    unsafe_comps: false,

                    pure_getters: true
                }
            }),

            // Split node_modules into vendor bundle
            new webpack.optimize.CommonsChunkPlugin({
                name: "vendor",
                minChunks: function (module) {
                    // this assumes your vendor imports exist in the node_modules directory
                    return module.context && module.context.indexOf('node_modules') !== -1;
                }
            })
        ],
        output: {
            path: resolve(__dirname, 'js/bundle'),
            filename: "[name].bundle.js",
            publicPath: "/resources/js/bundle/"
        }
    });