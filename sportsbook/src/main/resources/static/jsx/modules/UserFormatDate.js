import React from "react";
import PropTypes from 'prop-types';
import {FormattedDate} from "react-intl";

const UserFormatDate = props => (

    <FormattedDate
        day="2-digit" month="2-digit" year="numeric"
        second="2-digit" hour="2-digit" minute="2-digit" hour12={false}
        timeZone="UTC"
        {...props}
    />

);

UserFormatDate.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

export default UserFormatDate;