import React, {Component} from "react";
import PropTypes from 'prop-types';
import classNames from "classnames";
import BaseModal from "react-modal";
import {getScrollbarWidth, removeScrollbarCompensation, addScrollbarCompensation} from "../../utils/Scrolling";

export class Modal extends Component {

    onHide = () => {
        this.props.onHide();
    };

    getChildContext() {
        return {
            onHide: this.onHide
        }
    }

    getSizeClass = () => {
        switch (this.props.size) {
            case "small":
                return "m__modal--sm";
            case "large":
                return "m__modal--lg";
            case "full":
                return "m__modal--full";
            case "medium":
                return "m__modal--md";
            case "iframe":
                return "m__modal--iframe";
            default:
                return "m__modal--md";
        }
    };

    componentWillUpdate(nextProps) {
        if (nextProps.show == this.props.show) {
            return;
        }

        if (!nextProps.show) {
            setTimeout(() => {
                removeScrollbarCompensation("app");
                removeScrollbarCompensation("top-nav");
                document.body.style.overflowY = null;
            }, 150 /* Animation Duration (see css) */);
            return;
        }

        const scrollbarWidth = getScrollbarWidth();
        if (scrollbarWidth > 0) {
            document.body.style.overflowY = "hidden";
            addScrollbarCompensation("app", scrollbarWidth);
            addScrollbarCompensation("top-nav", scrollbarWidth);
        }
    }

    render() {
        const {show, children} = this.props;
        const sizeClass = this.getSizeClass();

        return (
            <BaseModal isOpen={show}
                       ariaHideApp={false}
                       className={classNames("m__modal", sizeClass)}
                       overlayClassName="ReactModal__Overlay"
                       onRequestClose={this.onHide}
                       closeTimeoutMS={150}
                       contentLabel=""
            >
                <div className="m__modal__wrap" onKeyDown={this.props.onEnterKeyDown}>
                    <input autoFocus={true} style={{width: "0px"}} />
                    {children}
                </div>
            </BaseModal>
        )
    }

}

Modal.propTypes = {
    size: PropTypes.string,
    onHide: PropTypes.func.isRequired
};

Modal.defaultProps = {
    size: "regular"
};

Modal.childContextTypes = {
    onHide: PropTypes.func
};

export const ModalContent = ({className, children, cssClass, ...props}) => (
    <div {...props} className={classNames("m__modal__content", className, cssClass)} role="document">
        {children}
    </div>
);

export const ModalFooter = ({className, children, ...props}) => (
    <div {...props} className={classNames("m__modal__foot", className)}>
        {children}
    </div>
);

export const ModalBody = ({className, children, ...props}) => (
    <div {...props} className={ classNames("m__modal__scrollbar", className)}>
        {children}
    </div>
);

export const ModalHeader = ({showCloseButton, className, children, handleClose, ...props}, {onHide}) => (
    <div {...props} className={classNames("m__modal__head", className)}>
        {showCloseButton && !handleClose && <span className="m__modal__close" onClick={onHide}/>}
        {children}
    </div>
);

ModalHeader.propTypes = {
    showCloseButton: PropTypes.bool,
    handleClose: PropTypes.bool
};

ModalHeader.defaultProps = {
    showCloseButton: true,
    handleClose: false
};

ModalHeader.contextTypes = {
    onHide: PropTypes.func
};

export const ModalTitle = ({className, children, ...props}) => (
    <h4 {...props} className={classNames("m__modal__head", className)}>
        {children}
    </h4>
);
