import {VelocityTransitionGroup} from "velocity-react";
import React, {Component} from "react";
import PropTypes from 'prop-types';

class AnimateInOut extends Component {

    render() {

        /* Automatic integration with the Sticky component */
        let animationStarted, animationStopped;
        const sticky = this.context.sticky;
        if (sticky) {
            animationStarted = sticky.animationStarted;
            animationStopped = sticky.animationStopped;
        }

        const {duration, children, display, animate, ...props} = this.props;

        if (!animate) {
            return children;
        }

        return (
            <VelocityTransitionGroup
                enter={{animation: "slideDown", duration, begin: animationStarted, complete: animationStopped, display}}
                leave={{
                    animation: "slideUp",
                    duration,
                    begin: animationStarted,
                    complete: animationStopped
                }} {...props}>
                { children }
            </VelocityTransitionGroup>
        )
    }

}


AnimateInOut.propTypes = {
    duration: PropTypes.number,
    component: PropTypes.string,
    display: PropTypes.string,
    animate: PropTypes.bool
};

AnimateInOut.contextTypes = {
    sticky: PropTypes.object
};

AnimateInOut.defaultProps = {
    duration: 200,
    component: "div",
    display: "block",
    animate: true
};

export default AnimateInOut;