import React, {Component} from "react";
import PropTypes from 'prop-types';

class NoConnectionIcon extends Component {

    state = {
        isUnload: false
    };

    componentDidMount() {
        window.addEventListener("beforeunload", this.setIsUnload)
    }

    setIsUnload = () => {
        this.setState({isUnload: true})
    };

    componentWillUnmount() {
        window.removeEventListener("beforeunload", this.setIsUnload);
    }

    render() {
        let {connected} = this.props;
        const {isUnload} = this.state;

        // Don't show if navigating.
        connected |= isUnload;

        if (connected) {
            return null;
        }

        return (
            <div className="u__no-connection__container">
                <i className="u__no-connection__icon"/>
            </div>
        )
    }
}


NoConnectionIcon.propTypes = {
    connected: PropTypes.bool.isRequired
};

export default NoConnectionIcon;