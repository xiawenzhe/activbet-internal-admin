import {createStore, applyMiddleware, compose} from "redux";
import thunk from "redux-thunk";
import {createResponsiveStoreEnhancer} from "redux-responsive";
import {routerMiddleware} from "react-router-redux";
import {notificationMiddleware} from "./notificationMiddleware";
import {browserHistory} from "react-router/es";
import rootReducer from "../reducers";

function configureStore(initialState) {

    /*
    return createStore(rootReducer, initialState, compose(
        applyMiddleware(thunk, routerMiddleware(browserHistory), notificationMiddleware),
        createResponsiveStoreEnhancer({calculateStateInitially: false, performanceMode: true})
    ));
     */


    if (__PRODUCTION__) {
        return createStore(rootReducer, initialState, compose(
            applyMiddleware(thunk, routerMiddleware(browserHistory), notificationMiddleware),
            createResponsiveStoreEnhancer({calculateStateInitially: false, performanceMode: true})
        ));
    }

    const store = createStore(rootReducer, initialState, compose(
        applyMiddleware(thunk, routerMiddleware(browserHistory), notificationMiddleware),
        // Delay the inital calculation, so the server renders the same markup
        // as the client (with the default values).
        createResponsiveStoreEnhancer({calculateStateInitially: false, performanceMode: true}),
        typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f

    ));

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept(['../reducers'], () => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}

export default configureStore;