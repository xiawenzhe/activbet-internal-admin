function run() {
    //require('velocity-animate');
    //require('velocity-animate/velocity.ui');
    var ReactDOM = require("react-dom");
    var React = require("react");
    var Root = require('./components/Root').default;
    var Setup = require("./setup");
    ReactDOM.render(<Root store={Setup.store} history={Setup.history} />, document.getElementById("app"));
}

// Check if Polyfill is required
if (!window.Intl) {
    require.ensure([], () => {
        require('intl');
        require('intl/locale-data/jsonp/en');
        require('intl/locale-data/jsonp/de');
        require('intl/locale-data/jsonp/zh');
        require('intl/locale-data/jsonp/tr');
        require('intl/locale-data/jsonp/it');
        require('intl/locale-data/jsonp/ru');
        require('intl/locale-data/jsonp/es');
        require('intl/locale-data/jsonp/am');
        require('intl/locale-data/jsonp/el');
        require('intl/locale-data/jsonp/fr');
        run();
    });
} else {
    run();
}