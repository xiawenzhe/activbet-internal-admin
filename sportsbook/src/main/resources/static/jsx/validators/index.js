export function email(value) {
    //return /.+@.+\..+/.test(value);
    /*
     * Backend API must validate email base on RFC 5321, RFC 5322, and extensions RFC 6531
     * open question:
     *   case sensitive ?
     *   local part max. 64 characters ?
     *   domain part max 255 characters ?
     *   together 254 characters ?
     *   ascii till 127 (except special characters) allowed (Umlaut) ?
     */
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(value)
}

export function minLength(value, minLength) {
    return value && value.length >= minLength;
}

export function min(value, min) {
    return typeof value !== "undefined" && parseFloat(value) >= min;
}

export function max(value, max) {
    return typeof value !== "undefined" && parseFloat(value) <= max;
}

export function getAge(bornYear, bornMonth, bornDay) {
    if (!bornYear || !bornMonth || !bornDay) return false;
    const now = new Date();
    const birthday = new Date(now.getFullYear(), bornMonth-1, bornDay);
    let age;
    if (now >= birthday)
        age = now.getFullYear() - bornYear;
    else
        age = now.getFullYear() - bornYear - 1;
    return age;
}

export function minAge(bornYear, bornMonth, bornDay, minAge) {
    return getAge(bornYear, bornMonth, bornDay) >= minAge;
}

/*
 * allowed only space(32) '(39) ,(44) -(45) .(46) in ascii 32 - 127 and > 127
 * if match any characters matches
 *  33-38   \x21-\x26
 *  40-43   \x28-\x2B
 *  47-64   \x2F-\x40
 *  91-96   \x5B-\x60
 *  123-126 \x7B-\x7F
 */
export function name(value) {
    let nameRegex = /([\x21-\x26,\x28-\x2B,\x2F-\x40,\x5B-\x60,\x7B-\x7F])+/;
    return !nameRegex.test(value);
}

/*
 * only allow a-z A-Z 0-9 space and -
 */
export function postalCode(value) {
    let postalCodeRegex = /[^a-zA-Z0-9\s-]+/;
    return !postalCodeRegex.test(value);
}

/*
 * at least 5 characters
 * at most 25 characters : maxlength
 * start with a-z A-Z
 * only allow a-z A-z 0-9 and - . _
 * uniqueness check: XiaWenzhe = xiawenzhe
 */
export function username(value) {
    let usernameRegex = /^[^a-zA-Z]|[^a-zA-Z0-9-._]+/;
    return !usernameRegex.test(value);
}

/*
 * at least 8 characters : minlength
 * at most 120 characters : maxLength
 * only asii 32-126 characters
 * at least 1 digital
 * at least 1 symbol
 * at least 1 capital letter
 *
 *  ([^\x20-\x7E]+) : true: there is at least a character not in 20-7E
 *  (^[^0-9]+$) true: no digital
 *  (^[a-zA-Z0-9]+$) true: no symbol
 *  (^[^A-Z]+$)   true: no capital
 *
 */
export function password(value) {
    let passwordRegex = /([^\x20-\x7E]+)|(^[^0-9]+$)|(^[a-zA-Z0-9]+$)|(^[^A-Z]+$)/;
    return !passwordRegex.test(value);
}

export function birthdayDate(year, month, day) {
    if ( (!year) || (!month) || (!day)
        || (year < 1900) || (month < 1) || (day < 1)
        || (month > 12) || (day > 31)
    ) {
        return false;
    }

    //big month 1 3 5 6 7 8 10 12
    let bigMonths = [1,3,5,7,8,10,12];
    let smallMonths = [4,6,9,11];
    if (bigMonths.includes(month)) {
        if (day > 31) {
            return false;
        }
    } else if (smallMonths.includes(month)) {
        if (day > 30) {
            return false;
        }
    } else {
        // year mod 4 but not 100
        // year mod 400
        if ( (0 == year%400) || ((0 == year%4) && (0 !== year%100)) ) {
            if (day > 29) {
                return false;
            }
        } else {
            if (day > 28) {
                return false;
            }
        }
    }
    return true;
}

/*
    valid stake format is
    88,88 / 88.88 / 88 / 88, / 88.
 */
export function stakeFormat(value) {
    return /^\d*([.,]\d{0,2})?$/.test(value);
}

export function integerFormat(value) {
    if (typeof value === "undefined" || !value) {
        return false;
    }

    if ( isNaN(value) ) {
        return false;
    }

    //return (Math.round(value) == value);
    let num = Number(value);
    return (num == value) && (num < 10000000000);
}

export function isEmpty(value) {
    if (value) {
        if (value.trim().length > 0) {
            return false;
        }
    }
    return true;
}

export function bankIban(value) {
    let ibanRegex = /[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}/;
    return ibanRegex.test(value);
}

export function bankBic(value) {
    let bicRegex = /([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)/;
    return bicRegex.test(value);
}

export function bankName(value) {
    return !isEmpty(value);
}

export function bankAddress(value) {
    return !isEmpty(value);
}

export function oddValidator(odd) {
    if (odd) {
        let valid = !isNaN(odd);
        if (true == valid) {
            if (odd > 0) {
                return true;
            }
        }
    }
    return false;
}