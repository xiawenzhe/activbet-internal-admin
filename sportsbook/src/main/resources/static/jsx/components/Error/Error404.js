import React, {Component} from "react";
import PropTypes from 'prop-types';
import PageTitle from "../../modules/PageTitle";
import {FormattedMessage, FormattedHTMLMessage} from "react-intl";

class Error404 extends Component {

    render() {
        return (
            <PageTitle id="error404.page_title">



                        <article className="m__panel">
                            <header className="m__panel__heading">
                                <h3 className="m__panel-title"><FormattedMessage id="error404.title"/></h3>
                            </header>

                            <div className="m__panel__body m__panel__content--norm">
                                <FormattedHTMLMessage id="error404.text.html" />
                            </div>

                            <footer className="m__panel__footer" />

                        </article>

            </PageTitle>
        );
    }

}

export default Error404;
