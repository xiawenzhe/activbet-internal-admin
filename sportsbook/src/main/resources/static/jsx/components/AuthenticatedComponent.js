import {connect} from "react-redux";
import {replace} from "react-router-redux";
import React, {Component} from "react";

export function requireAuthentication(Component) {

    class AuthenticatedComponent extends Component {

        componentWillMount() {
            this.checkAuth(this.props.authenticated);
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps.authenticated);
        }

        checkAuth(authenticated) {
            if (!authenticated) {
                let redirectAfterLogin = encodeURI(this.props.location.pathname);
                this.props.replace(`/login?next=${redirectAfterLogin}`);
            }
        }

        render() {
            return this.props.authenticated === true
                ? <Component {...this.props}/>
                : null;
        }
    }

    const mapStateToProps = (state) => ({
        authenticated: state.user.authenticated
    });

    const mapDispatchToProps = {
        replace
    };

    return connect(mapStateToProps, mapDispatchToProps)(AuthenticatedComponent);

}