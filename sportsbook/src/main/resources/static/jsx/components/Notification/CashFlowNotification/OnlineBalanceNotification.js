import React, {Component} from "react";
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {requestCashFlowNotification} from "../../../actions/notification";
import {Currency, timeDifferenceInMinutes} from "../../App/Toolkit";
import {toUTC} from "../../../actions/app";

class OnlineBalanceNotification extends Component {

    render() {
        const {notification, isLoading} = this.props;

        if (isLoading) {
            return (
                <div>
                    |
                    <span style={{paddingLeft: "10px", paddingRight: "10px"}}>
                        loading ...
                    </span>
                    |
                </div>
            )

        } else {
            if (notification) {
                const {
                    onlineBalanceByOnlineCustomer, onlineBalanceByShopCustomer, onlineBalance
                } = notification;

                return (
                    <div>
                        |
                        <span style={{paddingLeft: "10px", paddingRight: "10px", color: "#1997c6"}}>
                            Total Online Balance:
                            <Currency value={onlineBalance} />
                        </span>
                        |
                    </div>
                )
            }
            return null;
        }
    }
}

function mapStateToProps(state) {
    const {isLoading, notification} = state.notification.cashFlowNotification;
    return {
        isLoading, notification
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(OnlineBalanceNotification);
