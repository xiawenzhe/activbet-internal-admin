import React, {Component} from "react";
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {requestCashFlowNotification} from "../../../actions/notification";
import {Currency, timeDifferenceInMinutes} from "../../App/Toolkit";
import {toUTC} from "../../../actions/app";

class CashFlowNotification extends Component {
    componentDidMount() {
        // refresh every 1 minute
        this.requestNotification = () => {
            this.props.requestCashFlowNotification()
                .then(
                    () => {
                        setTimeout(() => {
                            if (this.requestNotification) {
                                this.requestNotification();
                            }
                        }, 1 * 60 * 1000);
                    }
                )
        };
        this.requestNotification();
    }

    componentWillUnmount() {
        delete this.requestNotification;
    }

    /*
    getTimeDiff = (date, now) => {
        if (date) {
            const lastDate = new Date(date);
            return timeDifferenceInMinutes(lastDate, now);
        }
        return "#";
    };
     */

    render() {
        const {notification, isLoading} = this.props;

        if (isLoading) {
            return (
                <div>
                    |
                    <span style={{paddingLeft: "10px", paddingRight: "10px"}}>
                        loading ...
                    </span>
                    |
                </div>
            )

        } else {
            if (notification) {
                const {
                    lastShopTicketAtDiff, lastOnlineTicketAtDiff,
                    lastShopPayInAtDiff, lastShopPayInAmount, lastOnlinePayInAtDiff, lastOnlinePayInAmount,
                    lastShopPayOutAtDiff, lastShopPayOutAmount, lastOnlinePayOutAtDiff, lastOnlinePayOutAmount,
                    onlineBalanceByOnlineCustomer, onlineBalanceByShopCustomer, onlineBalance
                } = notification;

                return (
                    <div>
                        |
                        <span style={{paddingLeft: "10px", paddingRight: "10px", color: "#1997c6"}}>
                            Shop:
                            last ticket since {lastShopTicketAtDiff} minutes;
                            last pay in <Currency value={lastShopPayInAmount} /> since {lastShopPayInAtDiff} minutes;
                            last pay out <Currency value={lastShopPayOutAmount} /> since {lastShopPayOutAtDiff} minutes;
                        </span>
                        ||
                        <span style={{paddingLeft: "10px", paddingRight: "10px", color: "#1997c6"}}>
                            Online:
                            last ticket since {lastOnlineTicketAtDiff} minutes;
                            last pay in <Currency value={lastOnlinePayInAmount} /> since {lastOnlinePayInAtDiff} minutes;
                            last pay out <Currency value={lastOnlinePayOutAmount} /> since {lastOnlinePayOutAtDiff} minutes;
                        </span>
                        |
                    </div>
                )
            }
            return null;
        }
    }
}

function mapStateToProps(state) {
    const {isLoading, notification} = state.notification.cashFlowNotification;
    return {
        isLoading, notification
    }
}

const mapDispatchToProps = {
    requestCashFlowNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(CashFlowNotification);
