import React, {Component} from "react";
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {requestMessageNotification} from "../../../actions/notification";
import Indicator from "../../Module/Indicator";

class MessageNotification extends Component {
    componentDidMount() {
        // refresh every 5 minute
        this.requestNotification = () => {
            this.props.requestMessageNotification()
                .then(
                    () => {
                        setTimeout(() => {
                            if (this.requestNotification) {
                                this.requestNotification();
                            }
                        }, 5 * 60 * 1000);
                    }
                )
        };
        this.requestNotification();
    }

    componentWillUnmount() {
        delete this.requestNotification;
    }

    render() {
        const {notification, isLoading} = this.props;
        let message = "";
        let isHighlight = false;

        if (isLoading) {
            message = "loading ...";
        } else {
            if (notification) {
                isHighlight = (notification.unread > 0);
                message = `${notification.unread} unread messages`;
            }
        }

        return (
            <div>
                |
                <span style={{paddingLeft: "10px", paddingRight: "10px"}}>
                    <a className={isHighlight ? "alert-info" : ""} href="/customer/messages">
                        {message}
                    </a>
                </span>
                |
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {isLoading, notification} = state.notification.messageNotification;
    return {
        isLoading, notification
    }
}

const mapDispatchToProps = {
    requestMessageNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageNotification);