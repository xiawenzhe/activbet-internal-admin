import React, {Component} from "react";
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {requestPaymentTransactionNotification} from "../../../actions/notification";
import Indicator from "../../Module/Indicator";

class PaymentTransactionNotification extends Component {

    componentDidMount() {
        // refresh every 10 minute
        this.requestNotification = () => {
            this.props.requestPaymentTransactionNotification()
                .then(
                    () => {
                        setTimeout(() => {
                            if (this.requestNotification) {
                                this.requestNotification();
                            }
                        }, 10 * 60 * 1000);
                    }
                )
        };
        this.requestNotification();
    }

    componentWillUnmount() {
        delete this.requestNotification;
    }

    render() {
        const {isLoading, notification} = this.props;
        let message = "";
        let isHighlight = false;

        if (isLoading) {
            message = "loading ...";
        } else {
            if (notification) {
                isHighlight = (notification.openPayOutCount > 0);
                message = `${notification.openPayOutCount} payment transactions, total ${notification.openPayOutVolume} Birr to be released`;
            }
        }

        return (
            <div>
                |
                <span style={{paddingLeft: "10px", paddingRight: "10px"}}>
                    <a className={isHighlight ? "alert-info" : ""} href="/account/payment_transactions">
                        {message}
                    </a>
                </span>
                |
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {isLoading, notification} = state.notification.paymentTransactionNotification;
    return {
        isLoading, notification
    }
}

const mapDispatchToProps = {
    requestPaymentTransactionNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentTransactionNotification);
