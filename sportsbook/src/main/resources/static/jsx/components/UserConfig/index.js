import React, {Component} from "react";
import {connect} from "react-redux";
import {login, abortLogin, confirmLogin} from "../../actions/user";
import {FormattedMessage} from "react-intl";
import {replace} from "react-router-redux";
import {Alert, Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import Indicator from "../Module/Indicator";
import ConfirmLoginModal from "./ConfirmLoginModal";

class UserConfig extends Component {

    render() {
        return (
            <span></span>
        );
    }
}

function mapStateToProps(state) {
    const {authenticated, showLoginConfirmation, isLoginLoading} = state.user;
    return {
        authenticated, showLoginConfirmation, isLoginLoading
    };
}

const mapDispatchToProps = {
    replace,
    login, abortLogin, confirmLogin
};

export default connect(mapStateToProps, mapDispatchToProps)(UserConfig);
