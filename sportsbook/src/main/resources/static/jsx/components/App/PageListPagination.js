import React, { Component } from "react";
import { Pagination, PaginationLink, PaginationItem, Button, InputGroup, InputGroupAddon, Input } from "reactstrap";
import PropTypes from 'prop-types';
import {validateInt} from "./Toolkit";

const defaultPageButtonsSize = 4;
const extendedPageButtonsSize = 6;
const spacerStyle = {marginLeft: '8px', marginRight: '8px', marginTop: '-4px', fontSize: '20px'};

class PageListPagination extends Component {

    state = {
        pageSize: 0,
    };

    onChange = (pageIndex, pageSize) => (e) => {
        e.stopPropagation();
        this.props.changePageAction(pageIndex, pageSize);
    };

    onUpdatePageSize = (e) => {
        e.stopPropagation();
        const size = validateInt(e.target.value, this.state.pageSize);
        if (this.state.pageSize === size) return;
        this.setState({ pageSize: size });
    };

    render() {
        const {isLoading, pageList} = this.props;
        if (!pageList) return null;

        const {pageIndex, pageSize, totalPages, showPreviousButton, showNextButton, totalCount} = pageList;

        let showFirstPageButton = false;
        let showLastPageButton = false;
        let minIndex = 0;
        let maxIndex = totalPages - 1;

        if (totalPages > extendedPageButtonsSize + 1) {
            if (pageIndex >= defaultPageButtonsSize - 1 && totalPages - pageIndex <= extendedPageButtonsSize) {
                minIndex = maxIndex - extendedPageButtonsSize + 1;
            } else if (pageIndex < defaultPageButtonsSize - 1 || pageIndex < defaultPageButtonsSize && totalPages - pageIndex < extendedPageButtonsSize) {
                maxIndex = defaultPageButtonsSize - 1;
            } else {
                minIndex = pageIndex;
                maxIndex = pageIndex + defaultPageButtonsSize - 1;
            }
            if (minIndex > 1) {
                showFirstPageButton = true;
            }
            if (totalPages - maxIndex > 1) {
                showLastPageButton = true;
            }
        }

        let pageArray = [];
        for (let index = minIndex; index <= maxIndex; index++) {
            pageArray.push({
                index: index,
                pageNumber: index + 1,
                active: index === pageIndex
            });
        }

        let { pageSize: statePageSize} = this.state;
        if (statePageSize === 0)
            statePageSize = pageSize;

        return (
            <Pagination>
                {showPreviousButton &&
                <PaginationItem disabled={isLoading == true}>
                    <PaginationLink previous onClick={this.onChange(pageIndex - 1, pageSize)}/>
                </PaginationItem>
                }

                {showFirstPageButton &&
                <PaginationItem disabled={isLoading == true}>
                    <PaginationLink onClick={this.onChange(0, pageSize)}>1</PaginationLink>
                </PaginationItem>
                }

                {showFirstPageButton &&
                <div style={spacerStyle}>...</div>
                }

                {maxIndex > 0 &&
                pageArray.map(page =>
                    <PaginationItem active={page.active} disabled={isLoading == true} key={page.index}>
                        <PaginationLink previous
                                        onClick={this.onChange(page.index, pageSize)}>{page.pageNumber}</PaginationLink>
                    </PaginationItem>
                )
                }

                {showLastPageButton &&
                <div style={spacerStyle}>...</div>
                }

                {showLastPageButton &&
                <PaginationItem disabled={isLoading == true}>
                    <PaginationLink
                        onClick={this.onChange(totalPages - 1, pageSize)}>{totalPages}</PaginationLink>
                </PaginationItem>
                }

                {showNextButton &&
                <PaginationItem disabled={isLoading == true}>
                    <PaginationLink next onClick={this.onChange(pageIndex + 1, pageSize)}/>
                </PaginationItem>
                }

                <div style={spacerStyle}/>
                <InputGroup>
                    <Input className="form-control"
                           type="number"
                           style={{width: '6em'}}
                           placeholder={pageSize}
                           value={statePageSize}
                           disabled={isLoading == true}
                           onChange={this.onUpdatePageSize}
                    />

                    <InputGroupAddon addonType="append">
                        <Button disabled={isLoading == true}
                                onClick={this.onChange(pageIndex, statePageSize)}
                        >
                            #/page
                        </Button>
                    </InputGroupAddon>


                    <InputGroupAddon addonType="append">
                        <Button>
                            Total Hit: {totalCount}
                        </Button>
                    </InputGroupAddon>

                </InputGroup>
            </Pagination>
        );
    };
}

PageListPagination.propTypes = {
    /*
    isLoading: false,
    pageList: {
        pageIndex: 0,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
        indexFrom: 0,
        hasPreviousPage: false,
        hasNextPage: false,
    },
     */
    changePageAction: PropTypes.func,
};

export default PageListPagination;