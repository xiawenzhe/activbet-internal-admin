import {FormattedMessage} from "react-intl";
import {connect} from "react-redux";
import {hideSettingsModal, selectLocale, showSettingsModal} from "../../actions/user";
import {requestTimezonesIfNeeded} from "../../actions/i18n";
import {
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalTitle
} from "../../modules/Modal";
import {updateQueryStringParameter} from "../../utils/Url"
import React, {Component} from "react";
import PropTypes from 'prop-types';

class SettingsModal extends Component {

    constructor(props) {
        super(props);
        const {locale, timezone} = props.user;
        this.state = {
            locale,
            timezone
        };
    }

    close = () => {
        const {locale: oldLocale, timezone: oldTimezone} = this.props.user;
        const {locale, timezone} = this.state;

        this.props.hideSettingsModal();
        if (oldLocale !== locale || oldTimezone !== timezone) {
            this.props
                .selectLocale();
            let reloadUrl = window.location.href;
            reloadUrl = updateQueryStringParameter(reloadUrl, "locale", encodeURIComponent(locale));
            reloadUrl = updateQueryStringParameter(reloadUrl, "timezone", encodeURIComponent(timezone));
            window.location = reloadUrl;
        }
    };

    open = () => {
        this.props.showSettingsModal();
        this.props.requestTimezonesIfNeeded();
    };

    onChange = () => {
        this.setState({locale: this.languageInput.value, timezone: this.timezoneInput.value});
    };

    render() {
        const {showSettingsModal} = this.props.user;
        const {locale, timezone} = this.state;

        const {languages, timezones, isLoading} = this.props.i18n;

        const {userLoggedIn} = this.props;
        const button = (
            <button
                onClick={this.open}
                type="button"
                className="u__button m__navlist__btn u__button--stateless">
                <FormattedMessage id="settings_modal.btn.settings"/>
            </button>
        );

        const modal = (
            <Modal show={showSettingsModal} onHide={this.close} size="small">
                <ModalContent>
                    <ModalHeader>
                        <ModalTitle><FormattedMessage id="settings_modal.title"/></ModalTitle>
                    </ModalHeader>
                    <ModalBody className="m__modal__body">

                        <div className="form-group">
                            <label className="d__form-control__label d__form-control__label--black">
                                <FormattedMessage id="settings_modal.language"/>
                            </label>
                            {languages.isLoading
                                ? <select className="d__form-control__select form-control"/>
                                : <select
                                    ref={l => this.languageInput = l}
                                    value={locale}
                                    className="d__form-control__select form-control"
                                    onChange={this.onChange}>
                                    {languages
                                        .languages
                                        .map(language => <option key={language.id} value={language.code}>{language.name}</option>)}
                                </select>
}
                        </div>

                        <div className="form-group">
                            <label className="d__form-control__label d__form-control__label--black">
                                <FormattedMessage id="settings_modal.timezone"/>
                            </label>
                            {timezones.isLoading
                                ? <select className="d__form-control__select form-control"/>
                                : <select
                                    ref={t => this.timezoneInput = t}
                                    value={timezone}
                                    className="form-control d__form-control__select"
                                    onChange={this.onChange}>
                                    {timezones
                                        .timezones
                                        .map(timezone => <option key={timezone.id} value={timezone.id}>
                                            {timezone.name}
                                            {timezone.offset && ` (${timezone.offset})`}
                                        </option>)}
                                </select>
                                }
                        </div>

                    </ModalBody>
                    <ModalFooter>
                        <button onClick={this.close} className="u__button u__button--stateless">
                            {isLoading
                                ? <FormattedMessage id="form.info.loading"/>
                                : <FormattedMessage id="settings_modal.btn.close"/>
                            }
                        </button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        )

        const settingsModal = (userLoggedIn)
            ? <li className="c__navbar__menu-account m__navlist__user-action">
                    {button}
                    {modal}
                </li>

            : <div className="m__navlist__settings">
                {button}
                {modal}
            </div>

        return settingsModal;
    }
}

SettingsModal.propTypes = {
    user: PropTypes.shape({locale: PropTypes.string.isRequired, timezone: PropTypes.string.isRequired}),
    i18n: PropTypes
        .shape({languages: PropTypes.object.isRequired, timezones: PropTypes.object.isRequired})
        .isRequired,
    userLoggedIn: PropTypes.bool
};

SettingsModal.defaultProps = {
    userLoggedIn: false
};

function mapStateToProps(state) {
    return {i18n: state.app.i18n, user: state.user}
}

const mapDispatchToProps = {
    showSettingsModal,
    hideSettingsModal,
    selectLocale,
    requestTimezonesIfNeeded
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsModal);