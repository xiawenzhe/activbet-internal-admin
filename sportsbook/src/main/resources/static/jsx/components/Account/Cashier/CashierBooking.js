import React, {Component} from "react";
import {connect} from "react-redux";
import {ContainerFluidSpacious} from "../../App/Toolkit";
import {Button, Card, Col, FormGroup, Row, UncontrolledDropdown} from "reactstrap";
import CompanyNav from "./CompanyNav";
import {requestAllWebCashiersIfNeeded, requestAllWebCashiers} from "../../../actions/customer";
import AccountPatching from "../../Customer/AccountPatching";

class CashierBooking extends Component {

    componentDidMount() {
        this.props.requestAllWebCashiersIfNeeded();
    }

    onRefresh = () => {
        this.props.requestAllWebCashiers();
    };

    onClear = () => {

    };

    onCompanyClick = () => {

    };

    render() {
        const {webCashierCompanyId, allCashiers} = this.props;
        let cashiers = [];
        if (allCashiers && webCashierCompanyId) {
            cashiers = allCashiers.filter(c => c.companyId == webCashierCompanyId);
        }

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col style={{flex: '0 0 20rem'}}>
                        <Card key="navigation-filter" color="dark" className="pt-3 pl-3 pr-3">
                            <FormGroup>
                                <UncontrolledDropdown className="btn-group w-100">
                                    <Button block color="primary cursor-pointer" onClick={this.onRefresh}>Refresh</Button>
                                    {/*
                                    <Button color="dark" onClick={this.onClear}>Clear</Button>
                                    */}
                                </UncontrolledDropdown>
                            </FormGroup>
                        </Card>

                        <CompanyNav
                            onCompanyClick={this.onCompanyClick}
                            onClear={this.onCLear}
                        />
                    </Col>

                    {/* show cashier patching */}
                    <Col>
                    {
                        (cashiers.length > 0) && cashiers.map(cashier =>
                        <AccountPatching cashier={cashier}/>
                        )
                    }
                    </Col>
                </Row>
            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {webCashierCompanyId} = state.company;
    const {allCashiers} = state.customer;

    return {
        webCashierCompanyId, allCashiers
    }
}

const mapDispatchToProps = {
    requestAllWebCashiersIfNeeded, requestAllWebCashiers
};

export default connect(mapStateToProps, mapDispatchToProps)(CashierBooking);