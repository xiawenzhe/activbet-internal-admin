import React, {Component} from "react";
import {connect} from "react-redux";
import {ContainerFluidSpacious} from "../../App/Toolkit";
import CompanyNav from "./CompanyNav";
import {Col, Row} from "reactstrap";
import {Button, Card, FormGroup, UncontrolledDropdown} from "reactstrap";
import {DateTimePicker} from "react-widgets";
import {requestWebCashierReports, resetWebCashierReportsFilter, clearWebCashierReports} from "../../../actions/company";
import CashierReportTable from "./CashierReportTable";
import Indicator from "../../Module/Indicator";
import IdNameDropDown from "../../Module/IdNameDropDown";
import {selectUserCurrency} from "../../../actions/user";

class Cashier extends Component {

    onFromChange = (from) => {
        const {webCashierReportsFilter} = this.props;
        const {to} = webCashierReportsFilter;
        this.props.resetWebCashierReportsFilter(
            from, to
        )
    };

    onToChange = (to) => {
        const {webCashierReportsFilter} = this.props;
        const {from} = webCashierReportsFilter;
        this.props.resetWebCashierReportsFilter(
            from, to
        )
    };

    onRequestReport = () => {
        this.props.requestWebCashierReports();
    };

    onClear = () => {
        this.props.clearWebCashierReports();
    };

    onCompanyClick = () => {
        this.props.requestWebCashierReports();
    };

    onCurrencyChange = (id) => {
        this.props.selectUserCurrency(id);
    };

    /*
    * cashier statistcs, filter by date
    * */
    render() {
        const {webCashierReportsFilter, webCashierReports, currencies, userCurrencyId} = this.props;
        const {from, to} = webCashierReportsFilter;
        const {isLoading} = webCashierReports;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col style={{flex: '0 0 20rem'}}>
                        <Card key="navigation-filter" color="dark" className="pt-3 pl-3 pr-3">

                            {/* select currency
                            <div style={{paddingTop: "0.5rem", paddingLeft: "0rem", paddingBottom: "1rem"}}>
                                <div className="checkbox-slider--b-flat">
                                    <IdNameDropDown
                                        id={userCurrencyId}
                                        addPlaceholder={true}
                                        placeholder="Currency"
                                        list={currencies}
                                        listLoading={isLoading}
                                        selectionChanged={this.onCurrencyChange}
                                        showDeleteButton={false}
                                    />
                                </div>
                            </div>
                            */}

                            <FormGroup>
                                <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                            </FormGroup>
                            <FormGroup>
                                <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                            </FormGroup>

                            <FormGroup>
                                <UncontrolledDropdown className="btn-group w-100">
                                    <Button block color="primary cursor-pointer" onClick={this.onRequestReport} disabled={isLoading}>Load</Button>
                                    <Button color="dark" onClick={this.onClear}>Clear</Button>
                                </UncontrolledDropdown>
                            </FormGroup>
                        </Card>

                        <CompanyNav
                            onCompanyClick={this.onCompanyClick}
                            onClear={this.onClear}
                        />
                    </Col>

                    {
                        (true == isLoading) &&
                        <div className="text-center">
                            <Indicator animate className="loading-indicator mt-3"/>
                            <div className="dashhead-subtitle mt-3">Loading</div>
                        </div>
                    }

                    {/* show account balance */}
                    <CashierReportTable />
                </Row>
            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {currencies, userCurrencyId} = state.user;
    const {webCashierReportsFilter, webCashierReports} = state.company;
    return {
        webCashierReportsFilter, webCashierReports, currencies, userCurrencyId
    };
}

const mapDispatchToProps = {
    resetWebCashierReportsFilter,
    requestWebCashierReports,
    clearWebCashierReports,
    selectUserCurrency
};

export default connect(mapStateToProps, mapDispatchToProps)(Cashier);