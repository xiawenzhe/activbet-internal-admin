import React from "react";

const ModalHeaderTitle = ({id, className, name}) =>
{
    if (id !== null && id === 0) {
        return (
            <div>
                <small className="text-muted">
                    New&nbsp;{className}
                </small>
            </div>
        );
    }
    else if (id !== null && id > 0) {
        return (
            <div>
                <small className="text-muted">
                    Edit&nbsp;{className}
                </small>
                &nbsp;:&nbsp;{name}
            </div>
        );
    }
    else {
        return null;
    }
};

export default ModalHeaderTitle;
