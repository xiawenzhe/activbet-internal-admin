import React, {Component} from "react";
import {connect} from "react-redux";
import Tickets from "./index";

class TicketAddFlag extends Component {
    render() {
        return (
            <Tickets
                flag={true}
            />
        )
    }
}

export default TicketAddFlag;