import React, {Component} from "react";
import {Currency, formatUserDate, getSystemsText, Odd} from "../../App/Toolkit";
import {
    Col,
    Row,
} from "reactstrap";
import TicketStatus from "../TicketStatus";
import BetTableRow from "./BetTableRow";
import Immutable from "immutable";
import Indicator from "../../Module/Indicator";
import classNames from "classnames";

class TicketDetailTable extends Component {

    state = {
        ticketDetailsIds: [],
        newTicketIds: Immutable.Set()
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.isRefreshEnabled) {
            const ticketIds = Immutable.Set(this.props.tickets.map(ticket => ticket.id));
            const diff = Immutable.Set(nextProps.tickets.map(ticket => ticket.id)).subtract(ticketIds);
            this.setState({
                newTicketIds: diff
            });
        }
    }

    toggleTicketDetails = (id) => {
        let {ticketDetailsIds} = this.state;
        if (!ticketDetailsIds.includes(id)) {
            ticketDetailsIds.push(id);
            this.setState({
                ticketDetailsIds: ticketDetailsIds
            })
        } else {
            for (let i = 0; i < ticketDetailsIds.length; i++) {
                if (id == ticketDetailsIds[i]) {
                    ticketDetailsIds.splice(i, 1);
                    this.setState({
                        ticketDetailsIds: ticketDetailsIds
                    });
                    break;
                }
            }
        }

    };

    /*
    getSystemsText = (ticket) => {
        let text = "";
        if (ticket.systems) {
            if (ticket.systems.length > 0) {
                if (ticket.systems[0].bankCount > 0) {
                    text = `${ticket.systems[0].bankCount}B + `
                }

                ticket.systems.forEach(
                    (system, i) => {
                        text = text + system.minSystem + (i < ticket.systems.length-1 ? "," : "");
                    }
                );
                text = text + " of " + ticket.systems[0].maxSystem
            }

        }
        return text;
    };
    */

    render() {
        const {tickets, showBets} = this.props;
        const {ticketDetailsIds, newTicketIds} = this.state;

        return (
            <Row className="mt-3">
                <Col>
                    <table className="table table-striped tickets-table">
                        <thead className="thead-xs">
                            <tr>
                                <th colSpan="5" className="border-right text-center">Ticket</th>
                                <th colSpan="3" className="border-right text-center">Stake</th>
                                <th colSpan="5" className="border-right text-center">Possible Win</th>
                                <th colSpan="6" className="text-center">Evaluation</th>
                            </tr>
                            <tr>
                                <th>Number</th>
                                <th>Inserted</th>
                                <th>Customer</th>
                                <th>Type</th>
                                <th className="border-right">Systems</th>

                                <th>Stake</th>
                                <th>Fee</th>
                                <th className="border-right">Pay-In</th>

                                <th>Bet #</th>
                                <th>Odd</th>
                                <th>Win</th>
                                <th>Fee</th>
                                <th className="border-right">Pay-Out</th>

                                <th>Status</th>
                                <th>Payout Time</th>
                                <th>Bet #</th>
                                <th>Odd</th>
                                <th>Win</th>
                                <th>Fee</th>
                                <th>Pay-Out</th>
                            </tr>
                        </thead>



                        <tbody>
                        {
                            tickets && tickets.map(ticket => {
                                const showTicketBets = showBets || ticketDetailsIds.includes(ticket.id);
                                return [
                                    <tr key={ticket.id} className={classNames("ticket-row", {
                                        "show-bets": showTicketBets,
                                        "animated flash": newTicketIds.includes(ticket.id)
                                    })}>
                                        <td>
                                            <a style={{padding: "0"}} className="cursor-pointer" onClick={() => this.props.openTicketEditor(ticket)}>
                                                {ticket.number}
                                            </a>
                                        </td>
                                        <td>{formatUserDate(ticket.insertTime)}</td>
                                        <td>{ticket.customerUser}</td>
                                        <td>
                                            {(true == ticket.isStandard) && <span className="ticket-type-standard">S</span>}
                                            {(true == ticket.isLive) && <span className="ticket-type-live">L</span>}
                                        </td>
                                        <td className="border-right">{getSystemsText(ticket)}</td>
                                        <td><Currency value={ticket.stake} /></td>
                                        <td><Currency value={ticket.stakeFee} /></td>
                                        <td className="border-right"><Currency value={ticket.payIn} /></td>

                                        <td>{ticket.betCount}</td>
                                        <td><Odd value={ticket.maxOdd} /></td>
                                        <td><Currency value={ticket.maxWin} /></td>
                                        <td><Currency value={ticket.maxWinFee} /></td>
                                        <td className="border-right"><Currency value={ticket.maxPayOut} /></td>

                                        <td><TicketStatus ticket={ticket} /> </td>

                                        {
                                            true == ticket.isCancelled &&
                                            <td></td>
                                        }

                                        <td>
                                            {
                                                true == ticket.isCancelled ?
                                                formatUserDate(ticket.cancelTime) :
                                                    true == ticket.isPayedOut ?
                                                        formatUserDate(ticket.payOutTime) : ""

                                            }
                                        </td>

                                        <td><Currency value={ticket.winBetCount} /></td>
                                        <td><Odd value={ticket.winOdd} /></td>
                                        <td><Currency value={ticket.win} /></td>
                                        <td><Currency value={ticket.winFee} /></td>
                                        <td><Currency value={ticket.payOut} /></td>

                                        <td>
                                            {(false == showBets) &&
                                            <a className="cursor-pointer" onClick={() => this.toggleTicketDetails(ticket.id)}>
                                                <i
                                                    className={classNames("icon", {
                                                        "icon-chevron-down": !showTicketBets,
                                                        "icon-chevron-up": showTicketBets
                                                    })}
                                                />
                                            </a>
                                            }
                                        </td>
                                    </tr>,
                                    (true == showTicketBets) && [
                                        <tr className="bets-row" key={`${ticket.id}-bets`}>
                                            <td colSpan={22} className="p-0">
                                                <div className="bets-table-wrapper">
                                                    <table className="table table-sm m-0 bets-table" style={{fontSize: ".75rem"}}>
                                                        <tbody>
                                                            {ticket.bets && ticket.bets.map((bet, i) => <BetTableRow key={i} bet={bet}/>)}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>,
                                        <tr className="spacer-row" key={`${ticket.id}-spacer`}>
                                            <td colSpan={11}/>
                                        </tr>
                                    ]
                                ]
                            })
                        }
                        </tbody>
                    </table>
                </Col>
            </Row>

        )
    }
}

export default TicketDetailTable;