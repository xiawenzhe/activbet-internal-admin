import React, {Component} from "react";
import {ContainerFluidSpacious, prepareStake} from "../../App/Toolkit";
import {
    Button,
    Col,
    InputGroup,
    Row
} from "reactstrap";
import {connect} from "react-redux";
import {requestTicketStatistics, clearTicketStatistics, resetTicketStatisticFilter} from "../../../actions/ticket";
import TicketStatisticsTable from "./TicketStatisticsTable";
import PaymentDirectionDropDown from "../../Account/PaymentTransaction/PaymentDirectionDropDown";
import {DateTimePicker} from "react-widgets";
import {TICKET_ACTIVES} from "../index";
import Indicator from "../../Module/Indicator";
import IdNameDropDown from "../../Module/IdNameDropDown";
import {selectUserCurrency} from "../../../actions/user";

class TicketStatistics extends Component {

    search = () => {
        this.props.requestTicketStatistics();
    };

    clear = () => {
        this.props.clearTicketStatistics();
    };

    onTicketActiveChange = (id) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            ticketActive: id
        });
    };

    onCustomerUsernameChange = (e) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            customerUsername: e.target.value
        });
    };

    onTicketNumberChange = (e) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            ticketNumber: e.target.value
        });
    };

    onInsertFromChange = (from) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            insertFrom: from
        });
    };

    onInsertToChange = (to) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            insertTo: to
        });
    };

    onStakeFromChange = (e) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            stakeFrom: prepareStake(e.target.value)
        });
    };

    onStakeToChange = (e) => {
        const {ticketStatisticFilter} = this.props;
        this.props.resetTicketStatisticFilter({
            ...ticketStatisticFilter,
            stakeTo: prepareStake(e.target.value)
        });
    };

    onCurrencyChange = (id) => {
        this.props.selectUserCurrency(id);
    };

    render() {
        const {currencies, userCurrencyId} = this.props;
        const {statistics, isLoading} = this.props.ticketStatistics;
        const {
            customerUsername, ticketActive, ticketNumber, insertFrom, insertTo, stakeFrom, stakeTo
        } = this.props.ticketStatisticFilter;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Ticket Statistics</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">

                                {/* select currency */}
                                <div className="ml-3">
                                    <IdNameDropDown
                                        id={userCurrencyId}
                                        addPlaceholder={true}
                                        placeholder="Currency"
                                        list={currencies}
                                        listLoading={isLoading}
                                        selectionChanged={this.onCurrencyChange}
                                        showDeleteButton={false}
                                    />
                                </div>

                                {/* customerUsername */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Customer username"
                                               value={customerUsername}
                                               onChange={this.onCustomerUsernameChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* ticketActive */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={ticketActive}
                                                              addPlaceholder={true}
                                                              placeholder="Select status"
                                                              list={TICKET_ACTIVES}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onTicketActiveChange}/>
                                </div>

                                {/* ticketNumber */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Ticket number"
                                               value={ticketNumber}
                                               onChange={this.onTicketNumberChange}
                                        />
                                    </InputGroup>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* insertFrom */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={insertFrom} format="L LT" placeholder="Insert from" onChange={this.onInsertFromChange}/>
                                </div>

                                {/* insertTo */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={insertTo} format="L LT" placeholder="Insert to" onChange={this.onInsertToChange}/>
                                </div>

                                {/* stakeFrom */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Stake from"
                                               value={stakeFrom}
                                               onChange={this.onStakeFromChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* stakeTo */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Stake to"
                                               value={stakeTo}
                                               onChange={this.onStakeToChange}
                                        />
                                    </InputGroup>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.search}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clear}
                                    >
                                        Clear
                                    </Button>
                                </div>

                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    (true == isLoading) &&
                    <div className="text-center">
                        <Indicator animate className="loading-indicator mt-3"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

                {
                    statistics && statistics.length > 0 &&
                    <TicketStatisticsTable
                        statistics={statistics}
                    />
                }

                {
                    null == statistics &&
                    <div>No record</div>
                }

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {ticketStatistics, ticketStatisticFilter} = state.ticket;
    const {userCurrencyId, currencies} = state.user;

    return {
        ticketStatistics, ticketStatisticFilter,
        userCurrencyId, currencies
    };
}

const mapDispatchToProps = {
    requestTicketStatistics,
    clearTicketStatistics,
    resetTicketStatisticFilter,
    selectUserCurrency
};

export default connect(mapStateToProps, mapDispatchToProps)(TicketStatistics);