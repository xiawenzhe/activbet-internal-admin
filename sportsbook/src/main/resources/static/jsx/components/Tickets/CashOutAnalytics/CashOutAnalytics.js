import React, {Component} from "react";
import {ContainerFluidSpacious, Currency, prepareStake} from "../../App/Toolkit";
import {
    Button,
    Col,
    InputGroup,
    Row
} from "reactstrap";
import {connect} from "react-redux";
import {requestCashOutAnalytics, clearCashOutAnalytics, resetCashOutAnalyticsFilter} from "../../../actions/ticket";
import {DateTimePicker} from "react-widgets";
import Indicator from "../../Module/Indicator";

class CashOutAnalytics extends Component {

    search = () => {
        const {from, to} = this.props;
        this.props.requestCashOutAnalytics(from, to);
    };

    clear = () => {
        this.props.clearCashOutAnalytics();
    };

    onFromChange = (newFrom) => {
        const {from, to} = this.props;
        this.props.resetCashOutAnalyticsFilter(newFrom, to);
    };

    onToChange = (newTo) => {
        const {from, to} = this.props;
        this.props.resetCashOutAnalyticsFilter(from, newTo);
    };

    render() {
        const {from, to, isLoading, analytics} = this.props;

        return (
            <ContainerFluidSpacious>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Cash Out Analytics</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* from */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                                </div>

                                {/* to */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.search}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clear}
                                    >
                                        Clear
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    analytics &&
                    <Row className="mt-3">
                        <Col>
                            <table className="table table-striped">
                                <thead className="thead-xs">
                                    <tr>

                                        <th colSpan={4} className="text-center">Lost Tickets</th>
                                        <th colSpan={5} className="text-center">Won Tickets</th>
                                        <th colSpan={4} className="text-center">Total</th>
                                    </tr>

                                    <tr>
                                        <th>#Count</th>
                                        <th>Stake</th>
                                        <th>Cash Out</th>
                                        <th>Cash Flow</th>

                                        <th>#Count</th>
                                        <th>Stake</th>
                                        <th>Soll Pay Out</th>
                                        <th>Cash Out</th>
                                        <th>Cash Flow</th>

                                        <th>#Count</th>
                                        <th>Stake</th>
                                        <th>Cash Out</th>
                                        <th>Cash Flow</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <th>{analytics.lostCount}</th>
                                        <th><Currency value={analytics.lostStake}/></th>
                                        <th><Currency value={analytics.lostCashOut}/></th>
                                        <th><Currency value={analytics.lostCashFlow}/></th>

                                        <th>{analytics.wonCount} / {analytics.partialWonCount} (partial won)</th>
                                        <th><Currency value={analytics.wonStake}/></th>
                                        <th><Currency value={analytics.wonMaxPayOut}/></th>
                                        <th><Currency value={analytics.wonCashOut}/></th>
                                        <th><Currency value={analytics.lostCashFlow}/></th>


                                        <th>{analytics.count}</th>
                                        <th><Currency value={analytics.stake}/></th>
                                        <th><Currency value={analytics.cashOut}/></th>
                                        <th><Currency value={analytics.cashFlow}/></th>
                                    </tr>
                                </tbody>
                            </table>

                            <div className="dashhead-titles">
                                <h6 className="dashhead-title">Cash Out Advantage: <Currency value={analytics.cashOutAdvantage} /></h6>
                            </div>
                        </Col>
                    </Row>
                }


                {
                    (true == isLoading) &&
                    <div className="text-center">
                        <Indicator animate className="loading-indicator mt-3"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {cashOutAnalyticsFilter, cashOutAnalytics} = state.ticket;
    const {isLoading, analytics} = cashOutAnalytics;
    const {from, to} = cashOutAnalyticsFilter;
    return {
        from, to, isLoading, analytics
    };
}

const mapDispatchToProps = {
    requestCashOutAnalytics, clearCashOutAnalytics, resetCashOutAnalyticsFilter
};

export default connect(mapStateToProps, mapDispatchToProps)(CashOutAnalytics);