import React, {Component} from "react";
import {connect} from "react-redux";
import {Modal, ModalHeader, ModalBody, ModalFooter, Button, Row, Col} from "reactstrap";
import classNames from "classnames";
import Indicator from "../../Module/Indicator";
import {Currency, formatUserDate, getSystemsText, Odd} from "../../App/Toolkit";
import TicketStatus from "../TicketStatus";
import {requestCancelTicket, closeTicketEditor} from "../../../actions/ticket";
import {SportIcon} from "../../App/Icons";
import TicketTipResult from "../TicketTipResult";


const OPEN_GAME_STATUS = ["None", "NotStarted", "Delayed", "Starting"];

class TicketEvaluation extends Component {

    cancelTicket = (ticket) => {
        this.props.requestCancelTicket(ticket).then(
            () => {
                // refresh

                // close
                this.props.closeTicketEditor();
            }
        );
    };

    render() {
        const {ticket, isLoading} = this.props;

        if (!ticket) {
            return null;
        }

        // can cancel : ticket.isOpen && game not started
        let isCancelable = false;
        if (true == ticket.isOpen) {

            /*
            if (ticket.bets && ticket.bets.length > 0) {
                isCancelable = true;
                ticket.bets.forEach(bet => {
                    if (!OPEN_GAME_STATUS.includes(bet.gameStatus)) {
                        isCancelable = false;
                        return;
                    }
                });
            } else {
                isCancelable = true;
            }
            */

            isCancelable = true;
        }

        return (
            <Modal className="ticket-modal" backdrop="static"
                   toggle={this.props.closeTicketEditor}
                   keyboard fade
                   isOpen={ticket !== null} size="xl"
            >
                <ModalHeader toggle={this.props.closeTicketEditor} tag="h6">
                    <small className="text-muted mr-3">Ticket</small>
                    {ticket.number}
                </ModalHeader>

                {(true == isLoading) ? <ModalBody className="text-center"><Indicator className="mt-3 mb-3" animate/></ModalBody>
                    :
                    <ModalBody className="pl-0 pr-0 pb-0">
                        <Row className="pl-3 pr-3">
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        {formatUserDate(ticket.insertTime)}
                                    </h5>
                                    <span className="statcard-desc">Insert Time</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        {ticket.customerUser}
                                        <span className="customer-id text-muted">{ticket.customerUserId}</span>
                                    </h5>
                                    <span className="statcard-desc">User</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        {getSystemsText(ticket)}
                                    </h5>
                                    <span className="statcard-desc">Systems</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        <TicketStatus ticket={ticket}/>
                                    </h5>
                                    <span className="statcard-desc">Status</span>
                                </div>
                            </Col>
                        </Row>
                        <Row className="pl-3 pr-3">
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        <Currency value={ticket.stake}/>
                                    </h5>
                                    <span className="statcard-desc">Stake</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        <Currency value={ticket.maxPayOut}/>
                                    </h5>
                                    <span className="statcard-desc">Max Pay Out</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        <Currency value={ticket.possiblePayOut}/>
                                    </h5>
                                    <span className="statcard-desc">Possible Pay Out</span>
                                </div>
                            </Col>
                            <Col xs={3}>
                                <div className="statcard pl-3 pr-3 p-2">
                                    <h5 className="statcard-number">
                                        <Currency value={ticket.payOut}/>
                                    </h5>
                                    <span className="statcard-desc">Pay Out</span>
                                </div>
                            </Col>
                        </Row>

                        <hr/>
                        <h5 className="subheader pt-3">Systems</h5>
                        <table className="table table-striped table-sm mb-3">
                            <thead className="thead-xs">
                            <tr>
                                <th>System</th>
                                <th className="text-right">Max Odd</th>
                                <th className="text-right">Bets</th>
                                <th className="text-right">Stake</th>
                                <th className="text-right">Max Pay Out</th>
                                <th className="text-right">Pay Out</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                ticket.systems.map((system, i) => (
                                <tr key={system.minSystem} className={classNames({
                                    "text-success": system.isWin === true,
                                    "text-danger": system.isWin === false
                                })}
                                >
                                    <td>{system.bankCount > 0 && `${system.bankCount}B + `}{system.minSystem}
                                        {" "}of {system.maxSystem}</td>
                                    <td className="text-right"><Odd value={system.maxOdd}/></td>
                                    <td className="text-right">{system.betRowCount}</td>
                                    <td className="text-right"><Currency value={system.stake}/></td>
                                    <td className="text-right"><Currency value={system.maxPayOut}/></td>
                                    <td className="text-right">{system.payOut > 0 ?
                                        <Currency value={system.payOut}/> : <span>&ndash;</span>}
                                    </td>
                                </tr>
                                    )
                                )
                            }
                            </tbody>
                        </table>
                        <div className="d-flex">
                            <h5 className="subheader pt-4">Bets</h5>
                            {/*
                            <div className="ml-auto align-self-end pb-2" style={{marginRight: "2rem"}}>
                                <Button color="warning" onClick={this.setAllToRefund}>Set Refund</Button>
                            </div>
                            */}
                        </div>
                        <table className="table table-striped table-sm mb-3">
                            <tbody>
                            {
                                ticket.bets && ticket.bets.map((bet, i) =>
                                <tr key={i}>
                                    <td className="pl-3 align-middle">
                                        {formatUserDate(bet.gameStartTime)}
                                    </td>
                                    <td className="pt-0 pb-0 align-middle">
                                        <div>
                                            <SportIcon iconName={bet.sportIconName}
                                                       className="tickets-sport-icon"/>
                                            {bet.sportName}
                                            <span className="divider">&middot;</span>
                                            <span
                                                className={`sprite__navside-flag__${bet.categoryIconName}`}>
                                                {bet.categoryName}
                                            </span>
                                            <span className="divider">&middot;</span>
                                            {bet.tournamentName}
                                            <div>{bet.rivalNames && bet.rivalNames.join(" - ")}</div>
                                        </div>
                                    </td>
                                    <td className="align-middle">
                                        {(true == bet.isBank) && <span className="bet-bank" title="Bank">B</span>}
                                        {bet.tipName}
                                    </td>
                                    <td className={classNames("align-middle", {
                                        "text-success": (true == bet.isWon) || bet.adjustedOddValue,
                                        "text-danger": true == bet.isLost
                                    })}>{bet.oddName}</td>

                                    <td className="align-middle" title={(() => {
                                        if (true == bet.isWon) return "Won";
                                        if (true == bet.isLost) return "Lost";
                                        if (true == bet.isRefund) return "Refunded";
                                        if (true == bet.isHalfLost) return "Half Lost";
                                        if (true == bet.isHalfWon) return "Half Won";
                                    })()}>
                                        <Odd isWon={!bet.adjustedOddValue && (true == bet.isWon)}
                                             isLost={true == bet.isLost}
                                             value={bet.oddValue}
                                        />
                                        {bet.adjustedOddValue &&
                                        <span> (<Odd isWon={true} value={bet.adjustedOddValue}/>)</span>}
                                    </td>
                                    <td className="align-middle">
                                        { (true == bet.isLive) &&
                                        <span className="bet-live">
                                            LIVE
                                            <span>@{bet.scoreHome}:{bet.scoreAway}</span>
                                        </span>
                                        }
                                    </td>
                                    <td className="text-right align-middle pr-2">
                                        {bet.gameStatus === "Cancelled" && "Cancelled"}
                                        <TicketTipResult sportIconName={bet.sportIconName}
                                                results={bet.gameResults}/>
                                    </td>
                                </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </ModalBody>
                }
                <ModalFooter>
                    <Button color="primary" className="cursor-pointer" disabled={false == isCancelable || true == isLoading} onClick={() => {this.cancelTicket(ticket)}}>
                        {
                            true == isLoading ? "Cancelling..." :
                            true == isCancelable ? "Cancel Ticket" : "Not cancelable"
                        }
                    </Button>
                    <Button color="secondary" className="cursor-pointer" disabled={true == isLoading} onClick={() => {this.props.closeTicketEditor()}}>Close</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    const {ticketEdit, isEvaluatingTicket} = state.ticket;
    return {
        ticket: ticketEdit,
        isLoading: isEvaluatingTicket
    };
}

const mapDispatchToProps = {
    requestCancelTicket, closeTicketEditor
};

export default connect(mapStateToProps, mapDispatchToProps)(TicketEvaluation);