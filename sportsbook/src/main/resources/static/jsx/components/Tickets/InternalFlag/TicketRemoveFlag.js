import React, {Component} from "react";
import Tickets from "./index";

class TicketRemoveFlag extends Component {

    render() {
        return (
            <Tickets
                flag={false}
            />
        )
    }
}

export default TicketRemoveFlag;
