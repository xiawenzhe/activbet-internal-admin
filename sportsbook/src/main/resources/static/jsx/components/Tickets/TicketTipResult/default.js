import React from "react";
import {splitEndAndPeriods} from "./utils";

const DefaultTicketTipResult = ({results}) => {

    const {endResult, periodResults} = splitEndAndPeriods(results);

    return (
        <span>
            <strong>{endResult.home}:{endResult.away}</strong>{" "}
            {periodResults.map(result =>
                <span key={result.playTimePeriodFlag}>
                    ({result.home}:{result.away}){" "}
                </span>
            )}
        </span>
    )
};

export default DefaultTicketTipResult;