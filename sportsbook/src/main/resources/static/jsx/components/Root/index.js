import {Provider} from "react-redux";
import {Router} from "react-router/es";
import I18NProvider from "../I18NProvider";
import {calculateResponsiveState} from "redux-responsive";
import React, {Component} from "react";
import routes from "../../routes";
import RouterUtils from "../../utils/Router";

class Root extends Component {

    componentDidMount() {
        this.props.store.dispatch(calculateResponsiveState(window));
    }

    render() {
        const {history, store} = this.props;
        return (
            <Provider store={store}>
                <I18NProvider>
                    <Router ref={r => { if (r) RouterUtils.setRouter(r.router) }} key={Math.random()} history={history}>
                        {routes}
                    </Router>
                </I18NProvider>
            </Provider>
        )
    }
}

export default Root;