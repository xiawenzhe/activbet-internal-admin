import React, {Component} from "react";
import classNames from "classnames";
import {Route} from "react-router-dom";
import {DropdownMenu, DropdownToggle, Dropdown} from "reactstrap";

class RouterNavDropdown extends Component {
    state = {
        open: false
    };

    render() {
        const { label, children, to, pathname} = this.props;
        const { open } = this.state;

        /*
        const filteredChildren = children.filter(child => child !== false);
        const subLinks = React.Children.map(filteredChildren, child =>
            React.cloneElement(child, {
                "to": `${to}${child.props.to}`,
                "onClick": () => this.setState({ open: !open })
            })
        );

         */

        return (
            <Dropdown nav
                      toggle={() => this.setState({open: !open})}
                      isOpen={this.state.open}
                      className={classNames("ml-3", {"active": pathname.startsWith(to)})}>
                <DropdownToggle tag="a" caret
                                className="nav-link cursor-pointer">
                    {label}
                </DropdownToggle>
                <DropdownMenu>
                    {children}
                </DropdownMenu>
            </Dropdown>
        )
    }
}

export default RouterNavDropdown;