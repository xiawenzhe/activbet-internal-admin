import React, {Component} from "react";
import {Link} from "react-router";
import {NavItem} from "reactstrap";
import classNames from "classnames";

export class RouterDropdownItem extends Component {
    render() {
        const {to, children, onClick, pathname} = this.props;

        return <Link className={classNames("dropdown-item", {'active': pathname == to})} onClick={onClick} to={to}>{children}</Link>

    }
}

export class RouterNavItem extends Component {
    render() {
        const {to, children, onClick, exact, pathname} = this.props;
        return (
            <NavItem className={classNames("ml-3", {'active': pathname == to})}>
                <Link className="nav-link" onClick={onClick} to={to}>{children}</Link>
            </NavItem>
        );
    }
}