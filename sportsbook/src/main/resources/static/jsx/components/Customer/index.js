import React, {Component} from "react";
import {ContainerFluidSpacious, Currency, getPopupDimensions} from "../App/Toolkit";
import {
    Button,
    Col,
    Row,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    UncontrolledTooltip,
} from 'reactstrap';
import {requestCompanyListIfNeeded} from "../../actions/company";
import {resetSearchCustomerFilter, clearSearchCustomers, requestSearchCustomers} from "../../actions/customer";
import {connect} from "react-redux";
import CompaniesDropDown from "../Company/CompaniesDropDown"
import PageListPagination from "../App/PageListPagination";
import Indicator from "../Module/Indicator";


class Customer extends Component {
    componentDidMount() {
        this.props.requestCompanyListIfNeeded();
    }

    onCompanyChange = (id) => {
        const {customerSearchFilter} = this.props;
        const {keyWord} = customerSearchFilter;
        this.props.resetSearchCustomerFilter(id, keyWord);
    };

    onSearchChange = (e) => {
        const {customerSearchFilter} = this.props;
        const {companyId} = customerSearchFilter;
        this.props.resetSearchCustomerFilter(companyId, e.target.value);
    };

    searchCustomers = () => {
        const {pageIndex, pageSize} = this.props.customerSearchPagination;
        this.onChangePageAction(pageIndex, pageSize);
    };

    clearCustomers = () => {
        this.props.clearSearchCustomers();
    };

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestSearchCustomers(pageIndex, pageSize);
    };

    popupAccountBookings = (username) => {
        window.open(
            `/customer/account_bookings?popup&username=${username}`,
            "_blank",
            getPopupDimensions(screen.width * 0.8, screen.height * 0.8));
    };

    popupPaymentTransactions = (username) => {
        window.open(
        `/account/payment_transactions?popup&username=${username}`,
        "_blank",
            getPopupDimensions(screen.width * 0.8, screen.height * 0.8));
    };

    popupPayoutRequest = (username) => {
        window.open(
            `/account/payout_requests?popup&username=${username}`,
            "_blank",
            getPopupDimensions(screen.width * 0.8, screen.height * 0.8));
    };

    popupTicketRequest = (username) => {
        window.open(
            `/ticket/tickets?popup&username=${username}`,
            "_blank",
            getPopupDimensions(screen.width * 0.8, screen.height * 0.8));
    };


    render() {
        const {companyList, customerSearchFilter, customers, customerSearchPagination} = this.props;
        const {companyId, keyWord} = customerSearchFilter;
        const isLoadingCustomers = customers.isLoading;
        const customerResults = customers.customers;

        return (
            <ContainerFluidSpacious>
                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Customers</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                <div className="ml-3">
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="icon icon-magnifying-glass"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <input className="form-control" type="text"
                                               value={keyWord}
                                               placeholder="Username"
                                               onChange={this.onSearchChange}
                                        />
                                    </InputGroup>
                                </div>
                                <div className="ml-3">
                                    <CompaniesDropDown id={companyId}
                                                       addPlaceholder={true}
                                                       placeholder="Select company"
                                                       list={companyList}
                                                       listLoading={isLoadingCustomers}
                                                       selectionChanged={this.onCompanyChange}/>
                                </div>

                                {/*
                                <div className="ml-3">
                                    {this.renderUserTypeDropDown()}
                                </div>

                                <div className="ml-3">
                                    <Button color="success" onClick={() => this.onEditCustomer(0)}>New</Button>
                                </div>
                                */}

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoadingCustomers}
                                            onClick={this.searchCustomers}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clearCustomers}
                                    >
                                        Clear
                                    </Button>

                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    true == isLoadingCustomers &&
                    <div className="text-center mt-5">
                        <Indicator animate className="loading-indicator"/>
                        <div className="dashhead-subtitle mt-3">Loading</div>
                    </div>
                }

                {
                    customerResults && customerResults.length > 0 &&
                    <Row className="mt-3">
                        <Col>
                            <table className="table table-striped">
                                <thead className="thead-xs">
                                <tr>
                                    <th className="pl-3" style={{width: "4rem"}}>Id</th>
                                    <th>Company</th>
                                    <th>UserName</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Balance</th>
                                    <th style={{width: "1rem"}}>Active</th>

                                    <th style={{width: "1rem"}}>
                                        {/* Account Bookings */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Payment Transactions */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Payout Requests */}
                                    </th>

                                    <th style={{width: "1rem"}}>
                                        {/* Tickets */}
                                    </th>
                                    <th style={{width: "1rem"}}>
                                        {/* Edit */}
                                    </th>
                                </tr>
                                </thead>
                                {
                                    customerResults &&
                                    <tbody>
                                    {
                                        customerResults.map(customer => (
                                            <tr key={customer.userId}>
                                                <td>{customer.userId}</td>
                                                <td>{customer.companyShortcut}</td>
                                                <td>{customer.username}</td>
                                                <td>{customer.firstName}</td>
                                                <td>{customer.lastName}</td>
                                                <td>{customer.email}</td>
                                                <td>{customer.mobile}</td>
                                                <td><Currency value={customer.balance} /></td>
                                                <td>{true == customer.activeNow ? <i className="icon icon-check"/> : null}</td>

                                                <td className="p-1 text-center">
                                                    <Button id={"AccountBooking" + customer.userId}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            onClick={() => {this.popupAccountBookings(customer.username)}}
                                                    >
                                                        AB
                                                        <UncontrolledTooltip placement="right" target={"AccountBooking" + customer.userId}>
                                                            Get Account Bookings for <br/>{customer.username}
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                </td>

                                                <td className="p-1 text-center">
                                                    <Button id={"Payment" + customer.userId}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            onClick={() => this.popupPaymentTransactions(customer.username)}
                                                    >
                                                        PT
                                                        <UncontrolledTooltip placement="right" target={"Payment" + customer.userId}>
                                                            Get Payment Transactions for <br/>{customer.username}
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                </td>

                                                <td className="p-1 text-center">
                                                    <Button id={"PayOutRequests" + customer.userId}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            onClick={() => {this.popupPayoutRequest(customer.username)}}
                                                    >
                                                        PO
                                                        <UncontrolledTooltip placement="right" target={"PayOutRequests" + customer.userId}>
                                                            Show payout requests for <br/>{customer.username}
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                </td>

                                                <td className="p-1 text-center">
                                                    <Button id={"Tickets" + customer.userId}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            onClick={() => {this.popupTicketRequest(customer.username)}}
                                                    >
                                                        T
                                                        <UncontrolledTooltip placement="right" target={"Tickets" + customer.userId}>
                                                            Show tickets for <br/>{customer.username}
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                </td>

                                                <td className="p-1 text-center">
                                                    <Button id={"EditButton" + customer.userId}
                                                            className="p-1"
                                                            color="outline-primary"
                                                            style={{width: "2rem", height: "2rem"}}
                                                            size="sm"
                                                            onClick={() => {}}
                                                    >
                                                        E
                                                        <UncontrolledTooltip placement="right" target={"EditButton" + customer.userId}>
                                                            Edit <br/>{customer.username}
                                                        </UncontrolledTooltip>
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                }
                            </table>
                        </Col>
                    </Row>
                }

                {
                    customerResults && customerResults.length > 0 &&
                    <Row>
                        <PageListPagination pageList={customerSearchPagination}
                                            loading={isLoadingCustomers}
                                            changePageAction={this.onChangePageAction}/>
                    </Row>
                }

                {
                    null == customerResults && <div>No record</div>
                }

            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {companyList} = state.company;
    const {customerSearchFilter, customerSearchPagination, customers} = state.customer;
    return {
        companyList, customerSearchFilter, customerSearchPagination, customers
    };
}

const mapDispatchToProps = {
    requestCompanyListIfNeeded, requestSearchCustomers, resetSearchCustomerFilter, clearSearchCustomers
};

export default connect(mapStateToProps, mapDispatchToProps)(Customer);