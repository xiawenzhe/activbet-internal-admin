import React, {Component} from "react";
import {ContainerFluidSpacious, Currency, getPopupDimensions} from "../../App/Toolkit";
import {connect} from "react-redux";
import {Button, Col, InputGroup, InputGroupAddon, InputGroupText, Row} from "reactstrap";
import PaymentDirectionDropDown from "../../Account/PaymentTransaction/PaymentDirectionDropDown";
import {DateTimePicker} from "react-widgets";
import {resetCustomerAccountBookingsFilter, requestCustomerAccountBookings, clearCustomerAccountBookings} from "../../../actions/customer";
import {showGeneralError} from "../../../actions/app";
import {parse} from "qs";
import AccountBookingTable from "./AccountBookingTable";
import PageListPagination from "../../App/PageListPagination";

export const ACCOUNT_BOOKING_TYPES = [
    {id: 1, name: "DIRECT_PAYIN"},
    {id: 2, name: "DIRECT_PAYOUT"},
    {id: 3, name: "PAYOUT_REQUEST"},
    {id: 10, name: "PRE_PAYMENT"},
    {id: 15, name: "EXTERNAL_PAY_IN"},
    {id: 16, name: "EXTERNAL_PAY_OUT"},
    {id: 21, name: "STAKE"},
    {id: 22, name: "WIN"},
    {id: 23, name: "CASHOUT"}
];

export const ACCOUNT_BOOKING_REASONS = [
    {id: 0, name: "INSERT"},
    {id: 1, name: "DELETE"},
    {id: 2, name: "CANCEL"},
    {id: 3, name: "PATCH"},
    {id: 4, name: "PAY_IN"},
    {id: 5, name: "PAY_OUT"}
];

export const ACCOUNT_BOOKING_CATEGORIES = [
    {id: 1, name: "ALL"},
    {id: 2, name: "PAYMENT"},
    {id: 3, name: "TICKET"},
];

class AccountBooking extends Component {

    componentDidMount() {
        /*
        http://localhost:9988/customer/account_bookings?username=xiaxi421&popup
        */
        const search = this.props.location;
        if (search) {
            const query = parse(location.search.substr(1));
            const username = query.username;
            if (username) {
                const {customerAccountBookingsFilter} = this.props;
                this.props.resetCustomerAccountBookingsFilter(
                    {
                        ...customerAccountBookingsFilter,
                        customerUsername: username
                    }
                ).then(
                    () => {
                        const {pageIndex, pageSize} = this.props.customerAccountBookingsPagination;
                        this.props.requestCustomerAccountBookings(pageIndex, pageSize);
                    }
                );
            }
        }
    }

    onCustomerUsernameChange = (e) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                customerUsername: e.target.value
            }
        );
    };

    onBookingTypeChange = (id) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                bookingType: id
            }
        );
    };

    onBookingReasonChange = (id) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                bookingReason: id
            }
        );
    };

    onBookingCategoryChange = (id) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                category: id
            }
        );
    };

    onFromChange = (from) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                from: from
            }
        );
    };

    onToChange = (to) => {
        const {customerAccountBookingsFilter} = this.props;
        this.props.resetCustomerAccountBookingsFilter(
            {
                ...customerAccountBookingsFilter,
                to: to
            }
        );
    };

    searchAccountBookings = () => {
        // check username
        const {customerAccountBookingsFilter} = this.props;
        const {customerUsername} = customerAccountBookingsFilter;
        if (!customerUsername || customerUsername.trim().length == 0) {
            const e = {error: "Please select at least one customer"};
            this.props.showGeneralError(e);
            return;
        }

        const {pageIndex, pageSize} = this.props.customerAccountBookingsPagination;
        this.props.requestCustomerAccountBookings(pageIndex, pageSize);
    };

    onChangePageAction = (pageIndex, pageSize) => {
        this.props.requestCustomerAccountBookings(pageIndex, pageSize);
    };

    clearAccountBookings = () => {
        this.props.clearCustomerAccountBookings();
    };

    render() {
        const {customerUsername, category, bookingType, bookingReason, from, to} = this.props.customerAccountBookingsFilter;
        const {isLoading, bookings} = this.props.customerAccountBookings;
        const {customerAccountBookingsPagination} = this.props;

        return (
            <ContainerFluidSpacious>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-titles">
                                <h3 className="dashhead-title">Customer Account Bookings</h3>
                            </div>

                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* tickets, payments */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={category}
                                                              addPlaceholder={true}
                                                              placeholder="Select booking category"
                                                              list={ACCOUNT_BOOKING_CATEGORIES}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onBookingCategoryChange}/>
                                </div>

                                {/* customerUsername */}
                                <div className="ml-3">
                                    <InputGroup style={{width: "14rem"}}>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="icon icon-magnifying-glass"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <input className="form-control"
                                               type="text"
                                               placeholder="Customer username"
                                               value={customerUsername}
                                               onChange={this.onCustomerUsernameChange}
                                        />
                                    </InputGroup>
                                </div>

                                {/* type */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={bookingType}
                                                              addPlaceholder={true}
                                                              placeholder="Select booking type"
                                                              list={ACCOUNT_BOOKING_TYPES}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onBookingTypeChange}/>
                                </div>

                                {/* reason */}
                                <div className="ml-3">
                                    <PaymentDirectionDropDown id={bookingReason}
                                                              addPlaceholder={true}
                                                              placeholder="Select booking reason"
                                                              list={ACCOUNT_BOOKING_REASONS}
                                                              listLoading={isLoading}
                                                              selectionChanged={this.onBookingReasonChange}/>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <Col>
                        <div className="dashhead mt-1">
                            <div className="dashhead-toolbar d-flex align-items-center">
                                {/* from */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={from} format="L LT" placeholder="From" onChange={this.onFromChange}/>
                                </div>

                                {/* to */}
                                <div className="ml-3" style={{width: "18rem"}}>
                                    <DateTimePicker value={to} format="L LT" placeholder="To" onChange={this.onToChange}/>
                                </div>

                                <div className="ml-3">
                                    <Button color="primary"
                                            disabled={isLoading}
                                            onClick={this.searchAccountBookings}
                                    >
                                        Search
                                    </Button>

                                    <Button color="dark"
                                            onClick={this.clearAccountBookings}
                                    >
                                        Clear
                                    </Button>
                                </div>

                            </div>
                        </div>
                    </Col>
                </Row>

                {
                    bookings && bookings.length > 0 &&
                    <AccountBookingTable
                        bookings={bookings}
                        isLoading={isLoading}
                    />
                }

                {
                    bookings && bookings.length > 0 &&
                    <Row>
                        <PageListPagination pageList={customerAccountBookingsPagination}
                                            isLoading={isLoading}
                                            changePageAction={this.onChangePageAction}
                        />
                    </Row>
                }
            </ContainerFluidSpacious>
        )
    }
}

function mapStateToProps(state) {
    const {customerAccountBookingsFilter, customerAccountBookingsPagination, customerAccountBookings} = state.customer;
    return {
        customerAccountBookingsFilter, customerAccountBookingsPagination, customerAccountBookings
    };
}

const mapDispatchToProps = {
    resetCustomerAccountBookingsFilter, requestCustomerAccountBookings, clearCustomerAccountBookings, showGeneralError
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountBooking);