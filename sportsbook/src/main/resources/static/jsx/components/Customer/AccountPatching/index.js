import React, {Component} from "react";
import {connect} from "react-redux";
import {requestSingleCustomerById, requestPatchCashierAccount} from "../../../actions/customer";
import {showGeneralError} from "../../../actions/app";
import {Currency, formatUserDate, prepareStake} from "../../App/Toolkit";

const ACTION_PAY_IN = "Pay-In";
const ACTION_PAY_OUT = "Pay-Out";

class AccountPatching extends Component {

    state = {
        lastAction: "",

        payInValue: "",
        payInComment: "",
        payOutValue: "",
        payOutComment: "",

        isLoadingCashier: false,
        isLoadingPayIn: false,
        isLoadingPayOut: false,

        cashierDetail: null,
        detailRequestTime: null
    };

    componentWillReceiveProps(nextProps, nextContext) {
        const {cashier} = nextProps;
        if (cashier && this.props.cashier) {
            if (cashier.userId !== this.props.cashier.userId) {
                this.setState({
                    payInValue: "",
                    payInComment: "",
                    payOutValue: "",
                    payOutComment: "",

                    isLoadingCashier: false,
                    isLoadingPayIn: false,
                    isLoadingPayOut: false,

                    cashierDetail: null,
                    detailRequestTime: null
                })
            }
        }
    }

    refresh = () => {
        this.setState({
            lastAction: ""
        });

        this.refreshAccountBalance();
    };

    refreshAccountBalance = () => {
        const customerId = this.props.cashier.userId;

        this.setState({
            isLoadingCashier: true
        });
        this.props.requestSingleCustomerById(customerId).then(
            ({customer, requestTimeStamp}) => {
                this.setState({
                    isLoadingCashier: false,
                    cashierDetail: customer,
                    detailRequestTime: requestTimeStamp
                })
            }
        ).catch(
            (error) => {
                this.setState({
                    isLoadingCashier: false
                });
                this.props.showGeneralError(error);
            }
        )
    };

    payInCustomerAccount = () => {
        const customerId = this.props.cashier.userId;
        const direction = 0;
        const value = this.state.payInValue;
        const text = this.state.payInComment;

        if (!customerId || !value) {
            return;
        }

        this.setState({
            isLoadingPayIn: true
        });
        this.props.requestPatchCashierAccount(customerId, direction, value, text).then(
            () => {
                this.setState({
                    isLoadingPayIn: false,
                    lastAction: ACTION_PAY_IN
                });
                this.refreshAccountBalance();
            }
        ).catch(
            (error) => {
                this.setState({
                    isLoadingPayIn: false,
                });
                this.props.showGeneralError(error);
            }
        )
    };

    onPayInValueChange = (e) => {
        this.setState({
            payInValue: prepareStake(e.target.value)
        })
    };

    onPayInCommentChange = (e) => {
        this.setState({
            payInComment: e.target.value
        })
    };

    onPayOutValueChange = (e) => {
        this.setState({
            payOutValue: prepareStake(e.target.value)
        })
    };

    onPayOutCommentChange = (e) => {
        this.setState({
            payOutComment: e.target.value
        })
    };

    payOutCustomerAccount = () => {
        const customerId = this.props.cashier.userId;
        const direction = 1;
        const value = this.state.payOutValue;
        const text = this.state.payOutComment;

        if (!customerId || !value) {
            return;
        }

        this.setState({
            isLoadingPayOut: true
        });
        this.props.requestPatchCashierAccount(customerId, direction, value, text).then(
            () => {
                this.setState({
                    isLoadingPayOut: false,
                    lastAction: ACTION_PAY_OUT
                });
                this.refreshAccountBalance();
            }
        ).catch(
            (error) => {
                this.setState({
                    isLoadingPayOut: false,
                });
                this.props.showGeneralError(error);
            }
        )
    };

    render() {
        const {cashier} = this.props;
        const {
            lastAction,

            payInValue,
            payInComment,
            payOutValue,
            payOutComment,

            isLoadingCashier,
            isLoadingPayIn,
            isLoadingPayOut,

            cashierDetail,
            detailRequestTime
        } = this.state;

        let lastActionValue = "";
        let lastActionComment = "";
        if (lastAction == ACTION_PAY_IN) {
            lastActionValue = payInValue;
            lastActionComment = payInComment;
        }
        if (lastAction == ACTION_PAY_OUT) {
            lastActionValue = payOutValue;
            lastActionComment = payOutComment;
        }

        const buttonDisabled = (true == isLoadingCashier) || (true == isLoadingPayIn) || (true == isLoadingPayOut);

        return (
            <div className="container-fluid-spacious border-bottom" key={cashier.userId} >
                <div className="mt-lg-3 row">
                    <div className="dashhead-titles">
                        <h6 style={{color: "#F1BF42"}}>{cashier.username}</h6>
                        <h6 style={{color: "#F1BF42"}}>Last Action: {lastAction}</h6>
                        <h6 style={{color: "#F1BF42"}}>Last Action Value: {lastActionValue}</h6>
                        <h6 style={{color: "#F1BF42"}}>Last Action Comment: {lastActionComment}</h6>
                        <h6 style={{color: "#F1BF42"}}>Current Balance: {cashierDetail && <Currency value={cashierDetail.balance} />}</h6>
                        <h6 style={{color: "#F1BF42"}}>Current Balance Timestamp: {formatUserDate(detailRequestTime)}</h6>
                    </div>
                </div>

                <div className="mt-lg-3 row">
                    <div className="col" style={{flex: "0 0 25rem"}}>
                        {/* pay in */}
                        <div className="pt-2 pl-2 pr-2 pb-2 card bg-dark">
                            <h6 className="dashhead-title text-center">Pay In</h6>

                            <div className="position-relative form-group">
                                <label>Input Pay In Value</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={payInValue}
                                    onChange={this.onPayInValueChange}
                                />
                            </div>
                            <div className="position-relative form-group">
                                <label>Input Pay In Comment</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={payInComment}
                                    onChange={this.onPayInCommentChange}
                                />
                            </div>

                            <div className="row">
                                <div className="col">
                                    <button className="cursor-pointer mr-auto btn btn-secondary"
                                            disabled={(true == buttonDisabled)}
                                            onClick={() => {this.refresh()}}
                                    >
                                        Refresh Account
                                    </button>
                                </div>

                                <div className="col">
                                    <button className="cursor-pointer cursor-pointer pos-r btn btn-primary float-right"
                                            disabled={true == buttonDisabled}
                                            onClick={() => {this.payInCustomerAccount()}}
                                    >
                                        Pay In
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* pay out */}
                    <div className="col" style={{flex: "0 0 25rem"}}>
                        <div className="pt-2 pl-2 pr-2 pb-2 card bg-dark">
                            <h6 className="dashhead-title text-center">Pay Out</h6>

                            <div className="position-relative form-group">
                                <label>Input Pay Out Value</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={payOutValue}
                                    onChange={this.onPayOutValueChange}
                                />
                            </div>
                            <div className="position-relative form-group">
                                <label>Input Pay Out Comment</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={payOutComment}
                                    onChange={this.onPayOutCommentChange}
                                />
                            </div>

                            <div className="row">
                                <div className="col">
                                    <button className="cursor-pointer mr-auto btn btn-secondary"
                                            disabled={(true == buttonDisabled)}
                                            onClick={
                                                () => {this.refresh();}
                                            }
                                    >
                                        Refresh Account
                                    </button>
                                </div>

                                <div className="col">
                                    <button className="cursor-pointer cursor-pointer pos-r btn btn-primary float-right"
                                            disabled={true == buttonDisabled}
                                            onClick={() => {this.payOutCustomerAccount()}}
                                    >
                                        Pay Out
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {};
}

const mapDispatchToProps = {
    requestSingleCustomerById, requestPatchCashierAccount, showGeneralError
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPatching);