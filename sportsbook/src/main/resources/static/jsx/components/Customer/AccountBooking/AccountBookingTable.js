import React, {Component} from "react";
import {Col} from "reactstrap";
import {Currency, formatUserDate} from "../../App/Toolkit";


class AccountBookingTable extends Component {

    /*
    private Integer id;
    private String insertTime;
    private String bookingReason;
    private String bookingType;
    private String text;
    private BigDecimal value;
    private BigDecimal balance;
    private String externalReferenceId;
    private String externalReferenceType;
    private ActionCustomer insertUser;
    private Company company;
    private SystemClient systemClient;
    private String remoteAddress;
    private String userAgent;
    */

    render() {
        const {bookings} = this.props;
        return (
            <Col>
                <table className="table table-striped">
                    <thead className="thead-xs">
                        <tr>
                            <th>id</th>
                            <th>Insert Time</th>
                            <th>Booking Reason</th>
                            <th>Booking Type</th>
                            <th>Text</th>
                            <th>Ticket</th>
                            <th>B. Before</th>
                            <th>Value</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        bookings && bookings.map(booking =>
                        <tr key={booking.id}>
                            <td>{booking.id}</td>
                            <td>{formatUserDate(booking.insertTime)}</td>
                            <td>{booking.bookingReason}</td>
                            <td>{booking.bookingType}</td>
                            <td>{booking.text}</td>
                            <td>{booking.ticketNumber}</td>
                            <td><Currency value={booking.balanceBefore} /> </td>
                            <td><Currency value={booking.value} /> </td>
                            <td><Currency value={booking.balance} /> </td>
                        </tr>
                        )
                    }
                    </tbody>
                </table>
            </Col>
        )
    }
}

export default AccountBookingTable;