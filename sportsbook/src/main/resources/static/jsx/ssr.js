import http from "http";
import render from "./server.node";

// Polyfill variables
global.window = {};

const server = http.createServer((request, response) => {
    let body = '';

    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {
        try {
            const {location, state} = JSON.parse(body);
            response.writeHead(200, {"Content-Type": "application/json"});
            const value = render(location, state);
            response.write(JSON.stringify(value));
            response.end();
        } catch (e) {
            response.writeHead(500, {"Content-Type": "application/json"});
            response.write(JSON.stringify({exception: e.message, stack: e.stack}));
            response.end();
        }
    });
});

server.listen(0, "127.0.0.1"); // Random port, accept from localhost only

server.on("listening", () => {
    console.log('Listening on port:', server.address().port);
});

