import {
    React,
    ReactDOMServer,
    Provider,
    match,
    RouterContext,
    createStore,
    rootReducer,
    I18NProvider,
    routes,
    DocumentTitle
} from "./server.init";

let {} = React; // So ne organize-imports doesn't delete the React import

export default function (location, state) {

    let result = {};
    let props;
    match({routes, location}, (error, redirectLocation, renderProps) => {
        if (error) {
            console.log("[React Router] Error " + error);
        } else if (redirectLocation) {
            console.log("[React Router] Send redirect to " + redirectLocation);
        } else if (renderProps) {
            props = renderProps;
        } else {
            console.log("[React Router] 404 Not found. Request Path: " + window.__URL__);
        }
    });

    const store = createStore(rootReducer, state);

    if (props) {
        result.html = ReactDOMServer.renderToString(
            <Provider store={ store }>
                <I18NProvider>
                    <RouterContext {...props} />
                </I18NProvider>
            </Provider>
        );
        result.pageTitle  = DocumentTitle.rewind().title;
    }

    return result;
}