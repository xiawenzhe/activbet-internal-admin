import {combineReducers} from "redux";
import i18n from "./i18n";
import {
    CHANGE_TITLE, DISMISS_GENERAL_ERROR,
    RESET_WEBCAM_ENABLED, SHOW_GENERAL_ERROR
} from "../../actionTypes/app";

function title(state = "", action) {
    switch (action.type) {
        case CHANGE_TITLE:
            return action.title;
        default:
            return state;
    }
}

// For device detection on initial rendering that works on the
// server and the client side. Is removed as soon as redux-responsive
// kicks in after the intial page load. Is set by the server.
function initialDeviceDetection(state = null, action) {
    switch (action.type) {
        case "redux-responsive/CALCULATE_RESPONSIVE_STATE":
            return null;
        default:
            return state;
    }
}

function imageResourceUrl(state = null) {
    return state;
}

function barcodeTypes(state = []) {
    return state;
}

function webTheme(state = null) {
    return state;
}

function webcamEnabled(state = true, action) {
    switch (action.type) {
        case RESET_WEBCAM_ENABLED:
            return action.webcamEnabled;
        default:
            return state;
    }
}

function generalError(state = null, action) {
    switch (action.type) {
        case SHOW_GENERAL_ERROR:
            return {
                errorType: action.errorType,
                error: action.error,
                trace: action.trace
            };
        case DISMISS_GENERAL_ERROR:
            return null;
        default:
            return state;
    }
}

const appReducer = combineReducers({
    i18n,
    title,
    initialDeviceDetection,
    imageResourceUrl,
    barcodeTypes,
    webTheme,
    webcamEnabled,
    generalError
});

export default appReducer;