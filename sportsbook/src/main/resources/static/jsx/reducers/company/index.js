import {combineReducers} from "redux";
import {
    REQUEST_COMPANY_TREE,
    RECEIVE_COMPANY_TREE,

    RESET_WEB_CASHIER_REPORT_FILTER,
    REQUEST_WEB_CASHIER_REPORT,
    RECEIVE_WEB_CASHIER_REPORT,
    CLEAR_WEB_CASHIER_COMPANY,
    SELECT_WEB_CASHIER_COMPANY, REQUEST_COMPANY_LIST, RECEIVE_COMPANY_LIST, CLEAR_WEB_CASHIER_REPORT
} from "../../actionTypes/company";
import {SHOW_GENERAL_ERROR} from "../../actionTypes/app";

/* company tree */
function companies(state = null, action) {
    switch (action.type) {
        case REQUEST_COMPANY_TREE:
            return null;
        case RECEIVE_COMPANY_TREE:
            return action.companies;
        default:
            return state;
    }
}

/* company list */
function companyList(state = null, action) {
    switch (action.type) {
        case REQUEST_COMPANY_LIST:
            return null;
        case RECEIVE_COMPANY_LIST:
            return action.companies;
        default:
            return state;
    }
}

/* selected company id for web cashier report */
function webCashierCompanyId(state = null, action) {
    switch (action.type) {
        case SELECT_WEB_CASHIER_COMPANY:
            return action.companyId;
        case CLEAR_WEB_CASHIER_COMPANY:
            return null;
        //case RECEIVE_WEB_CASHIER_REPORT:
        //    return action.companyId;
        case CLEAR_WEB_CASHIER_REPORT:
            return null;
        default:
            return state;
    }
}

/* time range filter for web cashier report */
function webCashierReportsFilter(state = {from: null, to: null}, action) {
    switch (action.type) {
        case RESET_WEB_CASHIER_REPORT_FILTER:
            return action.filter;
        case CLEAR_WEB_CASHIER_REPORT:
            return {
                from: null, to: null
            };
        default:
            return state;
    }
}

/* web cashier report */
function webCashierReports(state = {isLoading: false, reports: null}, action) {
    switch (action.type) {
        case REQUEST_WEB_CASHIER_REPORT:
            return {
                isLoading: true,
                report: null
            };
        case RECEIVE_WEB_CASHIER_REPORT:
            return {
                isLoading: false,
                reports: action.cashierReports
            };
        case CLEAR_WEB_CASHIER_REPORT:
            return {
                isLoading: false,
                reports: null
            };

        case SHOW_GENERAL_ERROR:
        case CLEAR_WEB_CASHIER_COMPANY:
            return {
                isLoading: false,
                reports: null
            };

        default:
            return state;
    }
}

/*
function companyCashiers(state=[], action) {
    switch (action.type) {
        case RECEIVE_COMPANY_CASHIERS: {
            let companies = state;
            for (let i = 0; i < companies.length; i++) {
                if (action.companyId == companies[i].companyId) {
                    companies.splice(i, 1);
                    break;
                }
            }
            companies.push({companyId: action.companyId, cashiers: action.cashiers});
            return companies;
        }
        default:
            return state;
    }
}
 */

const companyReducer = combineReducers({
    companies,
    companyList,
    webCashierReportsFilter,
    webCashierReports,
    webCashierCompanyId
});

export default companyReducer;