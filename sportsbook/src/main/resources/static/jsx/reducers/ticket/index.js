import {combineReducers} from "redux";
import {
    CLEAR_CASH_OUT_ANALYTICS,
    CLEAR_TICKET_STATISTICS,
    CLEAR_TICKETS,
    CLOSE_EDIT_TICKET,
    OPEN_EDIT_TICKET,
    RECEIVE_CASH_OUT_ANALYTICS,
    RECEIVE_DELETE_TICKET,
    RECEIVE_TICKET_STATISTICS,
    RECEIVE_TICKETS,
    REQUEST_CASH_OUT_ANALYTICS,
    REQUEST_DELETE_TICKET,
    REQUEST_TICKET_STATISTICS,
    REQUEST_TICKETS, RESET_CASH_OUT_ANALYTICS_FILTER,
    RESET_TICKET_STATISTIC_FILTER,
    RESET_TICKETS_FILTER
} from "../../actionTypes/ticket";
import {SHOW_GENERAL_ERROR} from "../../actionTypes/app";

function ticketStatisticFilter(
    state = {
        customerUsername: "",
        ticketActive: null,
        ticketNumber: "",
        insertFrom: null,
        insertTo: null,
        stakeFrom: "",
        stakeTo: ""
    }, action
) {
    switch (action.type) {
        case RESET_TICKET_STATISTIC_FILTER:
            return action.filter;
        case CLEAR_TICKET_STATISTICS:
            return {
                customerUsername: "",
                ticketActive: null,
                ticketNumber: "",
                insertFrom: null,
                insertTo: null,
                stakeFrom: "",
                stakeTo: ""
            };
        default:
            return state;
    }
}

function ticketStatistics(state = {isLoading: false, statistics: []}, action) {
    switch (action.type) {
        case REQUEST_TICKET_STATISTICS:
            return {
                isLoading: true,
                statistics: []
            };
        case RECEIVE_TICKET_STATISTICS:
            return {
                isLoading: false,
                statistics: action.ticketStatistics
            };
        case SHOW_GENERAL_ERROR:
            return {
                isLoading: false,
                statistics: []
            };
        case CLEAR_TICKET_STATISTICS:
            return {
                isLoading: false,
                statistics: []
            };
        default:
            return state;
    }
}

function ticketsFilter(
    state = {
        companyId: null,
        customerUsername: "",
        ticketActive: null,
        ticketNumber: "",
        insertFrom: null,
        insertTo: null,
        stakeFrom: "",
        stakeTo: "",
        showBets: false,
        signature: ""
    }, action
) {
    switch (action.type) {
        case RESET_TICKETS_FILTER:
            return action.filter;
        case CLEAR_TICKETS:
            return {
                customerUsername: "",
                ticketActive: null,
                ticketNumber: "",
                insertFrom: null,
                insertTo: null,
                stakeFrom: "",
                stakeTo: "",
                showBets: false,
                signature: ""
            };
        case RECEIVE_TICKETS:
            return {
                ...state,
                signature: action.signature
            };
        default:
            return state;
    }
}

function ticketsPagination(state = {pageIndex: 0, pageSize: 25}, action) {
    switch (action.type) {
        case RECEIVE_TICKETS:
            return action.pagination;
        case CLEAR_TICKETS:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

function tickets(state = {isLoading: false, tickets: []}, action) {
    switch (action.type) {
        case REQUEST_TICKETS:
            return {
                isLoading: true,
                tickets: []
            };
        case RECEIVE_TICKETS:
            return {
                isLoading: false,
                tickets: action.tickets
            };
        case CLEAR_TICKETS:
            return {
                isLoading: false,
                tickets: []
            };
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        case RECEIVE_DELETE_TICKET: {
            let tickets = state.tickets;
            let ticket = action.ticket;
            for (let i = 0; i < tickets.length; i++) {
                if (tickets[i].id == ticket.id) {
                    tickets[i] = ticket;
                }
            }
            return {
                ...state,
                tickets: tickets
            }
        }
        default:
            return state;
    }
}

function ticketsTotalStatistic(state = null, action) {
    switch (action.type) {
        case REQUEST_TICKETS:
            return null;
        case RECEIVE_TICKETS:
            return action.ticketStatistic ? action.ticketStatistic : null;
        case CLEAR_TICKETS:
            return null;
        default:
            return state;
    }
}

function ticketEdit(state = null, action) {
    switch (action.type) {
        case OPEN_EDIT_TICKET:
            return action.ticket;
        case CLOSE_EDIT_TICKET:
            return null;
        default:
            return state;
    }
}

function isEvaluatingTicket(state = false, action) {
    switch (action.type) {
        case REQUEST_DELETE_TICKET:
            return true;
        case RECEIVE_DELETE_TICKET:
            return false;
        case SHOW_GENERAL_ERROR:
            return false;
        default:
            return state;
    }
}

function cashOutAnalyticsFilter(state = {from: null, to: null}, action) {
    switch (action.type) {
        case RESET_CASH_OUT_ANALYTICS_FILTER:
            return {
                from: action.from,
                to: action.to
            };
        case CLEAR_CASH_OUT_ANALYTICS:
            return {
                from: null,
                to: null
            };
        default:
            return state;
    }
}

function cashOutAnalytics(state = {isLoading: false, analytics: null}, action) {
    switch (action.type) {
        case REQUEST_CASH_OUT_ANALYTICS:
            return {
                isLoading: true,
                analytics: null
            };
        case RECEIVE_CASH_OUT_ANALYTICS:
            return {
                isLoading: false,
                analytics: action.cashOutAnalytics
            };
        case CLEAR_CASH_OUT_ANALYTICS:
            return {
                isLoading: false,
                analytics: null
            };
        default:
            return state;
    }
}


const ticketReducer = combineReducers({
    ticketStatisticFilter,
    ticketStatistics,

    ticketsFilter,
    ticketsPagination,
    tickets,
    ticketsTotalStatistic,

    ticketEdit,
    isEvaluatingTicket,

    cashOutAnalyticsFilter,
    cashOutAnalytics
});

export default ticketReducer;