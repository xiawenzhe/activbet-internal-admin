import {combineReducers} from "redux";
import {
    REQUEST_PAYMENT_PROVIDERS,
    RECEIVE_PAYMENT_PROVIDERS,
    RESET_PAYMENT_TRANSACTIONS_FILTER,
    REQUEST_PAYMENT_CHANNELS,
    RECEIVE_PAYMENT_CHANNELS,
    RECEIVE_PAYMENT_TRANSACTIONS,
    REQUEST_PAYMENT_TRANSACTIONS,
    RESET_PAY_OUT_REQUEST_FILTER,
    RECEIVE_PAY_OUT_REQUESTS,
    REQUEST_PAY_OUT_REQUESTS,
    CLEAR_PAYMENT_TRANSACTIONS,
    CLEAR_PAY_OUT_REQUESTS
} from "../../actionTypes/payment";
import {SHOW_GENERAL_ERROR} from "../../actionTypes/app";

/* payment providers */
function paymentProviders(state = null, action) {
    switch (action.type) {
        case REQUEST_PAYMENT_PROVIDERS:
            return null;
        case RECEIVE_PAYMENT_PROVIDERS:
            return action.paymentProviders;
        default:
            return state;
    }
}

/* payment channels */
function paymentChannels(state = null, action) {
    switch (action.type) {
        case REQUEST_PAYMENT_CHANNELS:
            return null;
        case RECEIVE_PAYMENT_CHANNELS:
            return action.paymentChannels;
        default:
            return state;
    }
}

/* transaction filter */
function paymentTransactionsFilter(
    state = {
        paymentProviderId: null,
        paymentChannelId: null,
        customerId: null,
        customerUsername: "",
        direction: null,
        status: null,
        from: null,
        to: null,
        orderDescend: true,
        signature: ""
    }, action
) {
    switch (action.type) {
        case RESET_PAYMENT_TRANSACTIONS_FILTER: {
            let filter = action.filter;
            const paymentChannels = action.paymentChannels;

            // direction changed, remove paymentChannel if it is not provided
            if (filter.direction !== state.direction) {
                if (filter.paymentChannelId) {
                    const paymentChannel = paymentChannels.find(c => c.id == filter.paymentChannelId);
                    if (paymentChannel) {
                        if (paymentChannel.paymentDirection !== filter.direction) {
                            filter.paymentChannelId = null;
                        }
                    }
                }
            }

            // payment provider changed, remove payment channel if it is not provided
            if (filter.paymentProviderId) {
                if (filter.paymentProviderId !== state.paymentProviderId) {
                    if (filter.paymentChannelId) {
                        const paymentChannel = paymentChannels.find(c => c.id == filter.paymentChannelId);
                        if (paymentChannel) {
                            if (paymentChannel.paymentProviderId !== filter.paymentProviderId) {
                                filter.paymentChannelId = null;
                            }
                        }
                    }
                }
            }

            return filter;
        }

        case CLEAR_PAYMENT_TRANSACTIONS:
            return {
                paymentProviderId: null,
                    paymentChannelId: null,
                customerId: null,
                customerUsername: "",
                direction: null,
                status: null,
                from: null,
                to: null,
                orderDescend: true,
                signature: ""
            };
        case RECEIVE_PAYMENT_TRANSACTIONS:
            return {
                ...state,
                signature: action.signature
            };
        default:
            return state;
    }
}

/* transaction pagination */
function paymentTransactionsPagination(
    state = {pageIndex: 0, pageSize: 25}, action
) {
    switch(action.type) {
        case RECEIVE_PAYMENT_TRANSACTIONS:
            return action.pagination;
        case CLEAR_PAYMENT_TRANSACTIONS:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

/* transactions */
function paymentTransactions(
    state = {isLoading: false, transactions: []}, action
) {
    switch (action.type) {
        case REQUEST_PAYMENT_TRANSACTIONS:
            return {
                isLoading: true,
                transactions: []
            };
        case RECEIVE_PAYMENT_TRANSACTIONS:
            return {
                isLoading: false,
                transactions: action.paymentTransactions
            };
        case CLEAR_PAYMENT_TRANSACTIONS:
            return {isLoading: false, transactions: []};
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
}

function payOutRequestsFilter(state = {
    companyId: null,
    payOutUserId: null,
    customerUsername: "",
    flag: null,
    from: null,
    to: null,
    keyWord: "",
    status: null,
    orderDescending: true,
    signature: ""
}, action) {
    switch (action.type) {
        case RESET_PAY_OUT_REQUEST_FILTER: {
            let filter = action.filter;

            const allCashiers = action.allCashiers;
            if (filter.companyId && filter.payOutUserId && allCashiers) {
                let cashier = allCashiers.find(c => c.userId == filter.payOutUserId);
                if (cashier) {
                    if (cashier.companyId !== filter.companyId) {
                        filter.payOutUserId = null;
                    }
                }
            }

            return filter;
        }
        case CLEAR_PAY_OUT_REQUESTS:
            return {
                companyId: null,
                payOutUserId: null,
                customerUsername: "",
                from: null,
                to: null,
                keyWord: "",
                status: null,
                orderDescending: true,
                signature: ""
            };
        case RECEIVE_PAY_OUT_REQUESTS:
            return {
                ...state,
                signature: action.signature
            };
        default:
            return state;
    }
}

function payOutRequestPagination(state = {pageIndex: 0, pageSize: 25}, action) {
    switch (action.type) {
        case RECEIVE_PAY_OUT_REQUESTS:
            return action.pagination;
        case CLEAR_PAY_OUT_REQUESTS:
            return {pageIndex: 0, pageSize: 25};
        default:
            return state;
    }
}

function payOutRequests(state = {isLoading: false, payOuts: null}, action) {
    switch (action.type) {
        case REQUEST_PAY_OUT_REQUESTS:
            return {
                isLoading: true,
                payOuts: null
            };
        case RECEIVE_PAY_OUT_REQUESTS:
            return {
                isLoading: false,
                payOuts:  action.payOutRequests
            };
        case CLEAR_PAY_OUT_REQUESTS:
            return {isLoading: false, payOuts: null};
        case SHOW_GENERAL_ERROR:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
}


const paymentReducer = combineReducers({
    paymentProviders, paymentChannels,

    paymentTransactionsFilter,
    paymentTransactionsPagination,
    paymentTransactions,

    payOutRequestsFilter,
    payOutRequestPagination,
    payOutRequests
});

export default paymentReducer;