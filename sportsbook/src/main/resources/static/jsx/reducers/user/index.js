import {combineReducers} from "redux";
import {
    REQUEST_LOGIN,
    RECEIVE_LOGIN_FAILED,
    RECEIVE_LOGIN_SUCCESSFUL,
    RECEIVE_DUPLICATE_LOGIN,

    SHOW_SETTINGS_MODAL,
    HIDE_SETTINGS_MODAL,

    REQUEST_CONFIRM_LOGIN,
    REQUEST_ABORT_LOGIN,
    RECEIVE_ABORT_LOGIN,
    FORCE_LOGOUT,
    SESSION_EXPIRED, SELECT_USER_CURRENCY
} from "../../actionTypes/user";
import {RECEIVE_CURRENCIES, REQUEST_CURRENCIES} from "../../actionTypes/app";

// Must be propagated by the server
function locale(state = "en") {
    return state;
}

function timezone(state = "Europe/Berlin") {
    return state;
}

function timezoneOffsetMinutes(state = 0) {
    return state;
}

function isLoginLoading(state = false, action) {
    switch (action.type) {
        // login
        case REQUEST_LOGIN:
            return true;
        case RECEIVE_LOGIN_SUCCESSFUL:
        case RECEIVE_LOGIN_FAILED:
        case RECEIVE_DUPLICATE_LOGIN:
            return false;

        // confirm login, confirm login has result success or failed
        case REQUEST_CONFIRM_LOGIN:
            return true;

        // abort login
        case REQUEST_ABORT_LOGIN:
            return true;
        case RECEIVE_ABORT_LOGIN:
            return false;

        default:
            return state;
    }
}

function showSettingsModal(state = false, action) {
    switch (action.type) {
        case SHOW_SETTINGS_MODAL:
            return true;
        case HIDE_SETTINGS_MODAL:
            return false;
        default:
            return state;
    }
}

function showForceLogoutModal(state = false, action) {
    switch (action.type) {
        case FORCE_LOGOUT:
            return true;
        default:
            return state;
    }
}

function showSessionExpiredModal(state = false, action) {
    switch (action.type) {
        case SESSION_EXPIRED:
            return true;
        default:
            return state;
    }
}

function authenticated(state = false, action) {
    switch (action.type) {
        case RECEIVE_LOGIN_SUCCESSFUL:
            return true;
        case FORCE_LOGOUT:
            return false;
        default:
            return state;
    }
}

/*
function userId(state = null, action) {
    switch (action.type) {
        case LOGIN_SUCCESSFUL:
            return action.userId;
        case REQUEST_FORCE_ACCOUNT_ACTIVATION:
            return action.userId;
        case FORCE_LOGOUT:
            return null;
        default:
            return state;
    }
}
 */

function username(state = null, action) {
    switch (action.type) {
        case RECEIVE_LOGIN_SUCCESSFUL:
            return action.username;
        case FORCE_LOGOUT:
            return null;
        default:
            return state;
    }
}

/*
function email(state = null, action) {
    switch (action.type) {
        case LOGIN_SUCCESSFUL:
            return action.email;
        case REQUEST_FORCE_ACCOUNT_ACTIVATION:
            return action.email;
        case FORCE_LOGOUT:
            return null;
        default:
            return state;
    }
}
 */

function loginTime(state = null, action) {
    switch (action.type) {
        case RECEIVE_LOGIN_SUCCESSFUL:
            return action.loginTime;
        case FORCE_LOGOUT:
            return null;
        default:
            return state;
    }
}

function userRoles(state = null, action) {
    switch (action.type) {
        case RECEIVE_LOGIN_SUCCESSFUL:
            return action.userRoles;
        case FORCE_LOGOUT:
            return null;
        default:
            return state;
    }
}

function showLoginConfirmation(state = false, action) {
    switch (action.type) {
        case RECEIVE_DUPLICATE_LOGIN:
            return true;
        case RECEIVE_LOGIN_SUCCESSFUL:
        case RECEIVE_ABORT_LOGIN:
        case RECEIVE_LOGIN_FAILED:
            return false;
        default:
            return state;
    }
}

function currencies(state = [], action) {
    switch (action.type) {
        case REQUEST_CURRENCIES:
            return [];
        case RECEIVE_CURRENCIES:
            return action.currencies;
        default:
            return state;
    }
}

function userCurrencyId(state = null, action) {
    switch (action.type) {
        case RECEIVE_LOGIN_SUCCESSFUL:
            return action.userCurrencyId;
        case FORCE_LOGOUT:
            return null;
        case SELECT_USER_CURRENCY:
            return action.id ? action.id : null;
        default:
            return state;
    }
}

const userReducer = combineReducers({
    isLoginLoading,
    locale,
    timezone,
    timezoneOffsetMinutes,
    loginTime,
    showSettingsModal,
    showForceLogoutModal,
    showSessionExpiredModal,
    authenticated,
    username,
    userRoles,
    showLoginConfirmation,
    currencies,
    userCurrencyId
});

export default userReducer;