/* Arguments passed to the reducers: value, previousValue, allValues, previousAllValues */

export function maxLength(maxLength) {
    return value => {
        if (!value) return value;
        if (value.length > maxLength) {
            value = value.substring(0, maxLength);
        }
        return value;
    }
}

export function onlyNums() {
    return value => {
        if (!value) return value;
        return value.toString().replace(/[^\d]/g, '');
    }
}

export function compose() {
    var funcs = arguments;
    return function () {
        var args = arguments;
        for (var i = funcs.length; i-- > 0;) {
            args = [funcs[i].apply(this, args)];
        }
        return args[0];
    };
}


// only allow input 88.88 or 88,88
export function stakeFormt() {
    return value => {
        if (!value) {
            return value
        }
        if ( /^\d*([.,]\d{0,2})?$/.test(value) ) {
            return value;
        }
    }
}

export function limitIntegerFormat() {
    return value => {
        if (!value) {
            return value
        }

        if ((/^\d+$/.test(value)) && (value.length < 11) ) {
            return value;
        }
    }
}

export const normalizers = {
    "name": maxLength(40),
    //"lastName": maxLength(40),
    "birthdayDay": compose(maxLength(2), onlyNums()),
    "birthdayMonth": compose(maxLength(2), onlyNums()),
    "birthdayYear": compose(maxLength(4), onlyNums()),
    //"phone": normalizer.onlyNums(),
    "phone": maxLength(20),
    "street": maxLength(80),
    "postalCode": maxLength(20),
    "city": maxLength(40),
    "email": maxLength(120),
    "username": maxLength(25),
    //"password": maxLength(120),
    "bonusCode": maxLength(120),
    "stakeFormat": stakeFormt(),
    "limitIntegerFormat": limitIntegerFormat()
};