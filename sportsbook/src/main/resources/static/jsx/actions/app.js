import {
    CHANGE_TITLE,
    RESET_WEBCAM_ENABLED, DISMISS_GENERAL_ERROR, SHOW_GENERAL_ERROR
} from "../actionTypes/app";
import md5 from "md5";

export function changeTitle(title) {
    return {
        type: CHANGE_TITLE,
        title
    }
}

export function dismissGeneralError() {
    return {
        type: DISMISS_GENERAL_ERROR
    }
}

export function resetWrebcamEnabled(webcamEnabled) {
    return {
        type: RESET_WEBCAM_ENABLED,
        webcamEnabled
    }
}

export function toUTC(datetime) {
    if (datetime) {
        let date = new Date(datetime);
        date.setHours(datetime.getHours() - (new Date()).getTimezoneOffset() / 60);
        return date;
    }
    return null;
};

export function showGeneralError(e) {
    return (dispatch, getState) => {
        dispatch({
            type: SHOW_GENERAL_ERROR, errorType: e.type, error: e.error, trace: e.trace
        })
    }
}