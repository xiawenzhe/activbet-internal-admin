import {
    RECEIVE_CASH_FLOW_NOTIFICATION,
    RECEIVE_MESSAGE_NOTIFICATION,
    RECEIVE_PAY_OUT_REQUEST_NOTIFICATION,
    RECEIVE_PAYMENT_TRANSACTION_NOTIFICATION,
    REQUEST_CASH_FLOW_NOTIFICATION,
    REQUEST_MESSAGE_NOTIFICATION,
    REQUEST_PAY_OUT_REQUEST_NOTIFICATION,
    REQUEST_PAYMENT_TRANSACTION_NOTIFICATION
} from "../actionTypes/notification";
import ApiRestClient from "../networking/ApiRestClient";

export function requestMessageNotification() {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_MESSAGE_NOTIFICATION
        }, true).then(
        ({notification}) => {
            dispatch({
                type: RECEIVE_MESSAGE_NOTIFICATION, notification
            })
        }
    )
}

export function requestPayOutRequestNotification() {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_PAY_OUT_REQUEST_NOTIFICATION
        }, true).then(
            ({notification}) => {
                dispatch({
                    type: RECEIVE_PAY_OUT_REQUEST_NOTIFICATION, notification
                })
            }
        )
}

export function requestPaymentTransactionNotification() {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_PAYMENT_TRANSACTION_NOTIFICATION
        }, true).then(
            ({notification}) => {
                dispatch({
                    type: RECEIVE_PAYMENT_TRANSACTION_NOTIFICATION, notification
                })
            }
        )
}

export function requestCashFlowNotification() {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_CASH_FLOW_NOTIFICATION
        }, true).then(
            ({notification}) => {
                dispatch({
                    type: RECEIVE_CASH_FLOW_NOTIFICATION, notification
                })
            }
        )
}