import {
    REQUEST_COMPANY_TREE,
    RECEIVE_COMPANY_TREE,

    RESET_WEB_CASHIER_REPORT_FILTER,
    REQUEST_WEB_CASHIER_REPORT,
    RECEIVE_WEB_CASHIER_REPORT,
    SELECT_WEB_CASHIER_COMPANY,
    CLEAR_WEB_CASHIER_COMPANY,
    REQUEST_COMPANY_LIST,
    RECEIVE_COMPANY_LIST, CLEAR_WEB_CASHIER_REPORT,
} from "../actionTypes/company";
import ApiRestClient from "../networking/ApiRestClient";
import {toUTC} from "./app";
import {SHOW_GENERAL_ERROR} from "../actionTypes/app";

/**
* get company tree
* */
export function requestCompaniesIfNeeded() {
    return (dispatch, getState) => {
        const {companies} = getState().company;
        if (!companies) {
            ApiRestClient.send({
                type:  REQUEST_COMPANY_TREE
            }, false).then(
                ({companies}) => {
                    dispatch({
                        type: RECEIVE_COMPANY_TREE, companies
                    })
                }
            )
        }
        return new Promise(resolve => resolve());
    }
}

/**
    get company list
 */
export function requestCompanyListIfNeeded() {
    return (dispatch, getState) => {
        const {companyList} = getState().company;
        if (!companyList) {
            ApiRestClient.send({
                type:  REQUEST_COMPANY_LIST
            }, false).then(
                ({companies}) => {
                    dispatch({
                        type: RECEIVE_COMPANY_LIST, companies
                    })
                }
            )
        }
        return new Promise(resolve => resolve());
    }
}

/**
* web cashier statistics filter
* */
export function resetWebCashierReportsFilter(from, to) {
    return (dispatch, getState) => {
        const filter = {from, to};
        dispatch({
            type: RESET_WEB_CASHIER_REPORT_FILTER, filter
        })
    }
}

/**
    search web cashier statistics
 */
export function requestWebCashierReports() {
    return (dispatch, getState) => {
        const {userCurrencyId} = getState().user;
        const {webCashierReportsFilter, webCashierCompanyId, companies} = getState().company;
        if (webCashierCompanyId) {
            const {from, to} = webCashierReportsFilter;

            let localFrom = toUTC(from);
            let localTo = toUTC(to);

            ApiRestClient.send({
                type:  REQUEST_WEB_CASHIER_REPORT,
                companyId: webCashierCompanyId, from: localFrom, to: localTo, currencyId: userCurrencyId
            }, false).then(
                ({cashierReports, companyId}) => {
                    dispatch({
                        type: RECEIVE_WEB_CASHIER_REPORT, cashierReports, companyId
                    })
                }
            )
        } else {
            dispatch({
                type: SHOW_GENERAL_ERROR, error: "error.web_cashier_report.no_company_selected", trace: ""
            })
        }
    }
}

/**
* clear web cashier statistics
* */
export function clearWebCashierReports() {
    return (dispatch, getState) => {
        dispatch({
            type: CLEAR_WEB_CASHIER_REPORT
        })
    }
}

export function selectWebCashierCompany(companyId) {
    return (dispatch, getState) => {
        dispatch({
            type: SELECT_WEB_CASHIER_COMPANY, companyId
        });
        return new Promise(resolve => resolve());
    }
}

export function clearWebCashierCompany() {
    return (dispatch, getState) => {
        dispatch({
            type: CLEAR_WEB_CASHIER_COMPANY
        })
    }
}

/*
    request cashier of a company
export function requestCompanyCashiers(companyId) {
    return (dispatch, getState) => {
        const companyCashiers = getState().company.companyCashiers;
        let cashiers = null;
        if (companyCashiers) {
            companyCashiers.forEach(companyCashier => {
                if (companyCashier.companyId == companyId) {
                    cashiers = companyCashier.cashiers;
                    return;
                }
            })
        }

        if (!cashiers) {
            const pageIndex = 0;
            const pageSize = 1000;
            ApiRestClient.send({
                type:  REQUEST_SEARCH_CASHIERS, pageIndex, pageSize, companyId
            }, false).then(
                ({customers}) => {
                    dispatch({
                        type: RECEIVE_COMPANY_CASHIERS, companyId, cashiers: customers
                    })
                }
            )
        }
    }
}
 */