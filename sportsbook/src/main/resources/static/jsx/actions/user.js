import ApiRestClient from "../networking/ApiRestClient";
import {
    SHOW_SETTINGS_MODAL,
    HIDE_SETTINGS_MODAL,
    REQUEST_LOGIN,
    REQUEST_CONFIRM_LOGIN,
    RECEIVE_LOGIN_SUCCESSFUL,
    RECEIVE_LOGIN_FAILED,

    REQUEST_LOGOUT,
    RECEIVE_ABORT_LOGIN,
    REQUEST_ABORT_LOGIN,

    SELECT_LOCALE,
    CHANGE_TIMEZONE_DIFF, SELECT_USER_CURRENCY
} from "../actionTypes/user";
import {replace} from "react-router-redux";
import {REQUEST_REMOVE_STATISTIC_FLAG, REQUST_ADD_STATISTIC_FLAG} from "../actionTypes/tickets";

function getNextParam() {
    let prmstr = window.location.search.substr(1);
    if (prmstr) {
        let params = prmstr.split("&");
        for (let i = 0; i < params.length; i++) {
            let param = params[i].split("=");
            if (param[0] && (param[0].toLowerCase().localeCompare("next") === 0)) {
                return decodeURIComponent(param[1]);
            }
        }
    }
    return undefined;
}

export function showSettingsModal() {
    return {type: SHOW_SETTINGS_MODAL}
}

export function hideSettingsModal() {
    return {type: HIDE_SETTINGS_MODAL}
}

export function selectLocale() {
    return {
        type: SELECT_LOCALE
    }
}

export function selectUserCurrency(id) {
    return dispatch =>
        dispatch(
            {type: SELECT_USER_CURRENCY, id}
        );
}

export function login(username, password) {
    return dispatch => ApiRestClient.send({
        type: REQUEST_LOGIN,
        username,
        password
    }, true).then(action => {
        // RECEIVE_LOGIN_SUCCESSFUL
        // REQUEST_CONFIRM_LOGIN
        dispatch(action);

        if (action.type === RECEIVE_LOGIN_SUCCESSFUL) {
            const next = getNextParam();
            if (next) {
                dispatch(replace(next));
            }
        }
    }).catch(
        (error) => {
            dispatch(
                {type: RECEIVE_LOGIN_FAILED}
            );
            throw error;
        }
    )
}

export function abortLogin() {
    return dispatch => ApiRestClient.send({
        type: REQUEST_ABORT_LOGIN
    }, true).then(
        () => dispatch({
            type: RECEIVE_ABORT_LOGIN
        })
    )
}

export function confirmLogin() {
    return dispatch => ApiRestClient.send({
        type: REQUEST_CONFIRM_LOGIN
    }, true).then(action => {
        // RECEIVE_LOGIN_SUCCESSFUL
        dispatch(action);

        const next = getNextParam();
        if (next) {
            dispatch(replace(next));
        }
    }).catch(
        (error) => {
            dispatch(
                {type: RECEIVE_LOGIN_FAILED}
            );
            throw error;
        }
    );
}

export function logout() {
    return dispatch => ApiRestClient.send({
        type: REQUEST_LOGOUT
    }, true).then(() => {
        window.location.reload(true);
    }).catch(
        () => {
            window.location.reload(true);
        }
    )
}

/* add flag to ticket */
export function addTicketStatisticFlag(ticketNumber) {
    return dispatch => ApiRestClient.send({
        type: REQUST_ADD_STATISTIC_FLAG, ticketNumber
    }, true);
}

/* remove flag from ticket */
export function removeTicketStatisticFlag(ticketNumber) {
    return dispatch => ApiRestClient.send({
        type: REQUEST_REMOVE_STATISTIC_FLAG, ticketNumber
    }, true);
}
