import ApiRestClient from "../networking/ApiRestClient";
import {
    REQUEST_FORGOT_PASSWORD_LOGS,
    RECEIVE_FORGOT_PASSWORD_LOGS,
    ERROR_FORGOT_PASSWORD_LOGS,
    RESET_SEARCH_CUSTOMER_FILTER,
    REQUEST_SEARCH_CUSTOMERS,
    RECEIVE_SEARCH_CUSTOMERS,
    REQUEST_SEARCH_CASHIERS,
    RECEIVE_SEARCH_CASHIERS,
    CLEAR_SEARCH_CUSTOMERS,
    REQUEST_CUSTOMER_ACCOUNT_BOOKINGS,
    RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS,
    CLEAR_CUSTOMER_ACCOUNT_BOOKINGS,
    RESET_CUSTOMER_ACCOUNT_BOOKINGS_FILTER,
    REQUEST_SINGLE_CUSTOMER_BY_ID,
    REQUEST_PATCH_CUSTOMER_ACCOUNT,
    REQUEST_CUSTOMER_MESSAGES,
    CLEAR_CUSTOMER_MESSAGES,
    RECEIVE_CUSTOMER_MESSAGES,
    REQUEST_UPDATE_CUSTOMER_MESSAGE,
    REQUEST_CLOSE_CUSTOMER_MESSAGE, RESET_CUSTOMER_MESSAGES_FILTER
} from "../actionTypes/customer";
import {toUTC} from "./app";
import md5 from "md5";

export function requestForgotPasswordLogs(pageIndex, pageSize) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_FORGOT_PASSWORD_LOGS, pageIndex, pageSize
        }, true).then(
            ({pagination, customerLogs}) => {
                dispatch(
                    {type: RECEIVE_FORGOT_PASSWORD_LOGS, pagination, customerLogs}
                )
            }
        ).catch(
            ({error}) => {
                dispatch(
                    {type: ERROR_FORGOT_PASSWORD_LOGS, error}
                )
            }
        )
}

export function resetSearchCustomerFilter(companyId, keyWord) {
    return (dispatch, getState) =>
        dispatch({
            type: RESET_SEARCH_CUSTOMER_FILTER, companyId, keyWord
        });
}

export function requestSearchCustomers(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {customerSearchFilter} = getState().customer;
        const {companyId, keyWord} = customerSearchFilter;

        let str = (null !== companyId) ? companyId : "companyId";
        str = str + keyWord;
        const signature = md5(str);
        const oldSignature = customerSearchFilter.signature;
        if (signature !== oldSignature) {
            pageIndex = 0;
            pageSize = 25;
        }

        ApiRestClient.send({
            type:  REQUEST_SEARCH_CUSTOMERS, companyId, keyWord, pageIndex, pageSize
        }, false).then(
            ({customers, pagination}) => {
                dispatch(
                    {type: RECEIVE_SEARCH_CUSTOMERS, customers, pagination, signature}
                )
            }
        )
    }
}

export function clearSearchCustomers() {
    return (dispatch, getState) => {
        dispatch(
            {type: CLEAR_SEARCH_CUSTOMERS}
        )
    }
}

/**
    request all cashiers
*/
export function requestAllWebCashiersIfNeeded() {
    return (dispatch, getState) => {
        const {allWebCashiers} = getState().customer;
        const pageIndex = 0;
        const pageSize = 1000;

        if (!allWebCashiers) {
            ApiRestClient.send({
                type:  REQUEST_SEARCH_CASHIERS, pageIndex, pageSize
            }, false).then(
                ({customers}) => {
                    dispatch({
                        type: RECEIVE_SEARCH_CASHIERS, customers
                    })
                }
            )
        }
        //return new Promise(resolve => resolve());
    }
}

export function requestAllWebCashiers() {
    return (dispatch, getState) => {
        const pageIndex = 0;
        const pageSize = 1000;
        ApiRestClient.send({
            type:  REQUEST_SEARCH_CASHIERS, pageIndex, pageSize
        }, false).then(
            ({customers}) => {
                dispatch({
                    type: RECEIVE_SEARCH_CASHIERS, customers
                })
            }
        )
    }
}

/**
    request customer account bookings
*/
export function requestCustomerAccountBookings(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {customerAccountBookingsFilter} = getState().customer;

        const oldSignature = customerAccountBookingsFilter.signature;
        const {category, customerUsername, keyWord, from, to, bookingType, bookingReason} = customerAccountBookingsFilter;
        let str = "";
        str = str + (null !== category) ? category : "category";
        str = str + customerUsername + keyWord;
        str = str + (null !== from) ? from : "from";
        str = str + (null !== to) ? to: "to";
        str = str + (null !== bookingType) ? bookingType : "bookingType";
        str = str + (null !== bookingReason) ? bookingReason : "bookingReason";
        const signature = md5(str);

        if (oldSignature !== signature) {
            pageIndex = 0;
            pageSize = 25;
        }

        let onlyTickets = false;
        let onlyPayments = false;
        if (category == 3) {
            onlyTickets = true;
        } else if (category == 2) {
            onlyPayments = true;
        }

        let localFrom = from;
        let localTo = to;
        if (from) {
            localFrom = toUTC(localFrom);
        }
        if (to) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type:  REQUEST_CUSTOMER_ACCOUNT_BOOKINGS, ...customerAccountBookingsFilter,
            pageIndex, pageSize, onlyTickets, onlyPayments, from: localFrom, to: localTo
        }, false).then(
            ({accountBookings, pagination}) => {
                dispatch(
                    {type: RECEIVE_CUSTOMER_ACCOUNT_BOOKINGS, accountBookings, pagination, signature}
                )
            }
        )
    }
}

export function clearCustomerAccountBookings() {
    return (dispatch, getState) => {
        dispatch(
            {type: CLEAR_CUSTOMER_ACCOUNT_BOOKINGS}
        )
    }
}

export function resetCustomerAccountBookingsFilter(filter) {
    return (dispatch, getState) => {
        dispatch({
            type: RESET_CUSTOMER_ACCOUNT_BOOKINGS_FILTER, filter
        }, true);
        return new Promise(resolve => resolve());
    };

}

/**
    request single customer
 */
export function requestSingleCustomerById(customerId) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_SINGLE_CUSTOMER_BY_ID, customerId
        });
}

/**
    patch customer account
 */
export function requestPatchCashierAccount(customerId, direction, value, text) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_PATCH_CUSTOMER_ACCOUNT, customerId, direction, value, text
        });
}

/** get customer messages */
export function requestCustomerMessages(pageIndex, pageSize) {
    return (dispatch, getState) => {
        const {customerMessagesFilter} = getState().customer;

        const {from, to, messageStatus, customerUsername} = customerMessagesFilter;
        const oldSignature = customerMessagesFilter.signature;

        let str = "";
        str = str + ((null !== from) ? from : "from");
        str = str + ((null !== to) ? to : "to");
        str = str + ((null !== messageStatus) ? messageStatus : "messageStatus");
        str = str + customerUsername;
        const signature = md5(str);

        if (signature !== oldSignature) {
            pageIndex = 0;
            pageSize = 25;
        }

        let localFrom = from;
        let localTo = to;
        if (from) {
            localFrom = toUTC(localFrom);
        }
        if (to) {
            localTo = toUTC(localTo);
        }

        ApiRestClient.send({
            type:  REQUEST_CUSTOMER_MESSAGES, from: localFrom, to: localTo, messageStatus, pageIndex, pageSize
        }, false).then(
            ({messages, pagination}) => {
                dispatch(
                    {type: RECEIVE_CUSTOMER_MESSAGES, messages, pagination, signature}
                )
            }
        );
    }
}

/** reset search messages filter */
export function resetCustomerMessagesFilter(filter) {
    return (dispatch, getState) => {
        dispatch(
            {type: RESET_CUSTOMER_MESSAGES_FILTER, filter}
        )
    }
}

/** clear messages */
export function clearCustomerMessages() {
    return (dispatch, getState) => {
        dispatch(
            {type: CLEAR_CUSTOMER_MESSAGES}
        )
    }
}

/** work on one message */
export function updateCustomerMessage(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_UPDATE_CUSTOMER_MESSAGE, id
        }, false)
}

/** close one message */
export function closeCustomerMessage(id) {
    return (dispatch, getState) =>
        ApiRestClient.send({
            type:  REQUEST_CLOSE_CUSTOMER_MESSAGE, id
        }, false)
}
