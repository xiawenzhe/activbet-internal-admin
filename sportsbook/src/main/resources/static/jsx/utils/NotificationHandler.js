class NotificationHandler {

    setStore(store) {
        this.store = store;
        this.dispatch = store.dispatch;
    }

    setNotificationSystem(notificationSystem) {
        this.notificationSystem = notificationSystem;
    }

    handleNotification(notification) {
        // Display the notification to the user
        this.notificationSystem.addNotification({
            position: "tc",
            autoDismiss: 0,
            ...notification
        });
    }

}

const notificationHandler = new NotificationHandler();
export default notificationHandler;