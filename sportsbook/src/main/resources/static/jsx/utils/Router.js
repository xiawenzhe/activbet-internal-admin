/**
 * Dependency-injection like router utils to easily be able to check active routes.
 */
class RouterUtils {

    setRouter(router) {
        this.router = router;
    }

    /**
     * Check if a route is active
     * @param {string} pathname e.g. /account/tickets
     * @param {boolean} [indexOnly] only match exact route, not children
     * @returns {boolean}
     */
    isRouteActive(pathname, indexOnly) {
        if (!this.router) {
            return false;
        }
        return this.router.isActive(pathname, indexOnly);
    }

    /**
     * Get the current location object
     * @returns {object} Location object, e.g. {
     *   pathname: "/account/tickets",
     *   query: {
     *      ...
     *   }
     *   ...
     * }
     */
    getCurrentLocation() {
        return this.router.getCurrentLocation();
    }

}

const routerUtils = new RouterUtils();

export default routerUtils;