/**
 * To be used withing onClickCapture.
 * @param callback A function that is called with the link dom element (e.target) as argument if a link was clicked.
 * @param preventDefault Prevent the default link action
 */
export function onLinkClick(callback, preventDefault = false) {
    return e => {
        if (e.target.tagName == "A") {
            if (preventDefault) {
                e.preventDefault();
            }

            if (typeof callback == "function") {
                callback(e.target);
            }
        }
    }
}