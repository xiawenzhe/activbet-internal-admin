/*
    print receipt
 */
export function printReceipt(prtContent) {
    //var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    let WinPrint = window.open('', '', 'left=0,top=0,width=800,toolbar=0,scrollbars=0,status=0');

    //var is_chrome = Boolean(WinPrint.chrome);
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.close();
    WinPrint.addEventListener('load',
        () => {
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        } , false
    );
};

/*
    duplicate window
 */
export var duplicateWindow;

export function openSecondScreen(prtContent) {
    if (!duplicateWindow) {
        duplicateWindow = window.open("/duplicate", "duplicateWindow", 'left=0,top=0,toolbar=0,scrollbars=0,status=0');
    }
    if (duplicateWindow.closed) {
        duplicateWindow = window.open("/duplicate", "duplicateWindow", 'left=0,top=0,toolbar=0,scrollbars=0,status=0');
    }

    duplicateWindow.addEventListener('load',
        () => {
            if (prtContent) {
                duplicateWindow.document.getElementById("duplicate_area").innerHTML = prtContent;
            } else {
                duplicateWindow.document.getElementById("duplicate_area").innerHTML = "Welcome";
            }
        } , false
    );
};

function duplicate(prtContentHtml) {
    if (duplicateWindow) {
        if (!duplicateWindow.closed) {
            if (prtContentHtml) {
                duplicateWindow.document.getElementById("duplicate_area").innerHTML = prtContentHtml;
            }
        }
    }
}

/*
export function duplicatePaymentCustomer() {
    let prtContent = window.document.getElementById("Duplicate_PaymentCustomer");
    if (prtContent) {
        duplicate(prtContent.innerHTML);
    }
}
 */

export function duplicateBetSlip() {
    let prtContent = window.document.getElementById("Duplicate_BetSlip");
    if (prtContent) {
        duplicate(
            '<div style="width: 400px">'
                + prtContent.innerHTML +
            '</div>'

        );
    }
}

export function duplicateTicket() {
    let prtContent = window.document.getElementById("Duplicate_Ticket");
    if (prtContent) {
        duplicate(
            '<div style="width: 800px">'
                + prtContent.innerHTML +
            '</div>'
        );
    }
}

/*
    globalbet popup
 */
export var globalBetPopupWindow;

export function openGlobalBetPopup(actionUrl, forceRefresh=false) {
    let params;
    params = "width=" + screen.width;
    params += ", height=" + screen.height;
    params += ", top=0, left=0";
    params += ", fullscreen=yes, menubar=yes, resizable=yes, scrollbars=yes, status=yes, titlebar=yes, toolbar=yes";

    // "globalBetPopupWindow"
    // "_blank"
    if (!globalBetPopupWindow) {
        globalBetPopupWindow = window.open(actionUrl, "globalBetPopupWindow", params);
    } else if (globalBetPopupWindow.closed) {
        globalBetPopupWindow = window.open(actionUrl, "globalBetPopupWindow", params);
    } else if (forceRefresh) {
        globalBetPopupWindow = window.open(actionUrl, "globalBetPopupWindow", params);
    }
};

export function closeGlobalBetPopup() {
    if (globalBetPopupWindow) {
        globalBetPopupWindow.close();
    }
}