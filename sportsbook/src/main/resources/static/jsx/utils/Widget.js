export const Type = {

    LMT_PLUS: "lmt-plus",
    LINEUP: "lineup",
    HEAD_TO_HEAD: "head-to-head",

    MATCH_TABLE: "match-live-table",
    SEASON_TABLE: "season-live-table",
    CUP_ROSTER: "cup-roster",

    VERTICAL_TIMELINE: "vertical-timeline",
    WIN_PROBABILITY: "win-probabilities",
    FACTS_FIGURES: "facts-and-figures",
    OUTCOME: "outcome",
    STANDING: "standing",
    TEAM_INFO: "team-info",
    TEAM_COMPARE: "team-compare",
    GENERAL_STATISTICS: "general-statistics",

    PLAYER_INFO: "player-info",

    HOME_ATTACKING_PERFORMANCE: "home-attacking-performance",
    AWAY_ATTACKING_PERFORMANCE: "away-attacking-performance",
    HOME_DEFENDING_PERFORMANCE: "home-defending-performance",
    AWAY_DEFENDING_PERFORMANCE: "away-defending-performance",

    AVERAGE_GOALS: "average-goals",
    SCORE_MATRIX: "score-matrix",
    SCORING_PROBABILITY: "scoring-probability",

    END: "end"
};

export const SportShortcut = {
    SOCCER: "fb",
    BASKETBALL: "ba",
    TENNIS: "te",
    RUGBY: "rg",
    VOLLEYBALL: "vb",
    BEACH_VOLLEYBALL: "bv",
    BADMINTON: "bm",
    HANDBALL: "ha",
    ICE_HOCKEY: "ih",
    DARTS: "da"
};

export function isAnyWidgetAvailable(sportShortcut, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory) {
    let any = false;

    any |= isWidgetAvailable(sportShortcut, Type.LMT_PLUS, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.LINEUP, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.HEAD_TO_HEAD, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);

    any |= isWidgetAvailable(sportShortcut, Type.MATCH_TABLE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.SEASON_TABLE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.CUP_ROSTER, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);

    any |= isWidgetAvailable(sportShortcut, Type.VERTICAL_TIMELINE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);

    any |= isWidgetAvailable(sportShortcut, Type.WIN_PROBABILITY, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.FACTS_FIGURES, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);


    any |= isWidgetAvailable(sportShortcut, Type.OUTCOME, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.STANDING, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.TEAM_INFO, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.TEAM_COMPARE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.GENERAL_STATISTICS, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.HOME_ATTACKING_PERFORMANCE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.AWAY_ATTACKING_PERFORMANCE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.HOME_DEFENDING_PERFORMANCE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.AWAY_DEFENDING_PERFORMANCE, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.AVERAGE_GOALS, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.SCORE_MATRIX, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);
    any |= isWidgetAvailable(sportShortcut, Type.SCORING_PROBABILITY, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory);

    //Type.PLAYER_INFO ?
    return any;
}

export function getStandingsWidgetType(roundType) {
    return roundType === "CUP"
        ? Type.CUP_ROSTER
        : Type.SEASON_TABLE;
}

// From the betradar widget documentation PDF
export function isWidgetAvailable(sportShortcut, type, statisticsId, tournamentStatisticsId, seasonStatisticsId, isPreLive, isLive, isHistory) {
    const SIRWidgetsAvailable = typeof window.SIR !== "undefined";
    if (!SIRWidgetsAvailable) {
        return false;
    }

    let noneNullStatisticsId = true;
    //let noneNullTournamentStatisticsId = true;
    let noneNullSeasonStatisticsId = true;
    if (typeof statisticsId === "undefined" || statisticsId === null) {
        noneNullStatisticsId = false;
    }
    //if (typeof tournamentStatisticsId === "undefined" || tournamentStatisticsId === null) {
    //    noneNullTournamentStatisticsId = false;
    //}
    if (typeof seasonStatisticsId === "undefined" || seasonStatisticsId === null) {
        noneNullSeasonStatisticsId = false;
    }

    switch (sportShortcut) {
        case SportShortcut.BASKETBALL:
        case SportShortcut.HANDBALL:
        case SportShortcut.TENNIS:
        case SportShortcut.ICE_HOCKEY:
        case SportShortcut.SOCCER: {
            switch (type) {
                // group 1 show by live and history
                case Type.LMT_PLUS:
                case Type.LINEUP:
                case Type.VERTICAL_TIMELINE:
                case Type.GENERAL_STATISTICS:
                    return (true == noneNullStatisticsId) && ( (true == isLive) || (true == isHistory) );

                // group 2 show
                case Type.MATCH_TABLE:
                case Type.HEAD_TO_HEAD:
                case Type.TEAM_COMPARE:
                case Type.STANDING:
                case Type.OUTCOME:
                    return (true == noneNullStatisticsId);

                // group 3
                case Type.WIN_PROBABILITY:
                case Type.FACTS_FIGURES:
                    return noneNullStatisticsId;

                case Type.SCORE_MATRIX:
                    return (true == noneNullStatisticsId) && ( (true == isLive) || (true == isHistory) ) ;

                case Type.SCORING_PROBABILITY:
                    return noneNullStatisticsId;

                // group 4
                case Type.HOME_ATTACKING_PERFORMANCE:
                case Type.AWAY_ATTACKING_PERFORMANCE:
                case Type.HOME_DEFENDING_PERFORMANCE:
                case Type.AWAY_DEFENDING_PERFORMANCE:
                    return noneNullStatisticsId;

                case Type.SEASON_TABLE:
                case Type.CUP_ROSTER:
                    return noneNullSeasonStatisticsId;

                // not licensed
                case Type.PLAYER_INFO:
                case Type.TEAM_INFO:
                case Type.AVERAGE_GOALS:
                    return false;

            }
        }

        /*
        case SportShortcut.BASKETBALL: {
            switch (type) {
                case Type.LMT_PLUS:
                case Type.LINEUP:
                case Type.HEAD_TO_HEAD:
                case Type.MATCH_TABLE:
                case Type.VERTICAL_TIMELINE:
                case Type.WIN_PROBABILITY:
                case Type.FACTS_FIGURES:
                case Type.OUTCOME:
                case Type.STANDING:
                case Type.TEAM_COMPARE:
                case Type.GENERAL_STATISTICS:

                case Type.HOME_ATTACKING_PERFORMANCE:
                case Type.AWAY_ATTACKING_PERFORMANCE:
                case Type.HOME_DEFENDING_PERFORMANCE:
                case Type.AWAY_DEFENDING_PERFORMANCE:

                case Type.AVERAGE_GOALS:
                case Type.SCORE_MATRIX:
                case Type.SCORING_PROBABILITY:
                    return noneNullStatisticsId;

                case Type.SEASON_TABLE:
                case Type.CUP_ROSTER:
                    return noneNullSeasonStatisticsId;

                case Type.PLAYER_INFO:
                case Type.TEAM_INFO:
                    return false;
            }
        }
         */

        /*
        case SportShortcut.HANDBALL: {
            switch (type) {
                case Type.LMT_PLUS:
                case Type.LINEUP:
                case Type.HEAD_TO_HEAD:
                case Type.MATCH_TABLE:
                case Type.VERTICAL_TIMELINE:
                case Type.WIN_PROBABILITY:
                case Type.FACTS_FIGURES:
                case Type.OUTCOME:
                case Type.STANDING:
                case Type.TEAM_COMPARE:
                    return noneNullStatisticsId;

                case Type.SEASON_TABLE:
                case Type.CUP_ROSTER:
                    return noneNullSeasonStatisticsId;

                case Type.PLAYER_INFO:
                case Type.TEAM_INFO:
                case Type.GENERAL_STATISTICS:
                    return false;
            }
        }
         */

        /*
        case SportShortcut.TENNIS: {
            switch (type) {
                case Type.LMT_PLUS:
                case Type.LINEUP:
                case Type.HEAD_TO_HEAD:
                case Type.MATCH_TABLE:
                case Type.VERTICAL_TIMELINE:
                case Type.WIN_PROBABILITY:
                case Type.FACTS_FIGURES:
                case Type.OUTCOME:
                case Type.STANDING:
                case Type.TEAM_COMPARE:
                    return noneNullStatisticsId;

                case Type.SEASON_TABLE:
                case Type.CUP_ROSTER:
                    return noneNullSeasonStatisticsId;

                case Type.PLAYER_INFO:
                case Type.TEAM_INFO:
                case Type.GENERAL_STATISTICS:
                    return false;
            }
        }
         */

        /*
        case SportShortcut.ICE_HOCKEY: {
            switch (type) {
                case Type.LMT_PLUS:
                case Type.LINEUP:
                case Type.HEAD_TO_HEAD:
                case Type.MATCH_TABLE:
                case Type.VERTICAL_TIMELINE:
                case Type.WIN_PROBABILITY:
                case Type.FACTS_FIGURES:
                case Type.OUTCOME:
                case Type.STANDING:
                case Type.TEAM_COMPARE:
                    return noneNullStatisticsId;

                case Type.SEASON_TABLE:
                case Type.CUP_ROSTER:
                    return noneNullSeasonStatisticsId;

                case Type.PLAYER_INFO:
                case Type.TEAM_INFO:
                case Type.GENERAL_STATISTICS:
                    return false;
            }
        }
         */
    }
    return false;
}
