export const clearBanks = state => ({...state, games: state.games.map(game => ({...game, isBank: false}))});

export const getSingleSystem = state => ({
    isActive: true,
    minSystem: 1,
    isStakePerBet: state.stake.perBet != null,
    stake: state.stake.total || state.stake.perBet
});

export const getCombiSystem = state => ({
    isActive: true,
    minSystem: state.games.length,
    isStakePerBet: state.stake.perBet !== null,
    stake: state.stake.total || state.stake.perBet
});

export const OddMode = {
    MIN: "MIN",
    MAX: "MAX"
};

export const BetSlipMode = {
    SINGLE: "SINGLE",
    COMBI: "COMBI",
    SYSTEM: "SYSTEM"
};

export const NotificationType = {
    ERROR: "error",
    WARNING: "warning",
    INFO: "info"
};

export function getNotificationType(notifications) {
    if (notifications.some(n => n.level === "ERROR")) {
        return NotificationType.ERROR;
    }
    if (notifications.some(n => n.level === "WARN")) {
        return NotificationType.WARNING;
    }
    if (notifications.some(n => n.level === "INFO")) {
        return NotificationType.INFO;
    }
}