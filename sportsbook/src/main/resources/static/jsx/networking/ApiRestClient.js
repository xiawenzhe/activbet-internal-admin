import log from "loglevel";

class ApiRestClient {

    constructor() {
        this.currentMessageId = 0;
        this.dispatch = () => {};
    }

    connect(store) {
        this.dispatch = store.dispatch;
    };

    CheckResponse(response) {
        return new Promise(
            (resolve, reject) => {
                if (response.status >= 200 && response.status < 300) {
                    resolve(response);
                } else {
                    response.text().
                    then(
                        (error) => {
                            reject(JSON.parse(error));
                        }
                    );
                }
            }
        );
    }


    parseJSON(response) {
        return response.json();
    }

    send(message, customErrorHandling = false) {
        const id = ++this.currentMessageId;
        message.actionId = id;

        this.dispatch(message);

        const payload = {...message};
        delete payload.type;
        log.debug("Sending restful message to", message.type, payload);

        let request = {
            headers: {
                "Content-Type" : "application/json"
            },
            method: "post",
            body: JSON.stringify(payload),
            credentials: 'include'
        };

        console.log("i am here rest", id, "/rest/" + message.type, request);

        let timeout;

        // normal promise
        const p = fetch("/rest/" + message.type, request)
            .then(
                (response) => {
                    clearTimeout(timeout);
                    return this.CheckResponse(response);
                }
            )
            .then(this.parseJSON);

        // time out after 60 seconds
        const abortPromise = new Promise((resolve, reject) => {
            timeout = setTimeout(
                () => {
                reject(
                    {
                        error: "err.api_rest_client.request_time_out"
                    }
                );
            }, 60 * 1000);
        });

        let resultPromise = Promise.race([p, abortPromise]);

        // Add an default exception handler if customErrorHandling is not set to true
        if (!customErrorHandling) {
            resultPromise.catch(e => {
                log.error(e);

                if (e.type == "UNAUTHORIZED_USER_EXCEPTION") {
                    window.location.reload();
                }

                this.dispatch({
                    type: "app/SHOW_GENERAL_ERROR", errorType: e.type, error: e.error, trace: e.trace
                })
            })
        }

        return resultPromise;
    }
}

const apiRestClient = new ApiRestClient();

export default apiRestClient;
