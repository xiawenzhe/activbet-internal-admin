export const CONNECTED = "networking/CONNECTED";
export const DISCONNECTED = "networking/DISCONNECTED";
export const CONNECTION_FORCE_CLOSED = "networking/CONNECTION_FORCE_CLOSED";