const {resolve} = require("path");

function getBabelLoaderRule(production) {
    let query = {
        babelrc: false,
        presets: [
            ["es2015", {"modules": false}],
            // webpack understands the native import syntax, and uses it for tree shaking
            "stage-0",
            // Specifies what level of language features to activate.
            // Stage 2 is "draft", 4 is finished, 0 is strawman.
            // See https://tc39.github.io/process-document/

            "react"
            // Transpile React components to JavaScript
        ],
        "ignore": [
            "node_modules"
        ]
    };

    if (!production) {
        query["plugins"] = [
            "react-hot-loader/babel",
            // Enables React code to work with HMR.
        ]
    }

    return {
        test: /\.js$/,
        loader: 'babel-loader',
        include: resolve(__dirname, 'jsx'),
        query: query
    }
}

module.exports.getBabelLoaderRule = getBabelLoaderRule;