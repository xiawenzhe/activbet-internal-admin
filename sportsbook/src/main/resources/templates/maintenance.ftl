<#import "spring.ftl" as spring>
<#-- @ftlvariable name="theme" type="java.lang.String" -->
<#-- @ftlvariable name="uuid" type="java.lang.String" -->
<#-- @ftlvariable name="error" type="java.lang.String" -->
<#-- @ftlvariable name="errorDescription" type="java.lang.String" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Server Error</title>

    <link rel="stylesheet" href="<@spring.url "/resources/css/components.css"/>" type="text/css"/>

    <style>
        .body-wrapper,
        html, body {
            min-height: 100vh;
            padding: 0;
            margin: 0;
        }

        body {
            background-color: white;
            margin: 0 auto;
            font-family: 'Arial, Helvetica Neue', Helvetica, sans-serif;
        }

        .c__navbar__brand {
            position: static;
            height: 4em;
            margin: 3em auto 0 auto;
        }

        /*header {*/
            /*background: linear-gradient(to bottom, #cdcdcd 0%, #cdcdcd 7%, #d9d9d9 26%, #dcdedd 100%);*/
        /*}*/

        article {
            background: #fff;
            text-align: center;
        }

        main, .header-container {
            margin: 0 auto;
            max-width: 60em;
        }

        .header-container {
            padding: 20px 0 10px 25px;
        }

        h2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px;
            line-height: 20px;
            color: #5f5f5f;
            padding: 10px 0;
            font-weight: bold;
        }

        p {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 18px;
            line-height: 1.5em;
            color: #5f5f5f;
            padding: 10px 0;
        }

        .body-wrapper {
            display: flex;
            flex-direction: column;
        }

        .main-wrapper {
            padding: 25px;
        }

        main {
            flex: 2;
            display: flex;
            align-self: center;
            background: white;
            width: 100%;
            max-width: 40em;
            flex-direction: column;
        }

        a {
            text-decoration: none;
            color: #5f5f5f;
        }

        h1 {
            font-size: 2rem;
            color: #5f5f5f;
        }
    </style>

    <#include 'themes/favicons.ftl'/>

</head>
<body>
<div class="body-wrapper">
    <header>
        <div class="c__navbar__brand"></div>
    </header>
    <main>
        <div class="main-wrapper">
            <article>
                <h1>We will be back soon!</h1>
                <p>The site is currently down for mantenance.</p>
            </article>
        </div>
    </main>
</div>
</body>
</html>