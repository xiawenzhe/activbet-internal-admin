<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            font-family: sans-serif;
            font-size: 9pt;
            line-height: 1.5em;
        }

        @page {
            size: A4;
            margin: 2.5cm 2cm;

            @bottom-center {
                font-family: sans-serif;
                font-size: 9pt;
                content: counter(page) " / " counter(pages)
            }
        }

        p {
            margin: 0;
            padding: .5em 0;
        }

        a {
            text-decoration: none;
            color: inherit
        }

        ul {
            padding: 0;
            list-style-type: none;
        }

        ul li {
            display: table;
            vertical-align: top;
        }

        ul li:before {
            content: "•";
            display: table-cell;
            overflow: visible;
            font-size: 1em;
            padding-right: 1em;
            padding-left: .5em;
        }

        .c__legals__enum {
            counter-reset: item;
            list-style-type: none;
            padding-left: 0;
        }

        .c__legals__enum > li {
            counter-increment: item;
            display: table;
            vertical-align: top;
            padding-top: .5em;
            orphans: 3;
            widows: 3;
            -fs-page-break-min-height: 3cm;
        }

        .c__legals__enum > li:before {
            content: counters(item, ".") ". ";
            font-weight: bold;
            font-size: 9pt;
            line-height: 1.5em;
            padding-top: .5em;
            padding-right: .5em;
            white-space: nowrap;
            display: table-cell;
        }

        ol li > ol li > ol li {
            page-break-inside: avoid;
        }

        .c__legals__enum > li ol > li:before {
            content: counters(item, ".") " ";
            font-weight: bold;
            font-size: 9pt;
            line-height: 1.5em;
            padding-top: .5em;
            padding-right: .5em;
            display: table-cell;
        }

        .c__legals h2 {
            font-size: 9pt;
            margin-bottom: 0;
        }

        .c__legals h3, .c__legals h4 {
            font-size: 9pt;
        }

        ul li+li {
            page-break-before: avoid;
        }

        h2+p, h3+p, h4+p, h2+ol, h3+ol, h4+ol, h2+ul, h3+ul, h4+ul, p+ul {
            page-break-before: avoid;
        }

        .c__legals h2, .c__legals h3, .c__legals h4 {
            width: 100%;
            display: inline-block;
            font-weight: bold;
            padding: .5em 0;
            margin: 0;
            -fs-page-break-min-height: 3cm;
        }
    </style>
</head>
<body>
<h1>${title}</h1>
${content}
</body>
</html>